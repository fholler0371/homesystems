from multiprocessing import Process
import asyncio
from pydantic import BaseModel

from pprint import pprint

###################################################
###################################################

class c_process_message(BaseModel):
    msg: str
    data: dict = {}
    app: str = ''
    target_app: str = ''
    source_app: str = ''
    msg: object = {}

###################################################
###################################################
class c_process_base:
    def __init__(self, *args, **kwargs):
        self.modul_name = 'no name'
        self.hs = kwargs.get("hs")
        self.running = True
        self.stopped = False

    async def doInit(self):
        hs = self.hs
        hs.log.debug(f'init modul: {self.modul_name}')

    async def doStart(self):
        hs = self.hs
        hs.log.debug(f'start modul: {self.modul_name}')

    async def doStop(self, stop=False):
        hs = self.hs
        hs.log.debug(f'stop modul: {self.modul_name}')
        if stop:
            self.stopped = True

