requirejs.config({
  "paths": {
    'jquery': '/js/jquery/jquery.min',
    'qrious': '/js/barcode/qrious.min',
    'packery': '/js/packery/packery.pkgd.min',
    "inject" : "/js/svg-inject",
    'jqxcore': '/js/jqwidgets/jqxcore',
    'jqxwindow': '/js/jqwidgets/jqxwindow',
    'jqxdropdownlist': '/js/jqwidgets/jqxdropdownlist',
    'jqxlistbox': '/js/jqwidgets/jqxscrollbar',
    'jqxscrollbar': '/js/jqwidgets/jqxlistbox',
    'jqxbuttons': '/js/jqwidgets/jqxbuttons',
    'jqxsplitter': '/js/jqwidgets/jqxsplitter',
    'jqxinput': '/js/jqwidgets/jqxinput',
    'jqxpasswordinput': '/js/jqwidgets/jqxpasswordinput',
    'jqxtabs': '/js/jqwidgets/jqxtabs'
  },
  'shim': {
    'jqxcore': { deps: ['jquery'] },
    'jqxwindow': { deps: ['jqxcore'] },
    'jqxdropdownlist': { deps: ['jqxlistbox'] },
    'jqxlistbox': { deps: ['jqxscrollbar'] },
    'jqxscrollbar': { deps: ['jqxbuttons'] },
    'jqxbuttons': { deps: ['jqxcore'] },
    'jqxsplitter': { deps: ['jqxcore'] },
    'jqxinput': { deps: ['jqxcore'] },
    'jqxpasswordinput': { deps: ['jqxcore'] },
    'jqxtabs': { deps: ['jqxcore'] },
    'packery': { deps: ['jquery'] }
  }
})

epoch = function () {
  var now = new Date()
  return Math.round(now / 1000)
}

function postData(url = '', data = {}, token = false) {
  if (token) {
    data.token = localStorage.getItem('access_token')
    if (data.token == null) {
      data.token = ''
    }
  }
  return fetch(url, {
    method: 'POST',
    mode: 'cors',
    cache: 'no-cache',
    redirect: 'follow',
    referrerPolicy: 'no-referrer',
    body: JSON.stringify(data)
  }).then((response) => {
    if (response.status >= 200 && response.status <= 299) {
      return response.json();
    } else {
      return { 'error': response.status, 'text': response.text() }
    }
  }).catch(err => {
    return { 'error': 1 }
  })
}

modul_fullscreen = function () {
  window.modul.fullscreen = {
    active: 'start',
    init: function () {
      modul.fullscreen.check()
      setInterval(modul.fullscreen.check, 10000)
      $('#button_fullscreen').on('click', window.modul.fullscreen.click)
    },
    check: function () {
      var innerHeight = window.innerHeight * window.devicePixelRatio,
        old = modul.fullscreen.active,
        img = $('#button_fullscreen > img'),
        url = "/js/img/mdi/fullscreen"
      modul.fullscreen.active = (innerHeight + 30 > screen.height)
      if (old != window.modul.fullscreen.active) {
        if (modul.fullscreen.active) {
          url += "-exit.svg"
        } else {
          url += ".svg"
        }
        $(img).attr("src", url)
      }
      return modul.fullscreen.active
    },
    click: function () {
      if (modul.fullscreen.active) {
        document.exitFullscreen()
      } else {
        document.documentElement.requestFullscreen()
      }
      setTimeout(modul.fullscreen.check, 500)
      modul.fullscreen.check()
    }
  }
  modul.fullscreen.init()
  console.log("fullscreen")
}

modul_login = function () {
  window.modul.login = {
    active: false,
    token_timer: null,
    tok_received: false,
    remote: false,
    init: function () {
      $('#button_login').on('click', window.modul.login.click)
      modul.login.ask_for_token()
      if (localStorage.getItem('access_token') != undefined) {
        modul.login.token_timer = setTimeout(modul.login.check_token, 5000)
      }
    },
    ask_for_token: function() {
      console.log("ask for token")
      if (!modul.login.tok_received) {
        setTimeout(modul.login.ask_for_token, 100)
        var postdata = {action: 'transfer_get'}
        document.getElementById('frame_device_id').contentWindow.postMessage(postdata, "https://auth.lcars.holler.pro")
      }
    },
    transfer_token_received: function(token) {
      console.log("received transfertoken", token)
      if (!modul.login.tok_received) {
        modul.login.tok_received = true
        if (token == null) {
          clearTimeout(modul.login.token_timer)
          modul.login.check_code()
          if (localStorage.getItem('access_token') != undefined) {
            modul.login.check_token()
          }
        } else {
          postData('/api', { 'action': 'check_transfer_token', 'transfer_token': token }).then(data => {
            if (data.error != null) {
              console.error(data)
            } else {
              modul.login.remote = true
              localStorage.setItem('access_token', data.access_token)
              localStorage.setItem('refresh_token', data.refresh_token)
              localStorage.setItem('access_token_timeout', epoch() + data.expires_in)
              $('#go_home').show().css('cursor', 'pointer')
              $('#go_home').on('click', function() {
                window.location.href = "https://lcars.holler.pro"
              })
              modul.login.check_token()
            }
          })
      }
      }
    },
    check_code: function () {
      // check for code logindata_code
      if (typeof data_code !== 'undefined' && typeof data_state !== 'undefined') {
        if (data_state == localStorage.getItem('login_state')) {
          localStorage.removeItem('access_token')
          localStorage.removeItem('refresh_token')
          localStorage.removeItem('access_token_timeout')
          localStorage.removeItem('login_state')
          postData('/api', { 'action': 'check_code', 'code': data_code }).then(data => {
            if (data.error != null) {
              console.error(data)
            } else {
              localStorage.setItem('access_token', data.access_token)
              localStorage.setItem('refresh_token', data.refresh_token)
              localStorage.setItem('access_token_timeout', epoch() + data.expires_in)
              window.location.href = 'https://' + window.location.host
            }
          })
        }
      }
    },
    check_token: function () {
      var access_token_timeout = localStorage.getItem('access_token_timeout')
      if ((access_token_timeout - epoch()) > 300) {
        var old_state = modul.login.active
        modul.login.active = true
        if (old_state != modul.login.active) {
          modul.login.update_icon()
        }
        if (window.modul.system == undefined) {
          modul.login.token_timer = setTimeout(modul.login.check_token, 1000)
        } else {
          modul.login.token_timer = setTimeout(modul.login.check_token, 60000)
          modul.system.get_module()
        }
        if (window.modul.settings != undefined) {
          modul.settings.device_defaults_load()
        }
      } else {
        var refresh_token = localStorage.getItem('refresh_token')
        postData('/api', { 'action': 'refresh_token', 'refresh_token': refresh_token }).then(data => {
          if (data.error != null) {
            console.error(data)
            localStorage.removeItem('access_token')
            localStorage.removeItem('refresh_token')
            localStorage.removeItem('access_token_timeout')
            modul.login.active = false
            modul.login.update_icon()
            window.location.href = 'https://' + window.location.host
          } else {
            localStorage.setItem('access_token', data.access_token)
            localStorage.setItem('refresh_token', data.refresh_token)
            localStorage.setItem('access_token_timeout', epoch() + data.expires_in)
            console.log("token refreshed")
            modul.login.token_timer = setTimeout(modul.login.check_token, 500)
          }
        })
      }
    },
    update_icon: function () {
      var src = '/js/img/mdi/log'
      if (modul.login.active) {
        src += 'out.svg'
      } else {
        src += 'in.svg'
      }
      $('#button_login > img').attr('src', src)
    },
    click: function () {
      if (!modul.login.active) {
        postData('/api', { 'action': 'get_login_url' }).then(data => {
          if (data.error != null) {
            console.error(data)
          } else {
            if (data.url) {
              var state = epoch()
              localStorage.setItem('login_state', state)
              window.location.assign(data.url + state)
            }
          }
        })
      } else {
        modul.login.logout()
      }
    },
    logout : function() {
      if (modul.login.active) {
        if (modul.login.remote) {
          var data = {action: 'logout'}
          data.token = localStorage.getItem('access_token')
          document.getElementById('frame_device_id').contentWindow.postMessage(data, "https://auth.lcars.holler.pro")
        }
        localStorage.removeItem('access_token')
        localStorage.removeItem('refresh_token')
        localStorage.removeItem('access_token_timeout')
        if (modul.login.remote) {
          window.location.href = 'https://lcars.holler.pro'
        } else {
          window.location.href = 'https://' + window.location.host
        }
      }
    }
  }
  modul.login.init()
  console.log("login")
}

modul_settings = function () {
  window.modul.settings = {
    logout: 180,
    clock: 180,
    device_defaults_loaded: false,
    send_to_device: true,
    init: function () {
      $('#button_user_settings').on('click', modul.settings.click)
    },
    click: function () {
      console.log('show settings')
      require(['jqxwindow', 'jqxdropdownlist'], function () {
        if (!($('#window_user_settings').length)) {
          var html =  '<div id="window_user_settings"><div><span>Einstellungen</span></div><div id="window_user_settings_content">'
          html += '<table><tr width="100%"><td style="width: 100%;">automatisch Abmelden nach:</td><td style="width: 100%; text-align: left; ">'
          html += '<div id="window_user_settings_logout"></div></td></tr>'
          html += '<tr width="100%"><td style="width: 100%;">Zeige Uhr nach:</td><td style="width: 100%; text-align: left; ">'
          html += '<div id="window_user_settings_clock"></div></td></tr></table>'
          html += '</div></div>'
          $('body').append(html)
          $('#window_user_settings').jqxWindow({
            maxHeight: 400, maxWidth: 700, minHeight: 150, minWidth: 200, height: 150, width: 500, theme: 'material',
            isModal: true, modalOpacity: 0.75,
            initContent: function () {
              var source= ['3min', '15min', '1h', 'nie']
              $("#window_user_settings_logout, #window_user_settings_clock").jqxDropDownList({theme: 'material', source: source, height:40})
              $("#window_user_settings_logout").on('change', function (event) {
                var args = event.args
                if (args) {
                  var value=[180, 900, 3600, -1]
                  modul.settings.logout = value[args.index]
                  if (modul.settings.send_to_device) {
                    modul.settings.device_save()
                  }
                }
              })
              $("#window_user_settings_clock").on('change', function (event) {
                var args = event.args
                if (args) {
                  var value=[180, 900, 3600, -1]
                  modul.settings.clock = value[args.index]
                  if (modul.settings.send_to_device) {
                    modul.settings.device_save()
                  }
                }
              })
            }
          })
        }
        $('#window_user_settings').jqxWindow('show')
        modul.settings.send_to_device = false
        var value=[180, 900, 3600, -1]
        var index = 0
        for (var i=0; i<4; i++) {
          if (modul.settings.logout == value[i]) {
            index = i
          }
        }
        $("#window_user_settings_logout").jqxDropDownList('selectIndex', index)
        var index = 0
        for (var i=0; i<4; i++) {
          if (modul.settings.clock == value[i]) {
            index = i
          }
        }
        $("#window_user_settings_clock").jqxDropDownList('selectIndex', index)
        modul.settings.send_to_device = true
      })
    },
    device_defaults_load: function() {
      if (!modul.settings.device_defaults_loaded) {
        document.getElementById('frame_device_id').contentWindow.postMessage({action: 'get_device_data', 
                                                                              token: localStorage.getItem('access_token')}, "https://auth.lcars.holler.pro")
      }
    },
    device_save: function() {
      var data = {action: 'save_device_data'}
      data.token = localStorage.getItem('access_token')
      data.timeout_clock = modul.settings.clock
      data.timeout_logout = modul.settings.logout
      document.getElementById('frame_device_id').contentWindow.postMessage(data, "https://auth.lcars.holler.pro")
    }
  }
  modul.settings.init()
  console.log("settings")
}

modul_acivity = function() {
  window.modul.actvity = {
    logout : 0,
    clock : 0,
    init : function() {
      modul.actvity.logout = modul.settings.logout
      modul.actvity.clock = modul.settings.clock
      var activityEvents = ['mousedown', 'mousemove', 'keydown', 'scroll', 'touchstart']
      activityEvents.forEach(function(eventName) {
        document.addEventListener(eventName, modul.actvity.user_action, true);
      })
      setInterval(modul.actvity.tick, 1000)
    },
    user_action : function() {
      modul.actvity.logout = modul.settings.logout
      modul.actvity.clock = modul.settings.clock
    },
    tick : function() {
      if (modul.actvity.logout >= 0) {
        modul.actvity.logout = modul.actvity.logout - 1
      }
      if (modul.actvity.logout == 0) {
        modul.login.logout()
      }
      if (modul.actvity.clock >= 0) {
        modul.actvity.clock = modul.actvity.clock - 1
      }
      if (modul.actvity.clock == 0) {
        $('.content_modul').hide()
        modul.clock.show()
      }
    }
  }
  modul.actvity.init()
  console.log("activity")
}

modul_clock = function() {
  window.modul.clock = {
    icon: '/js/img/mdi/clock.svg',
    already_init : false,
    label: 'Uhr',
    init: function() {
      if (!modul.clock.already_init) {
        modul.clock.already_init = true
        $('.content_modul').hide()
        modul.clock.show()
      }
    },
    show: function() {
      console.log('show clock')
      $('#header_appname').text(modul.clock.label)
      if (!($('#content_modul_clock').length)) {
        if ($('.loading_outer').length) { 
          $(".main_content").html("")
        }
        var html = '<div id="content_modul_clock" class="content_modul">'
        html += '<div id="modul_clock_outer" style="height: 300px; width:600px; position: absolute; top: 50%; right: 50%; transform: translate(50%, -50%);">'
        html += '<canvas id="modul_clock_hour" style="height: 200px; width:200px; position:absolute;"></canvas>'
        html += '<canvas id="modul_clock_minute" style="height: 200px; width:200px; position:absolute; left: 200px"></canvas>'
        html += '<canvas id="modul_clock_seconds" style="height: 200px; width:200px; position:absolute; left: 400px"></canvas>'
        html += '<div id="modul_clock_data" style="color: #99c; top: 210px; position: relative; font-size: 42px; font-family: LCARS; width: 100%; text-align: center"></div>'
        html += '<div id="modul_clock_week" style="color: #99c; top: 220px; position: relative; font-size: 42px; font-family: LCARS; width: 100%; text-align: center"></div></div>'
        $(".main_content").append(html)
      }
      $('#content_modul_clock').show()
      modul.clock.refresh_time()
    },
    refresh_time : function() {
      if ($('#content_modul_clock').is(':visible')) {
        var date = new Date,
          h = date.getHours(),
          m = date.getMinutes(),
          s = date.getSeconds(),
          hp = 100 / 24 * h,
          hm = 100 / 60 * m,
          hs = 100 / 60 * s
          modul.clock.update_circle('modul_clock_seconds', hs, s)
          modul.clock.update_circle('modul_clock_minute', hm, m)
          modul.clock.update_circle('modul_clock_hour', hp, h)
          setTimeout(modul.clock.refresh_time, 1000)
        // prepare Date
        var d = Date.now(),
          today =  new Date
        $('#modul_clock_data').text(today.toLocaleDateString('de-de', { weekday:"long", year:"numeric", month:"long", day:"numeric"}))
        // prepare Weeknumber
        var date = new Date(Date.now())
        date.setHours(0, 0, 0, 0)
        date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7)
        var week1 = new Date(date.getFullYear(), 0, 4)
        var week =  1 + Math.round(((date.getTime() - week1.getTime()) / 86400000 - 3 + (week1.getDay() + 6) % 7) / 7)
        $('#modul_clock_week').text(week+'. Woche')
      }
    },
    update_circle : function(ele_name, p, v) {
      var ele = $('#'+ele_name)[0];
      var ctx = ele.getContext('2d')
      var ctx = ele.getContext('2d')
      var start =  1.5 * Math.PI // Start circle from top
      var end = (2 * Math.PI) / 100
      ele.style.width = (ele.width = 200) + "px"
      ele.style.height = (ele.height = 200) + "px"
      v = v < 10 ? '0' + v : v
      p = p || 100
      ctx.lineWidth = 20
      ctx.textAlign = 'center'
      ctx.textBaseline = 'middle'
      ctx.font = '115px LCARS'
      ctx.fillStyle = '#fc9'  
      ctx.strokeStyle = '#99c'  
      ctx.clearRect(0, 0, 200, 200)
      ctx.fillText(v, 100, 100)
      ctx.beginPath()
      ctx.arc(100,100, 85, start, p * end + start, false)
      ctx.stroke()
    }
  }
  modul.clock.init()
  console.log("clock")
}

modul_system = function() {
  window.modul.system = {
    moduls : ['clock'],
    module_received : false,
    menu_retry: 50,
    timer_create_side_menu: null,
    init : function() {
      modul.system.create_side_menu()
      window.addEventListener("message", (event) => {
        var data = event.data
        if (data.action == 'settings_received') {
          modul.settings.clock = data.timeout_clock
          modul.settings.logout = data.timeout_logout
          modul.settings.device_defaults_loaded = true
        } else if (data.action == 'transfer_data') {
            modul.login.transfer_token_received(data.token)
        } else {
          console.log(event)
        }
      })
    }, 
    create_side_menu : function() {
      if (modul.system.menu_retry <= 0) {
        return
      }
      $('.main_button_area_up').html('')
      $('.main_button_area_up').height(4)
      var i = 0
      var all_loaded = true
      modul.system.moduls.forEach(function(modul_name) {
        if (modul[modul_name] == undefined) {
          console.log('modul not found:', modul_name)
          modul.system.menu_retry = modul.system.menu_retry - 1
          clearTimeout(modul.system.timer_create_side_menu)
          modul.system.timer_create_side_menu = setTimeout(modul.system.create_side_menu, 250)
          all_loaded = false
          return
        }
        if (all_loaded) {
          i = i + 1
          $('.main_button_area_up').height(i*60+4)
          var html = '<div class="button_left button_left_menu" data-id="'+modul_name+'" style="top: '
          html += (i-1)*60+4 + 'px ;"><img src="'+modul[modul_name].icon+'" class="button_32"></div>'
          $('.main_button_area_up').append(html)
        }
      })
      $('.button_left_menu').off('click')
      $('.button_left_menu').on('click', modul.system.click)
    },
    click : function(event) {
      var modul = $(this).data('id')
      $(".content_modul").hide()
      window.modul[modul].show()
    }, 
    get_module : function() {
      if (!(modul.system.module_received)) {
        modul.system.module_received = true
        postData('/api', { 'action': 'get_module' }, token=true).then(data => {
          if (data.error != null) {
            console.error(data)
          } else {
            modul.system.moduls = ['clock']
            for (var i=0; i<data.modul.length; i++) {
              var _modul = data.modul[i].modul
              requirejs.config({'paths': {_modul: data.modul[i].url}})
              require([_modul], function(){})
              modul.system.moduls.push(data.modul[i].modul)
            }
            $('#header_username').text(data.user)
            modul.system.menu_retry = 20
            modul.system.create_side_menu()
          }
        })
      }
    }
  }
  modul.system.init()
  console.log('system')
}

define(["jquery"], function ($) {
  $(document).ready(function() {
    // create Header
    $("body").prepend('<div class="header"><div class="header_l1"><div class="header_l2"><div class="header_l2i" /></div></div></div>')
    $(".header").append('<div class="header_l3"><div class="header_l3i"></div></div><div class="header_l4"></div>')
    $(".header").append('<div class="header_content"><div id="header_appname"></div><div id="header_username"></div></div>')

    $('.header').append('<div id="go_home" style="display: none"><img src="/js/img/mdi/home.svg" class="button_32"></div>')

    // create left-bar ; main
    $('.loading_outer').wrap('<div class="main_content"></div>')
    $('.main_content').wrap('<div class="left"></div>')
    $('.left').prepend('<div class="left_l1"></div><div class="left_l2"></div><div class="left_l3"><div class="left_l3i" /></div>')

    // CREATE BUTTON UP
    $('.left_l1').append('<div class="main_button_area_up"></div>')

    // CREATE BUTTON DOWN
    $('.left_l1').append('<div class="main_button_area_down"></div>')
    $('.main_button_area_down').append('<div class="button_left" id="button_fullscreen"><img src="/js/img/mdi/fullscreen.svg" class="button_32"></div>')
    $('.main_button_area_down').append('<div class="button_left" id="button_user_settings"><img src="/js/img/mdi/settings.svg" class="button_32"></div>')
    $('.main_button_area_down').append('<div class="button_left" id="button_login"><img src="/js/img/mdi/login.svg" class="button_32"></div>')

    // Create iframe for device_id
    $('body').append('<iframe id="frame_device_id" src="https://auth.lcars.holler.pro/device_id" style="display: none"></iframe>')
    window.modul = {}

    modul_fullscreen()
    modul_login()
    modul_settings()
    modul_acivity()
    modul_clock()
    modul_system()
  })
})