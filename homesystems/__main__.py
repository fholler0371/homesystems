# COMMENT: Documentation TypeHint
# DONE: typehint
# TODO: Documentation

import sys

# TODO: Quality homesystems.lib.starter
from homesystems.lib.starter import get_arguments


args = None


def main() -> int:
    ''' main function

        :return:  int
    '''
    global args
    log_str, args = get_arguments()
    if args.debug:
        print(log_str)
    log_data = [log_str]
    exit_code = 0
    if args.script is not None:
        # run scripts
        # TODO: Quality homesystems.scripts
        import homesystems.scripts as scripts

        log_str, _ = scripts.call(args.script, args)
        if args.debug:
            print(log_str)
    else:
        # run main program
        # TODO: Quality homesystems.lib.starter
        # TODO: homesystems.lib.paths_util
        from homesystems.lib.starter import run
        from homesystems.lib.paths_util import c_paths

        log_str, paths = c_paths(args)
        if args.debug:
            print(log_str)
        log_data.append(log_str)
        exit_code = run(args, paths, log_data)
    return 0
#    return exit_code


if __name__ == "__main__":
    sys.exit(main())
