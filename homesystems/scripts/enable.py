""" call systemend enable and clear """

import subprocess

def run(cmdargs):
    if cmdargs.verbose:
        print("install service: HomeSystems")
    subprocess.run("sudo cp /srv/homesystems/.git/homesystems/systemd_homesytems.service /etc/systemd/system/homesytems@hs.service".split(" "))
    if cmdargs.verbose:
        print("enable: HomeSystems")
    subprocess.run("sudo systemctl enable homesytems@hs.service".split(" "))
    if cmdargs.verbose:
        print("start: HomeSystems")
    subprocess.run("sudo systemctl start homesytems@hs.service".split(" "))
