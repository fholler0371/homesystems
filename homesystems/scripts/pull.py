""" pull change from git """

import subprocess
import os
from datetime import date

def run(cmdargs):
    os.chdir(".git/homesystems")
    subprocess.run("git pull".split(" "))
    os.chdir("../..")