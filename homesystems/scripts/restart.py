""" call systemend restart """

import subprocess

def run(cmdargs):
    subprocess.run("sudo systemctl restart homesytems@hs.service".split(" "))
