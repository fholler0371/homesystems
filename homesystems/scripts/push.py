""" commit and push changes """

import subprocess
import os
from datetime import date

def run(cmdargs):
    os.chdir(".git/homesystems")
    subprocess.run("git add *".split(" "))
    subprocess.run(f"git commit -a -m {date.today().strftime('%Y%m%d')}".split(" "))
    subprocess.run("git push".split(" "))
    os.chdir("../..")
