""" call systemend disable and clear """

import subprocess

def run(cmdargs):
    if cmdargs.verbose:
        print("stop: HomeSystems")
    subprocess.run("sudo systemctl stop homesytems@hs.service".split(" "))
    if cmdargs.verbose:
        print("disable: HomeSystems")
    subprocess.run("sudo systemctl disable homesytems@hs.service".split(" "))
    if cmdargs.verbose:
        print("remove service: HomeSystems")
    subprocess.run("sudo rm /etc/systemd/system/homesytems@hs.service".split(" "))
