# DONE: typehint
# TODO: Documentation

import importlib
from typing import Type

# TODO: Quality homesystems.lib.helper
from homesystems.lib.helper import time_it_log

# TODO: ALL Files under omesystems.scripts


HELPER_TEXT = """
    start : start systemd service
    stop : stop systemed service
    restart: restart systemd service
    enable: enable and start systemd service
    disable: disable and remove systemd service
    status : status systemed service

    push: commit and put all changes
    pull: pull from git

"""


@time_it_log
def call(name: str, cmd_args: dict):
    """ run a script

        name: str name of script to call
        cmd_args: object commandline parameter
    """
    try:
        mod = importlib.import_module(f".{name}", "homesystems.scripts")
    except Exception:
        print(f"Script not found: {name}", f"{HELPER_TEXT}")
        return
    mod.run(cmd_args)
