import asyncio

from homesystems.lib.aio_queue import AsyncMpQueue

from .inner_process import InnerProcess


class OuterProcess:
    def __init__(self, hs, name):
        self._hs = hs
        self._name = name
        self.process = None
        self.queue = None
        self.can_start = True
        self.requirements_ok = False

    def prepare(self):
        hs = self._hs
        if self.requirements_ok:
            if self.queue is None:
                self.queue = AsyncMpQueue(hs, hs.queue)

    async def create_process(self, manifest):
        hs = self._hs
        if self.queue is None:
            self.queue = AsyncMpQueue(hs, hs.queue)
        else:
            self.process = InnerProcess(
                self.queue.mirror(),
                self._name, hs.path,
                manifest, hs.config)

    def start(self):
        self.process.start()

    def is_alive(self):
        if self.process is None:
            return False
        return self.process.is_alive()

    @property
    def exitcode(self):
        return self.process.exitcode

    async def check_running(self, manifest):
        hs = self._hs
        if self.process is None:
            hs.log.info(f"create process: {self._name}")
            await self.create_process(manifest)
            await asyncio.to_thread(self.start)
        if not self.is_alive() and self.can_start:
            hs.log.debug(f"process is not running: {hs.name}")
            if self.exitcode == 255:
                self.can_start = False
            else:
                hs.log.debug(f"restart process: {hs.name}")
                await self.create_process(manifest)
                await asyncio.to_thread(self.start)
