from .outer_process import OuterProcess
from homesystems.process import c_process_message

from homesystems.const import (
    EVENT_APP_STOPPING,
)


class OuterProcessList:
    def __init__(self, hs):
        self._hs = hs
        self._data = {}

    @property
    def items(self):
        for i, (comp, cls) in enumerate(self._data.items()):
            yield (comp, cls)

    @property
    def keys(self):
        return self._data.keys()

    def create_process(self, name):
        hs = self._hs
        self._data[name] = OuterProcess(hs, name)

    def process(self, name):
        return self._data[name]

    async def send_stop_message(self):
        for name, cls in self.items:
            if cls.is_alive():
                await cls.queue.put(c_process_message(msg=EVENT_APP_STOPPING))

    @property
    def some_process_running(self):
        hs = self._hs
        for name, cls in self.items:
            if cls.is_alive():
                return True
        return False
