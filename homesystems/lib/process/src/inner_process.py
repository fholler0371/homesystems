import asyncio
from multiprocessing import Process


class InnerProcess(Process):
    def __init__(self, queue, name, path, manifest, base_config):
        Process.__init__(self)
        self.daemon = True
        self.name = name
        self.queue = queue
        self.path = path
        self.manifest = manifest
        self._base_config = base_config

    async def async_run(self):
        # wenn in der Importsection loaded -> crossreference
        from homesystems.lib.core import HS_Process

        hs = HS_Process(asyncio.get_event_loop(), self.path, self.queue,
                        self.name, self.manifest)
        await hs.start(self._base_config)
        await hs.run()
        return await hs.cleanup()

    def run(self):
        asyncio.run(self.async_run())
