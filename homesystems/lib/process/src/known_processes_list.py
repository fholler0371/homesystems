import asyncio
import time

from homesystems.lib.asyncio_files import is_file, Path, open
from homesystems.lib.asyncio_config import yaml_loader, yaml_safe

from .known_process import KonwnProcess

from pprint import pprint

FILENAME = 'components.yaml'
RPCPORT = 'rpcPort'
RPCPORT_DEFAULT = 9999

ports_lock = asyncio.Lock()


class KnownProcessList:
    def __init__(self, hs):
        self._hs = hs
        self._file_name = f"{hs.path.data}/{FILENAME}"
        self._data = {}
        self._change = False

    @property
    def processes(self):
        return self._data

    @property
    def items(self):
        for i, (comp, cls) in enumerate(self._data.items()):
            yield (comp, cls)

    @property
    def startable(self):
        for comp, cls in self.items:
            if cls.start:
                yield comp

    async def load_manifest(self, comp, fname):
        manifest = await yaml_loader(self._hs, fname)
        if comp not in self._data:
            self._data[comp] = KonwnProcess(self._hs, manifest)
            self._change = True
        else:
            if self._data[comp].update(manifest):
                self._change = True

    async def load_data(self):
        hs = self._hs
        # load history
        start_time = time.time()
        if await is_file(self._file_name):
            processes = await yaml_loader(hs, self._file_name)
            try:
                for process in processes:
                    self._data[process] = KonwnProcess(
                        hs, processes[process])
            except Exception as e:
                hs.log.error(e)
        # load from manifests
        self._change = False
        tasks = []
        async for apath in Path(hs.path.comp).glob('*'):
            comp = str(apath).split('/')[-1]
            manifest_file = f"{apath}/manifest.yaml"
            tasks.append(asyncio.create_task(
                self.load_manifest(comp, manifest_file)))
        await asyncio.gather(*tasks)
        if self._change:
            await self.save_data()
        hs.log.debug(f'load process data in {time.time()-start_time} sec')

    async def save_data(self):
        hs = self._hs
        data = {}
        for comp, cls in self.items:
            data[comp] = cls._data
        await yaml_safe(self._file_name, data)

    async def check_requirements(self, process):
        hs = self._hs
        return await self._data[process].check_requirements()

    async def check_ports(self, process):
        global ports_lock
        hs = self._hs
        self.changed = False
        while self._data[process].ports > len(
                self._data[process].reserved_ports):
            with ports_lock:  # nur eine sollte checken
                free_port = hs.config.net.get(RPCPORT, RPCPORT_DEFAULT) + 1
                for comp, cls in self.items:
                    for used_port in cls.reserved_ports:
                        if used_port >= free_port:
                            free_port = used_port + 1
                self._data[process].reserved_ports.append(free_port)
                self.changed = True
        if self.changed:
            await self.save_data()

    def reserved_ports(self, process):
        return self._data[process].reserved_ports

    def manifest(self, process):
        return self._data[process]
