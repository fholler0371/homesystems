import sys
import asyncio
from asyncio import subprocess

from homesystems.lib.requirements import check_list


DEFAULTS = [('start', False),
            ('ports', 0),
            ('reserved_ports', []),
            ('requirements', [])]


class KonwnProcess:
    def __init__(self, hs, rec):
        self.__dict__['hs'] = hs
        self.__dict__['_data'] = rec
        for defaults in DEFAULTS:
            if not defaults[0] in self._data:
                self.__dict__['_data'][defaults[0]] = defaults[1]
                # new empty list for mutable
                if self.__dict__['_data'][defaults[0]] == []:
                    self.__dict__['_data'][defaults[0]] = []

    def __setattr__(self, name, value):
        if name in self._data:
            self._data[name] = value
        else:
            self.__dict__[name] = value

    def __getattr__(self, name):
        if name in self._data:
            return self.__dict__['_data'][name]
        return super().__getattr__(name)

    def update(self, rec):
        ret = False
        for key, val in rec.items():
            if key in self.__dict__['_data']:  # hasattr(self, key):
                if not self.__dict__['_data'][key] == val:
                    self.__dict__['_data'][key] = val
                    ret = True
            else:
                self.__dict__['_data'][key] = val
                ret = True
        return ret

    async def check_requirements(self):
        hs = self.hs
        return await check_list(self.requirements)

    @property
    def _data(self):
        return self.__dict__['_data']

    @property
    def manifest(self):
        return self.__dict__['_data']
