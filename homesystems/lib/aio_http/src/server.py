import logging

from aiohttp import web

logging.getLogger('aiohttp').setLevel(logging.WARNING)
logging.getLogger('asyncio').setLevel(logging.WARNING)


class http_simple:
    def __init__(self, log, port, handler):
        self.log = log
        self.port = port
        self.handler = handler
        self.runner = None

    async def setup(self):
        server = web.Server(self.handler)
        log = logging.getLogger('http_simple')
        log.setLevel(logging.CRITICAL)
        self.runner = web.ServerRunner(server, access_log=log)
        await self.runner.setup()
        site = web.TCPSite(self.runner, '0.0.0.0', self.port)
        await site.start()

    async def stop(self):
        if self.runner:
            await self.runner.cleanup()
