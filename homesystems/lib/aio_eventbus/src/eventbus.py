import asyncio
import uuid
import time
from fnmatch import fnmatch
from functools import partial
from dataclasses import dataclass, field
from typing import Any


from pydantic import BaseModel

# FIXME: Function to set returns (proc(msg, return))


from .const import (
    EVENT_RESPONSE
)


class message(BaseModel):
    msg: str
    domain: str
    id: str
    org_id: str
    sensor: str
    context: dict = {}
    created: int
    target_app: str = ''
    result: dict = {}


@dataclass
class event:
    msg: str
    data: Any = field(default_factory=Any)
    sensor: str = ''
    target: str = ''


class msg_wait(BaseModel):
    signal: object = None
    result: object = None


class EventBus():
    def __init__(self, hs):
        self.domain = 'None'
        self.hs = hs
        self.listeners = {}
        self.events = {}
        self.default = None
        self._subscriptions = {}

    def set_domain(self, domain):
        self.domain = domain

    def uuid(self):
        return str(uuid.uuid4()).replace('-', '')

    async def add_default(self, listener):
        self.default = listener

    # V 2
    async def subscribe(self, event, listener):
        if event not in self._subscriptions:
            self._subscriptions[event] = [listener]
        else:
            self._subscriptions[event].append(listener)

    # V 2
    async def response(self, msg_id, target, data):
        await self.send(EVENT_RESPONSE, target=target, resp_id=msg_id,
                        result=data)

    # V 2
    async def send(self, msg, sensor="", target="", data=None, resp_id="",
                   result=None):
        if data is None:
            data = {}
        if result is None:
            result = {}
        ev = event(msg, data=data, sensor=sensor, target=target)
        if msg in self._subscriptions:
            for listener in self._subscriptions[msg]:
                await listener(ev, None)
        # for old
        if data is None and result is not None:
            data = {'result': result}
        # print(ev)
        # not handled send to old
        await self.hs.bus.emit(
            msg,
            sensor=sensor,
            target_app=target,
            context=data,
            result=result,
            org_id=resp_id,
            from_v2=True)

    # FIXED: remve add_listener
    async def add_listener(self, event_name, listener, sensor=None):
        if not self.listeners.get(event_name, None):
            self.listeners[event_name] = [{'func': listener, 'sensor': sensor}]
        else:
            self.listeners[event_name].append({'func': listener,
                                               'sensor': sensor})

    async def remove_listener(self, event_name, listener):
        if event_name in self.listeners:
            to_remove = None
            for _listener in self.listeners[event_name]:
                print(_listener)
                if _listener['func'] == listener:
                    to_remove = _listener
            if to_remove is not None:
                self.listeners[event_name].remove(to_remove)
            if len(self.listeners[event_name]) == 0:
                del self.listeners[event_name]

    async def subscriber_to_v2(self, listener, msg, uuid, app):
        ev = event(msg.msg, data=msg.context, sensor=msg.sensor)
        self.hs.loop.create_task(listener(ev,
                                          partial(self.response, uuid, app)))

    async def emit(self, text, org_id='', sensor='', context={},
                   target_app='', from_v2=False, **kwargs):
        hs = self.hs
        domain = kwargs.get('domain', self.domain)
        uuid = kwargs.get('id', self.uuid())
        created = kwargs.get('created', int(time.time()))
        if text == EVENT_RESPONSE:
            if org_id in self.events:
                self.events[org_id].result = kwargs.get('result', {})
                if self.events[org_id].result == {} or \
                   self.events[org_id].result is None:
                    self.events[org_id].result = context.get('result', {})
                self.events[org_id].signal.set()
                return
        result = kwargs.get('result', {})
        if result is None or result == 'none':
            result = {}
        msg = message(msg=text, domain=domain, id=uuid, org_id=org_id,
                      sensor=sensor, context=context, created=created,
                      target_app=target_app, result=result)
        if text in self.listeners:
            for _listener in self.listeners[text]:
                do_push = True
                if sensor != "" and _listener['sensor'] is not None:
                    do_push = fnmatch(sensor, _listener['sensor'])
                if do_push:
                    self.hs.loop.create_task(_listener['func'](msg))
        if not from_v2:
            if text in self._subscriptions:
                for _listener in self._subscriptions[text]:
                    app = msg.context.get('app', '')
                    await self.subscriber_to_v2(_listener, msg, uuid, app)
        if target_app != "":
            if self.default:
                try:
                    await self.default(msg)
                except Exception as e:
                    hs.kill.set()
        return uuid

    async def emit_wait(self, text, domain=None, org_id='', sensor='',
                        context={}, created=None, target_app=''):
        if domain is None:
            domain = self.hs.name
        msg_id = await self.emit(text, domain=domain, org_id=org_id,
                                 sensor=sensor, context=context,
                                 target_app=target_app)
        signal = asyncio.Event()
        self.events[msg_id] = msg_wait(signal=signal)
        result = None
        try:
            await asyncio.wait_for(signal.wait(), 5)
            result = self.events[msg_id].result
        except asyncio.TimeoutError:
            pass
        del self.events[msg_id]
        return result
