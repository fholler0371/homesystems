from aiohttp_xmlrpc import handler

from homesystems.process import c_process_message
from homesystems.const import (
    EVENT_PROCESS_RELAY
)


class XMLRPC(handler.XMLRPCView):
    def __init__(self, hs, request, *args, **kwargs):
        handler.XMLRPCView.__init__(self, request, *args, **kwargs)
        self.hs = hs

    # local response
    def rpc_get_target_host(self, app):
        hs = self.hs
        targetapp = app.split('.')
        if targetapp[1] in hs._processes.keys:
            return {'app': f'{hs.hostname}.{targetapp[1]}'}

    def rpc_message_transfer(self, msg):
        hs = self.hs
        hs.queue.put_nowait_rx(c_process_message(
            msg=EVENT_PROCESS_RELAY, data=msg))
        return {'ok': True}
