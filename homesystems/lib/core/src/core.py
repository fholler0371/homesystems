import os
import time
import asyncio
import signal
from functools import partial
import importlib

import netifaces as ni
from aiohttp import web

from homesystems.lib.asyncio_files import open, is_file, unlink
from homesystems.lib.asyncio_config import Config, yaml_loader
from homesystems.lib.aio_logger import PreLogger, Logger
from homesystems.lib.aio_eventbus import EventBus
from homesystems.lib.aio_queue import AsyncMpQueue
# FIXME: c_process_message
from homesystems.process import c_process_message
from homesystems.lib.process import KnownProcessList, OuterProcessList

from .xmlrpc import XMLRPC

from homesystems.lib.aio_eventbus import (
    EVENT_RESPONSE
)

from homesystems.const import (
    EVENT_PROCESS_RELAY,
    EVENT_CAST_REMOTE_MESSAGE,
    EVENT_APP_STOPPING,
    EVENT_PROCESS_STOPPING
)

PROCESSCHECKINTERVAL = 30
WAITALLPROCESSCLOSE = 120
WAITALLMODULQUIT = 30


class HS():
    def __init__(self, loop, path, pre_debug):
        self.loop = loop
        self.path = path
        self.hostname = os.uname()[1]
        self.pid = os.getpid()
        self.start_time = int(time.time())
        self.kill = asyncio.Event()
        self.kill_code = 0
        self.log = PreLogger(pre_debug)

    def _run(self, callback, *args, **kwargs):
        self.loop.create_task(callback(*args))

    async def run_delay(self, delay, callback, *args):
        self.loop.call_at(delay+self.loop.time(), self._run, callback,  *args)

    async def save_pid(self):
        self.log.debug("save pid file")
        async with open(f"{self.path.tmp}/{self.name}.pid", mode='w') as f:
            await f.write(str(self.pid))

    async def remove_pid(self):
        self.log.debug("remove pid file")
        fname = f"{self.path.tmp}{self.name}.pid"
        if await is_file(fname):
            await unlink(fname)

    async def signal_shutdown(self, sig):
        self.log.info(f"Received exit signal {sig.name}...")
        self.kill_code = -1
        self.kill.set()

    async def start(self, pre_logs=[], src_base_config=None):
        """
        erstellen des hs objects
        """
        # speichern der pid files
        self.loop.create_task(self.save_pid())
        # laden der configuration
        if src_base_config is None:
            self.config = Config(self)
            await self.config.load()
        else:
            self.config = src_base_config.clone(self)
        # starten des Loggers
        self.log = Logger(self)
        self.log.debug(f"app pre intiialized: {self.name}")
        # schreibe zwichengespeicherte Logs
        for pre_log in pre_logs:
            self.log.debug(pre_log)
        # add ip / import netifaces as ni
        self.ip = ni.ifaddresses(self.config.net.get(
            'interface', 'eth0'))[ni.AF_INET][0]['addr']

    async def run(self):
        self.log.info(f"app intiialized: {self.name}")
        await self.kill.wait()

    async def cleanup(self):
        # remove pid
        await self.remove_pid()


class HS_Base(HS):
    def __init__(self, loop, paths, pre_debug):
        HS.__init__(self, loop, paths, pre_debug)
        self.base = True
        self.queue = AsyncMpQueue(self, rx_only=True, maxsize=64)
        self.name = 'HomeSystems'
        self._known_processes = None
        self._processes = None
        self.log.info("start pre loading HomeSystems")

    async def queue_checker(self):
        while self.queue.running:
            try:
                msg = await self.queue.get()
                if msg == 'kill':
                    return
                if msg is None:
                    continue
                if msg.msg == EVENT_PROCESS_RELAY:
                    try:
                        target_host, target_app = msg.data['target_app'].split(
                            '.')
                        handled = False
                        if target_host in [self.hostname, 'local', '*']:
                            if target_app in self._processes.keys:
                                try:
                                    _app = self._processes.process(target_app)
                                    if _app.queue is not None:
                                        await _app.queue.put(msg)
                                    handled = True
                                except Exception as e:
                                    self.log.warn(repr(e))
                        if not handled:
                            data = {'msg': EVENT_CAST_REMOTE_MESSAGE,
                                    'domain': '', 'id': '',
                                    'org_id': '', 'sensor': '',
                                    'created': int(time.time()),
                                    'context': msg}
                            _process = self._processes.process('cast_master')
                            await _process.queue.put(
                                c_process_message(
                                    msg=EVENT_PROCESS_RELAY, data=data))
                    except Exception as e:
                        self.log.error(f"target: {target_host}.{target_app}")
                        self.log.error(repr(e))
                else:
                    print('qc', msg)
            except Exception as e:
                print(e)
                self.log.error(repr(e))

    async def prepare_process(self, name, cls):
        if not cls.requirements_ok:
            if await self._known_processes.check_requirements(name):
                cls.requirements_ok = True
            cls.prepare()
            await self._known_processes.check_ports(name)

    async def prepare_components(self):
        kp = KnownProcessList(self)
        self._known_processes = kp
        await kp.load_data()
        op = OuterProcessList(self)
        self._processes = op
        for process_name in kp.startable:
            op.create_process(process_name)
        tasks = []
        for name, cls in self._processes.items:
            tasks.append(self.prepare_process(name, cls))
        await asyncio.gather(*tasks)

    async def component_checker(self):
        if self.kill.is_set():
            return
        await self.run_delay(PROCESSCHECKINTERVAL, self.component_checker)
        tasks = []
        for name, cls in self._processes.items:
            tasks.append(cls.check_running(
                self._known_processes.manifest(name).manifest))
        await asyncio.gather(*tasks)

    async def start(self, pre_logs=[]):
        await HS.start(self, pre_logs)
        # add signals
        for s in (signal.SIGHUP, signal.SIGTERM, signal.SIGINT):
            self.loop.add_signal_handler(
                s, lambda s=s: asyncio.create_task(self.signal_shutdown(s)))
        self.log.debug(f"add signals")
        # create rpc server
        server = web.Server(partial(XMLRPC, self))
        runner = web.ServerRunner(server)
        await runner.setup()
        self.rpc = web.TCPSite(
            runner, '0.0.0.0', self.config.net.get('rpcPort', 9999))
        await self.rpc.start()
        self.log.debug(f"rpc server started")
        # starte message queue
        await self.queue.bind_async_to_mp_queue(self)
        asyncio.create_task(self.queue_checker())
        # prepare components
        self.log.debug(f"init component_checker")
        await self.prepare_components()
        asyncio.create_task(self.component_checker())

    async def cleanup(self):
        self.log.debug("system cleanup")
        await self._processes.send_stop_message()
        # wait for close
        start_wait = time.time()
        while self._processes.some_process_running \
                and (start_wait+WAITALLPROCESSCLOSE) > time.time():
            await asyncio.sleep(0.1)
        self.log.debug(
            "all processes are closed in " +
            f"{int((time.time()-start_wait)*10)/10}s")
        # stop queue
        self.queue.put_nowait_rx('kill')
        self.queue.running = False
        await asyncio.sleep(0)
        # base remove pid
        await HS.cleanup(self)
        return self.kill_code


class HS_Process(HS):
    def __init__(self, loop, paths, queue, name, manifest):
        HS.__init__(self, loop, paths, False)
        self.base = False
        self.queue = queue
        self.name = name
        self.moduls = {}
        self.manifest = manifest
        self.reserved_ports = manifest['reserved_ports']

    async def bus_to_process(self, msg):
        if hasattr(msg, 'result'):
            result = msg.result
        else:
            result = None
        await self.queue.put(
            c_process_message(
                msg=EVENT_PROCESS_RELAY,
                source_app=f"{self.hostname}.{self.manifest['name']}",
                target_app=msg.target_app,
                message=msg,
                data={
                    'msg': msg.msg,
                    'domain': msg.domain,
                    'id': msg.id,
                    'org_id': msg.org_id,
                    'sensor': msg.sensor,
                    'context': msg.context,
                    'created': msg.created,
                    'result': result,
                    'target_app': msg.target_app}))

    async def queue_checker(self):
        while self.queue.running:
            try:
                msg = await self.queue.get()
                if msg == 'kill':
                    return
                if msg is None:
                    continue
                if msg.msg == EVENT_APP_STOPPING:
                    self.kill.set()
                elif msg.msg == EVENT_PROCESS_RELAY:
                    await self.bus.emit(msg.data['msg'],
                                        result=msg.data.get('result'),
                                        domain=msg.data.get('domain', ''),
                                        id=msg.data.get('id', ''),
                                        org_id=msg.data.get('org_id', ''),
                                        sensor=msg.data.get('sensor', ''),
                                        context=msg.data.get('context', {}),
                                        created=msg.data.get('created', 0))

                else:
                    print('pqc', msg)
            except Exception as e:
                self.log.error(repr(e))

    async def _init_modul(self, libary, mod):
        try:
            lib = importlib.import_module(libary)
            if hasattr(lib, "Modul"):
                module = lib.Modul(hs=self)
            else:
                self.log.warn(f"Deprecated Modulloader {libary}")
                module = lib.c_modul(hs=self)
            self.moduls[module.modul_name] = module
            await module.doInit()
        except Exception as e:
            self.log.error(repr(e))
            self.log.error(f"can not load {mod} modul: {self.name}")

    async def modul_loader(self):
        tasks = [self._init_modul(
                    f"homesystems.components.{self.name}",
                    "base")]
        if 'states' in self.manifest and self.manifest['states']:
            tasks.append(self._init_modul(
                "homesystems.lib.modul_states",
                "states"))
        if 'mqtt' in self.manifest and self.manifest['mqtt']:
            tasks.append(self._init_modul(
                "homesystems.lib.aio_mqtt",
                "mqtt"))
        await asyncio.gather(*tasks)
        # start after all init
        tasks = []
        for modul in self.moduls:
            tasks.append(self.moduls[modul].doStart())
        await asyncio.gather(*tasks)

    async def start(self, base_config):
        await HS.start(self, src_base_config=base_config)
        # add signals
        for s in (signal.SIGHUP, signal.SIGTERM):
            self.loop.add_signal_handler(
                s, lambda s=s: asyncio.create_task(self.signal_shutdown(s)))
        self.log.debug(f"add signals")
        # add eventbus
        self.bus = EventBus(self)
        await self.bus.add_default(self.bus_to_process)
        self.log.debug(f"created eventbus")
        # starte message queue
        await self.queue.bind_async_to_mp_queue(self)
        asyncio.create_task(self.queue_checker())
        # load moduls
        self.log.debug(f"modul starter: {self.name}")
        await self.modul_loader()

    async def cleanup(self):
        self.log.debug(f"app cleanup: {self.name}")
        # stop module
        for modul in self.moduls:
            if self.moduls[modul].running:
                cond1 = hasattr(self.moduls[modul].doStop, '_overload')
                if cond1:
                    cond2 = self.moduls[modul].doStop._overload
                else:
                    cond2 = False
                if not (cond1 and cond2):
                    asyncio.create_task(
                        self.moduls[modul].doStop(stop=True))
        await self.bus.emit(EVENT_PROCESS_STOPPING)
        await asyncio.sleep(0)
        # wait for stop
        start_wait = time.time()
        something_running = True
        while something_running and \
                (start_wait+WAITALLMODULQUIT) > time.time():
            something_running = False
            for modul in self.moduls:
                if not self.moduls[modul].stopped:
                    something_running = True
            if something_running:
                await asyncio.sleep(0.1)
        self.log.debug(
            f"all module for process ({self.name}) are closed " +
            f"in {int((time.time()-start_wait)*10)/10}s")
        # remove pid
        return await HS.cleanup(self)
        self.log.debug(f"app stopped: {self.name}")
        sys.exit(self.kill_code)
