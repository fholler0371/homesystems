import asyncio
import logging

from homesystems.lib.requirements import libaries_requirements


async def setup_and_run(args, paths, pre_logs):
    """Set up Home Systems and run."""

    # check for requiremnets
    log_str, res = await libaries_requirements(paths, args.debug)
    if res:
        if args.debug:
            print(log_str)
        pre_logs.append(log_str)
    else:
        return 3

    # after preparing all libaries enter the core app
    from homesystems.lib.core import HS_Base

    hs = HS_Base(asyncio.get_running_loop(), paths, args.debug)
    await hs.start(pre_logs=pre_logs)
    await hs.run()
    return await hs.cleanup()


def run(args, paths, pre_logs):
    asyncio.run(setup_and_run(args, paths, pre_logs))
