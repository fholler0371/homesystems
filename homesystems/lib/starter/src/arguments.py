import argparse

from homesystems.lib.helper import time_it_log

from .config_helper import get_default_config_dir
from .const import RESTART_EXIT_CODE


@time_it_log
def get_arguments():
    '''get commandline arguments
       return -> object arguments data
    '''
    parser = argparse.ArgumentParser(
        description="HomeSystems: Home Data NetWork",
        epilog=f"If restart is requested, exits with code {RESTART_EXIT_CODE}",
    )
    parser.add_argument(
        "-c",
        "--config",
        metavar="path_to_config_dir",
        default=get_default_config_dir(),
        help="Directory that contains the HomeSystems configuration",
    )
    parser.add_argument(
        "-l",
        "--log_path",
        metavar="path_to_log_dir",
        default=None,
        help="Directory that log files are store",
    )
    parser.add_argument(
        "-db",
        "--history_path",
        metavar="path_to_db_dir",
        default=None,
        help="Directory that history_files are store",
    )
    parser.add_argument(
        "-d",
        "--debug", action="store_true", help="Start HomeSystems in debug mode"
    )
    parser.add_argument(
        "-s",
        "--script",
        help="Script to maintain HomeSystems",
    )
    arguments = parser.parse_args()
    return arguments
