from .src.arguments import get_arguments


def __getattr__(name):
    if name == 'run':
        from .src.runner import run
        return run
