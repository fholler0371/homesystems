import types
from jinja2 import Template

from homesystems.lib.modul_states import (
    EVENT_STATES_CHANNEL_SET_VALUE
)


async def _s_set(self, **kwargs):
    hs = self.hs
    script_code = kwargs.get('script_code', {})
    script_data = kwargs.get('data', {})
    sensor = ""
    try:
        device = script_code.get('device', '')
        for entry in script_code:
            if entry != 'device':
                sensor = f"{device}.{entry}"
                value = await jinja_script(script_code[entry],
                                           data=script_data)
                _sensor = '.'.join(sensor.split('.')[1:])
                _app = sensor.split('.')[0]
                await hs.bus.send(EVENT_STATES_CHANNEL_SET_VALUE,
                                  sensor=_sensor,
                                  target=f"*.{_app}",
                                  data={'app': "", 'value': value})
    except Exception as e:
        hs.log.error(repr(e))
        hs.log.error(sensor)
    return script_data

##################################################


async def _s_sequence(self, **kwargs):
    hs = self.hs
    script_code = kwargs.get('script_code', {})
    script_data = kwargs.get('data', {})
    try:
        for step in script_code:
            for func in self.entries:
                if func in step:
                    script_data = await self.entries[func](
                        script_code=step[func], data=script_data)
    except Exception as e:
        hs.log.error(repr(e))
    return script_data

##################################################
##################################################


class c_scriptengine:
    def __init__(self, hs, code):
        self.hs = hs
        self.code = code
        self.entries = {}
        self.add(_s_sequence, 'do__sequence')
        self.add(_s_set, 'do__set')

    def add(self, func, name):
        self.entries[name[4:]] = types.MethodType(func, self)
        setattr(self, name, types.MethodType(func, self))

    async def start(self):
        await self.do__sequence(script_code=self.code, script_data={})

##################################################
##################################################


async def jinja_script(in_str, data={}, convert_to=None):
    if isinstance(convert_to, int):
        convert_to = 'int'
    elif isinstance(convert_to, float):
        convert_to = 'float'
    elif isinstance(convert_to, str):
        convert_to = 'str'
    try:
        tm = Template(in_str)
        out = tm.render(data=data)
    except Exception:
        out = in_str
    if str(convert_to) == 'bool':
        if out.replace(' ', '') == 'False':
            out = False
        elif out.replace(' ', '') == 'True':
            out = True
    if str(convert_to) == 'float' or str(convert_to) == 'int':
        try:
            out = float(out)
        except Exception:
            pass
    if str(convert_to) == 'int':
        try:
            out = int(out)
        except Exception:
            pass
    return out
