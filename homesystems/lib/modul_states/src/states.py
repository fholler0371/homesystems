import os
import time
import yaml

from homesystems.process import c_process_base
from homesystems.lib.helper import overload

from homesystems.lib.aio_eventbus import (
    EVENT_RESPONSE
)

from homesystems.const import (
    # EVENT_PROCESS_RUNNING,
    EVENT_PROCESS_STOPPING,
    # EVENT_RCP_PORT_GET
)

from .const import (
    EVENT_STATES_DEVICE_ADD,
    EVENT_STATES_CHANNEL_ADD,
    EVENT_STATES_VALUE_NEW,
    EVENT_STATES_DEVICE_GET,
    EVENT_STATES_CHANNEL_GET,
    EVENT_STATES_CHANNEL_SUBSCRIBE,
    EVENT_STATES_CHANNEL_DATA
)

SAVE_INTERVAL = 3600


class Modul(c_process_base):
    def __init__(self, *args, **kwargs):
        c_process_base.__init__(self, *args, **kwargs)
        hs = self.hs
        self.modul_name = 'states'
        self._filename = f"{hs.path.data}/{hs.name}_states.yaml"
        self._data = {}
        self._subscriptions = {}
        self._datachange = False

    @overload
    async def doInit(self):
        hs = self.hs
        await hs.bus.subscribe(EVENT_PROCESS_STOPPING, self.doStop)
        await hs.bus.subscribe(EVENT_STATES_DEVICE_ADD, self.device_add)
        await hs.bus.subscribe(EVENT_STATES_CHANNEL_ADD, self.channel_add)
        await hs.bus.subscribe(EVENT_STATES_VALUE_NEW, self.value_new)
        await hs.bus.subscribe(EVENT_STATES_DEVICE_GET, self.device_get)
        await hs.bus.subscribe(EVENT_STATES_CHANNEL_GET, self.channel_get)
        await hs.bus.subscribe(EVENT_STATES_CHANNEL_SUBSCRIBE,
                               self.channel_subscription)
        await hs.run_delay(SAVE_INTERVAL, self.save_values)

##############################################

    @overload
    async def doStart(self):
        hs = self.hs
        try:
            if os.path.isfile(self._filename):
                with open(self._filename, "r") as yaml_file:
                    self._data = yaml.load(yaml_file, Loader=yaml.SafeLoader)
            if self._data is None:
                self._data = {}
        except Exception:
            hs.log.error(f"can nof load '{self.filename}'")

##############################################
##############################################

    async def device_add(self, msg, resp):
        hs = self.hs
        name = msg.data['name']
        label = msg.data['label']
        type = msg.data['type']
        if name not in self._data:
            self._datachange = True
            self._data[name] = {'label': label,
                                'type': type, 'channels': {}}

##############################################

    async def device_get(self, msg, resp):
        hs = self.hs
        await resp(self._data.get(msg.sensor, {}))

##############################################

    async def channel_get(self, msg, resp):
        hs = self.hs
        data = msg.data
        if data is None:
            data = {}
        target_app = data.get('app', '')
        try:
            device, channel = msg.sensor.split('.')
            value = self._data[device]['channels'][channel]
        except Exception:
            value = None
        await resp(value)

##############################################

    async def channel_subscription(self, msg, resp):
        hs = self.hs
        if msg.sensor not in self._subscriptions:
            self._subscriptions[msg.sensor] = [msg.data['app']]
        elif msg.data['app'] not in self._subscriptions[msg.sensor]:
            self._subscriptions[msg.sensor].append(msg.data['app'])
        device, channel = msg.sensor.split(".")
        await hs.bus.send(EVENT_STATES_CHANNEL_DATA,
                          sensor=f"{hs.manifest['name']}.{msg.sensor}",
                          target=msg.data['app'],
                          data=self._data[device]['channels'][channel])

##############################################

    async def channel_add(self, msg, resp):
        hs = self.hs
        data = msg.data
        if msg.sensor not in self._data:
            hs.log.warn(f"device not registered {msg.sensor}")
            return
        stamp = int(time.time())
        if data['name'] not in self._data[msg.sensor]['channels']:
            self._datachange = True
            self._data[msg.sensor]['channels'][data['name']] = {'type':
                                                                data['type'],
                                                                'lc': stamp,
                                                                'ts': stamp}
            if data['type'] == 'str':
                self._data[msg.sensor]['channels'][data['name']]['last'] = ''
                self._data[msg.sensor]['channels'][data['name']]['value'] = ''
            elif data['type'] == 'float':
                self._data[msg.sensor]['channels'][data['name']]['last'] = 0.0
                self._data[msg.sensor]['channels'][data['name']]['value'] = 0.0
            elif data['type'] == 'int':
                self._data[msg.sensor]['channels'][data['name']]['last'] = 0
                self._data[msg.sensor]['channels'][data['name']]['value'] = 0
            elif data['type'] == 'decimal':
                self._data[msg.sensor]['channels'][data['name']]['last'] = 0.0
                self._data[msg.sensor]['channels'][data['name']]['value'] = 0.0

##############################################

    async def value_new(self, msg, resv):
        hs = self.hs
        device, channel = msg.sensor.split(".")
        if device not in self._data:
            hs.log.warn(f"device not registered {device}")
            return
        if channel not in self._data[device]['channels']:
            hs.log.warn(f"device '{device}' has no channel '{channel}'")
            return
        self._datachange = True
        value = msg.data['value']
        stamp = int(time.time())
        if self._data[device]['channels'][channel]['type'] == 'str':
            value = str(value)
        elif self._data[device]['channels'][channel]['type'] == 'float':
            value = float(value)
        elif self._data[device]['channels'][channel]['type'] == 'int':
            value = int(float(value))
        elif self._data[device]['channels'][channel]['type'] == 'decimal':
            value = float(value)
        if not (value == self._data[device]['channels'][channel]['value']):
            self._data[device]['channels'][channel]['lc'] = stamp
            self._data[device]['channels'][channel]['last'] = \
                self._data[device]['channels'][channel]['value']
        self._data[device]['channels'][channel]['ts'] = stamp
        self._data[device]['channels'][channel]['value'] = value
        if msg.sensor in self._subscriptions:
            for target_app in self._subscriptions[msg.sensor]:
                await hs.bus.send(EVENT_STATES_CHANNEL_DATA,
                    sensor=f"{hs.manifest['name']}.{msg.sensor}",
                    target=target_app,
                    data=self._data[device]['channels'][channel])

##############################################
##############################################

    async def save_values(self):
        hs = self.hs
        if self.running:
            await hs.run_delay(SAVE_INTERVAL, self.save_values)
        if self._datachange:
            self._datachange = False
            with open(self._filename, 'w') as yaml_file:
                yaml.dump(self._data, yaml_file, default_flow_style=False)

##############################################
##############################################

    @overload
    async def doStop(self, msg, resv):
        self.running = False
        hs = self.hs
        self._datachange = True
        await self.save_values()
        self.stopped = True
