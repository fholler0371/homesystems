import sys
import logging
import glob
import os


TRACE_LEVELV_NUM = 5
logging.addLevelName(TRACE_LEVELV_NUM, "TRACE")


def trace(self, message, *args, **kws):
    # Yes, logger takes its '*args' as 'args'.
    if TRACE_LEVELV_NUM >= self.level:
        self._log(TRACE_LEVELV_NUM, message, args, **kws)


logging.Logger.trace = trace

BLATHER_LEVELV_NUM = 3
logging.addLevelName(BLATHER_LEVELV_NUM, "BLATHER")


def blather(self, message, *args, **kws):
    # Yes, logger takes its '*args' as 'args'.
    if BLATHER_LEVELV_NUM >= self.level:
        self._log(BLATHER_LEVELV_NUM, message, args, **kws)


logging.Logger.blather = blather


class MyFormatter(logging.Formatter):
    def format(self, record):
        s = super().format(record)
        s += " "*(80-len(s))
        s += record.msg
        return s


reset = "\x1b[0m"
bold_red = "\x1b[31;1m"
yellow = "\x1b[33;20m"
red = "\x1b[31;20m"
blue = "\x1b[34;20m"
green = "\x1b[32;20m"


class MyColorFormatter(logging.Formatter):
    def format(self, record):
        s = bold_red
        if record.levelname == "ERROR":
            s = red
        elif record.levelname == "WARN":
            s = yellow
        elif record.levelname == "INFO":
            s = green
        elif record.levelname == "DEBUG":
            s = blue
        s += super().format(record)
        s += " "*(80-len(s))
        s += record.msg
        s += reset
        return s


class Logger:
    def __init__(self, hs):
        self.hs = hs
        self._log = logging.getLogger(hs.name)
        self._log.setLevel(hs.config.logger.get('level', 'INFO').upper())
        formatter = '%(asctime)s %(levelname)-8s ' +\
                    '[%(filename)s:%(funcName)s:%(lineno)d]'
        if hs.config.logger.get('console', False):
            handler = logging.StreamHandler(sys.stdout)
            handler.setFormatter(MyColorFormatter(formatter))
            self._log.addHandler(handler)
        if hs.config.logger.get('file', True):
            log_file_name = os.path.join(hs.path.log, f"{hs.name}.log")
            mode = "a"
            if hs.config.logger.get('clear_start', True):
                for file in glob.glob(f"{log_file_name}*"):
                    try:
                        os.remove(file)
                    except Exception:
                        pass
                mode = "w"
            handler = logging.FileHandler(log_file_name, mode=mode, delay=True)
            handler.setFormatter(MyFormatter(formatter))
            self._log.addHandler(handler)

    def __getattr__(self, name):
        return getattr(self._log, name)
