from datetime import datetime


class PreLogger:
    def __init__(self, verbose):
        self.verbose = verbose

    def out(self, text):
        if self.verbose:
            print(f"{datetime.now().strftime('%Y-%m-%d %H:%M:%S,%f')[:-3]} \
                {text}")

    def info(self, text):
        self.out(f"INFO  {text}")

    def debug(self, text):
        self.out(f"DEBUG {text}")

    def error(self, text):
        self.out(f"ERROR {text}")
