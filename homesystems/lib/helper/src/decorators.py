import time


def time_it_log(func, *args, **kwargs):
    def runner(*args, **kwargs):
        start = time.time()
        ret = func(*args, **kwargs)
        diff = time.time()-start
        if diff > 1:
            time_str = f"{diff:.2f} sec"
        else:
            time_str = f"{int(diff*1000)} ms"
        return (f"run function '{func.__name__}' in {time_str}", ret)
    return runner


def overload(func):
    setattr(func, "_overload", True)
    return func
