LOG_DIR_NAME = 'log'
DB_DIR_NAME = 'db'
TMP_DIR_NAME = '.tmp'
DATA_DIR_NAME = 'data'

ERROR_CONFIG_PATH = "Fatal Error: Can not create config path:"
ERROR_LOG_PATH = "Fatal Error: Can not create log path:"
ERROR_DB_PATH = "Fatal Error: Can not create db path:"
ERROR_TMP_PATH = "Fatal Error: Can not create tmp path:"
ERROR_DATA_PATH = "Fatal Error: Can not create data path:"