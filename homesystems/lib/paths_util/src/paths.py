import os
import sys
import __main__

from homesystems.lib.helper import time_it_log

from .const import *


def ensure_paths(path, error_txt):
    if not os.path.isdir(path):
        try:
            os.makedirs(path, exist_ok=True)
        except OSError:
            print(error_txt, path)
            sys.exit(1)


@time_it_log
class c_paths(object):
    def __init__(self, args):
        self.config = args.config
        ensure_paths(self.config, ERROR_CONFIG_PATH)
        self.tmp = os.path.join(self.config, TMP_DIR_NAME)
        ensure_paths(self.tmp, ERROR_TMP_PATH)
        self.home = os.path.expanduser("~")
        if args.log_path is None:
            self.log = os.path.join(self.home, LOG_DIR_NAME)
        else:
            self.log = args.log_path
        ensure_paths(self.log, ERROR_LOG_PATH)
        self.data = os.path.join(self.home, DATA_DIR_NAME)
        ensure_paths(self.data, ERROR_DATA_PATH)
        if args.history_path is None:
            self.db = os.path.join(self.home, DB_DIR_NAME)
        else:
            self.db = args.history_path
        ensure_paths(self.db, ERROR_DB_PATH)
        self.base = ("/").join(__main__.__file__.split('/')[:-2])
        self.comp = os.path.join(self.base, 'homesystems/components')
        self.web = os.path.join(self.base, 'homesystems/www')
        self.bin = os.path.join(self.base, 'bin')
