import json
import asyncio
from contextlib import AsyncExitStack, asynccontextmanager

from asyncio_mqtt import Client, MqttError, MqttCodeError

from homesystems.process import c_process_base
from homesystems.lib.helper import overload

from .const import (
    EVENT_MQTT_CONNECT,
    EVENT_MQTT_CONNECTED,
    EVENT_MQTT_SUBSCRIBE,
    EVENT_MQTT_MESSAGE,
    EVENT_MQTT_PUBLISH
)

CHECKTIME = 10


class Modul(c_process_base):
    def __init__(self, *args, **kwargs):
        c_process_base.__init__(self, *args, **kwargs)
        self.modul_name = 'mqtt'
        self.tasks = set()
        self.restart_time = 5

    @overload
    async def doInit(self):
        hs = self.hs
        await hs.bus.subscribe(EVENT_MQTT_CONNECT, self.connect)
        await hs.bus.subscribe(EVENT_MQTT_SUBSCRIBE, self.msg_subscribe)
        await hs.bus.subscribe(EVENT_MQTT_PUBLISH, self.publish)

    async def msg_subscribe(self, msg, _):
        data = msg.data
        await self.client.subscribe(data['value'])

    async def resv_message(self, messages):
        hs = self.hs
        try:
            async for message in messages:
                try:
                    try:
                        data = json.loads(message.payload.decode())
                        if not isinstance(data, dict):
                            data = {'value': data}
                    except Exception as e:
                        data = {'value': message.payload.decode()}
                    await hs.bus.send(EVENT_MQTT_MESSAGE,
                                      sensor=str(message.topic),
                                      data=data)
                except Exception as e:
                    print(str(message.topic), data, message.payload.decode())
                    hs.log.error(repr(e))
        except Exception as e:
            if self.running:
                hs.log.error(repr(e))

    async def publish(self, msg, _):
        hs = self.hs
        value = msg.data['value']
        topic = msg.sensor
        await self.client.publish(topic, payload=value)

    async def connect(self, _, resv):
        hs = self.hs
        try:
            self.stack = AsyncExitStack()
            self.stack.push_async_callback(self.tasks_cancel, self.tasks)
            self.client = Client(hs.config.data.get('mqtt_host', '127.0.0.1'))
            await self.stack.enter_async_context(self.client)
            messages = await self.stack.enter_async_context(
                self.client.messages())
            task = asyncio.create_task(self.resv_message(messages))
            self.tasks.add(task)
            if await self.client._connected == 0:
                await hs.bus.send(EVENT_MQTT_CONNECTED)
                self.restart_time = 5
        except Exception as e:
            hs.log.error(repr(e))

    async def tasks_cancel(self, tasks):
        for task in tasks:
            if task.done():
                continue
            try:
                task.cancel()
                await task
            except asyncio.CancelledError:
                pass

    @overload
    async def stopping(self):
        self.running = False
        await self.client.__aexit__(None, None, None)
        await self.stack.aclose()
        self.stopped = True

    async def check_connection(self):
        hs = self.hs
        if self.client and self.client._disconnected.done():
            e = self.client._disconnected.exception()
            if e is not None:
                if 'The connection was lost.' in str(e):
                    hs.log.warn(f'mqtt restart in {self.restart_time}s')
                    await asyncio.sleep(self.restart_time)
                    self.restart_time = self.restart_time * 2
                    await hs.bus.send(EVENT_MQTT_CONNECT)
        if self.running:
            await hs.run_delay(CHECKTIME, self.check_connection)

    @overload
    async def doStart(self):
        hs = self.hs
        await hs.run_delay(CHECKTIME, self.check_connection)
