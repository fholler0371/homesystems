def version_comparer(version, desired_version, comparator):
    v1 = version.strip().replace(' ', '').split('.')
    for entry in enumerate(v1):
        try:
            v1[entry[0]] = int(entry[1])
        except Exception:
            v1[entry[0]] = 0
    v2 = desired_version.strip().replace(' ', '').split('.')
    for entry in enumerate(v2):
        try:
            v2[entry[0]] = int(entry[1])
        except Exception:
            v2[entry[0]] = 0
    while len(v1) < len(v2):
        v1.append(0)
    while len(v2) < len(v1):
        v2.append(0)
    v1 = tuple(v1)
    v2 = tuple(v2)
    if comparator == '<=':
        return v1 <= v2
    elif comparator == '>=':
        return v1 >= v2
    elif comparator == '==':
        return v1 == v2
    elif comparator == '>':
        return v1 > v2
    elif comparator == '<':
        return v1 < v2
    else:
        print('Unexpected comparator')
        return False


def to_name_and_version(s):
    found = []
    for ele in ['>', '<', '=']:
        pos = s.find(ele)
        if pos > -1:
            found.append(pos)
    if len(found):
        name = s[:min(found)].strip()
        version = s[min(found):].replace(' ', '')
        return (name, version)
    return (s, '')


def seperate_operator(s):
    op = ""
    while s.startswith('>') or s.startswith('<') or s.startswith('='):
        op = s[0]
        s = s[1:]
    return (op, s)
