import os
import sys
from pathlib import Path
import subprocess as s
import time
import asyncio
from asyncio.subprocess import PIPE

from homesystems.lib.asyncio_files import Path as aPath, open

from .helper import seperate_operator, to_name_and_version, version_comparer


LIBFOLDER = 'homesystems/lib'
package_dict = None


# FIXME: Loadfile to list of entries (not parsed)

# async def check

install_lock = None


async def pip_install(name):
    global install_lock
    if not install_lock:
        install_lock = asyncio.Lock()
    async with install_lock:
        cmd = f'{sys.executable} -m pip install "{name}"'
        proc = await asyncio.create_subprocess_shell(
            cmd, stdout=PIPE, stderr=PIPE)
        await proc.communicate()
        return proc.returncode == 0


async def get_installed():
    global install_lock
    if not install_lock:
        install_lock = asyncio.Lock()
    async with install_lock:
        cmd = f"{sys.executable} -m pip list"
        proc = await asyncio.create_subprocess_shell(
            cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = await proc.communicate()
        packages = stdout.decode().split('\n')[2:-1]
        package_version = [package.split() for package in packages]
        package_dict = {}
        for package, version in package_version:
            package_dict[package] = version
        return package_dict


package_lock = None


async def check_installed(name, specs, full):
    global package_lock
    if not package_lock:
        package_lock = asyncio.Lock()
    async with package_lock:
        global package_dict
        if package_dict is None:
            package_dict = await get_installed()
    if "[" in name:
        name = name.split("[")[0]
    if name not in package_dict:
        if not await pip_install(full):
            print(f'Can not install {full}')
            return False
        package_dict = await get_installed()
    if specs == []:
        return True
    for spec in specs:
        if not version_comparer(package_dict[name], spec[1], spec[0]):
            if not await pip_install(full):
                print(f'Can not install {full}')
                return False
            package_dict = await get_installed()
    return True


async def check_list(requirements_list, debug=False):
    result = True
    for s in requirements_list:
        s = s.strip()
        if s and not s.startswith('#'):
            parsed = to_name_and_version(s)
            specs = []
            for spec in parsed[1].split(','):
                if spec:
                    specs.append(seperate_operator(spec))
            if not await check_installed(
                    parsed[0], specs, f"{parsed[0]}{parsed[1]}"):
                result = False
                if debug:
                    print(f"can not install: {s}")
                print(f"can not install: {s}")
    return result


async def check_file(package_dict, file_name, debug):
    if debug:
        print(f"check requirements in {file_name}")
    async with open(str(file_name)) as f:
        content = await f.read()
        return await check_list(content.splitlines(), debug)
    return True


async def libaries_requirements(paths, debug=False):
    global package_dict
    result = True
    start = time.perf_counter()
    tasks = []
    path = aPath(paths.base) / LIBFOLDER
    async for file_name in path.glob('*/requirement.txt'):
        tasks.append(check_file(package_dict, file_name, debug))
    result = all(await asyncio.gather(*tasks))
    time_str = f"function 'alibaries_requirements' run \
        in {(time.perf_counter()-start)*1000:2f} ms"
    return [time_str, result]
