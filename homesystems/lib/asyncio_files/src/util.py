import os
import shutil
import asyncio


def is_file(name):
    return asyncio.to_thread(os.path.isfile, name)


def copy(src, dest):
    return asyncio.to_thread(shutil.copy, src, dest)


def path_split(path):
    return os.path.split(path)


def unlink(name):
    return asyncio.to_thread(os.unlink, name)
