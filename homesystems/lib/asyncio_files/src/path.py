import asyncio
from pathlib import Path as _Path


class Path:
    def __init__(self, folder, loop=None):
        self.path = _Path(folder)
        self.loop = loop or asyncio.get_running_loop()

    def __truediv__(self, path_add):
        return Path(self.path / path_add)

    async def glob(self, search):
        self.glob = self.path.glob(search)
        for file in self.glob:
            yield Path(file, loop=self.loop)

    async def is_file(self):
        return await self.loop.run_in_executor(None, self.path.is_file)

    def __str__(self):
        return str(self.path)
