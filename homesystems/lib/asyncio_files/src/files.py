import asyncio
from contextlib import AbstractAsyncContextManager


class Open(AbstractAsyncContextManager):
    def __init__(self, filename, mode="r", loop=None):
        self.filename = filename
        self.mode = mode
        self._loop = loop
        self._obj = None

    def write(self, data):
        return asyncio.to_thread(self._obj.write, data)

    def read(self, bytes_to_read=-1):
        if bytes_to_read == -1:
            return asyncio.to_thread(self._obj.read)
        else:
            return self._iter_read(bytes_to_read)

    async def _iter_read(self, bytes_to_read):
        while True:
            result = await asyncio.to_thread(self._obj.read, bytes_to_read)
            if len(result) > 0:
                yield result
            else:
                break

    async def __aenter__(self):
        if self._loop is None:
            self._loop = asyncio.get_event_loop()
        self._obj = await asyncio.to_thread(open, self.filename, self.mode)
        return self

    async def __aexit__(self, exc_type, exc, tb):
        if self._obj:
            await asyncio.to_thread(self._obj.close)
