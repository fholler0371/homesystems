import asyncio


class DiscoveryClientProtocol(asyncio.DatagramProtocol):
    def __init__(self, hs, callback):
        self.hs = hs
        self.callback = callback
        self.transport = None

    def connection_made(self, transport):
        self.transport = transport
        sock = self.transport.get_extra_info('socket')

    def error_received(self, exc):
        if not self.hs.kill.is_set():
            self.hs.log.error(exc)

    def connection_lost(self, exc):
        self.hs.log.debug("Connection closed")

    def datagram_received(self, data, addr):
        if not self.hs.kill.is_set():
            hs = self.hs
            asyncio.create_task(self.callback(data, addr))


class c_client_receiver:
    def __init__(self, hs, port, callback):
        self.hs = hs
        self.port = port
        self.callback = callback

    async def start(self):
        await self.hs.loop.create_datagram_endpoint(
            lambda: DiscoveryClientProtocol(self.hs, self.callback),
            local_addr=('0.0.0.0', self.port))
