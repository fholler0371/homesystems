import netifaces as ni
import socket
import asyncio


class c_client_broadcast:
    def __init__(self, hs, port):
        self.hs = hs
        self.port = port

    async def send(self, msg):
        for interface in ni.interfaces():
            try:
                ip = ni.ifaddresses(interface)[ni.AF_INET][0]['addr']
            except Exception:
                ip = ""
            if ip != "":
                sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM,
                                     socket.IPPROTO_UDP)
                sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
                try:
                    sock.setsockopt(socket.SOL_SOCKET, 25,
                                    str(interface + '\0').encode('utf-8'))
                except Exception:
                    pass
                sock.settimeout(0.2)
                sock.sendto(msg.encode(), ('255.255.255.255', self.port))
                await asyncio.sleep(0.25)
                sock.close()
