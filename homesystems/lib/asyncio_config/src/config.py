import yaml

from deepmerge import always_merger

from homesystems.lib.asyncio_files import is_file, copy

from .wrapper_yaml import yaml_load

from pprint import pprint


CONFIG_FILE_NAME = "config.yaml"
DEFAULT_FILE = f"default_{CONFIG_FILE_NAME}"

PACKAGES = "packages"

# attribut: internale_store, config_file_attribut, is_app_base
mapper = {'app': ('c_app', 'apps', True),
          'logger': ('c_logger', 'log', False),
          'data': ('c_data', 'data', False),
          'net': ('c_net', 'net', False),
          'front': ('c_front', 'front', False),
          'scenes': ('c_scenes', 'scenes', False)}


class Config:
    def __init__(self, hs):
        self.hs = hs

    def clone(self, hs):
        cls = Config(hs)
        cls.raw = self.raw
        cls.base_domain = self.base_domain
        return cls

    async def load(self):
        hs = self.hs
        hs.log.debug('load config')
        fname = f"{hs.path.config}/{CONFIG_FILE_NAME}"
        if not await is_file(fname):
            await copy(f"{hs.path.bin}/{DEFAULT_FILE}", fname)
        self.raw = await yaml_load(hs, fname)
        if self.raw is None:
            self.raw = {}
        if PACKAGES in self.raw:
            for package in self.raw[PACKAGES]:
                self.raw = always_merger.merge(
                    self.raw, self.raw[PACKAGES][package])
        self.base_domain = self.raw.get('base_domain', '')

    def __getattr__(self, name):
        global mapper
        hs = self.hs
        if name in mapper:
            if mapper[name][0] in self.__dict__:
                return self.__dict__[mapper[name][0]]
            hs.log.debug(f'build config: {name}')
            self.__dict__[mapper[name][0]] = self.raw.get(mapper[name][1], {})
            if self.__dict__[mapper[name][0]] is None:
                self.__dict__[mapper[name][0]]
            if mapper[name][2]:
                val = self.__dict__[mapper[name][0]]
                self.__dict__[mapper[name][0]] = val.get(hs.name, {})
            else:
                self.__dict__[mapper[name][0]] = always_merger.merge(
                    self.__dict__[mapper[name][0]],
                    self.app.get(mapper[name][1], {}))
                if 'hosts' in self.app:
                    self.__dict__[mapper[name][0]] = always_merger.merge(
                        self.__dict__[mapper[name][0]],
                        self.app.get('hosts', {}).get(
                            hs.hostname, {}).get(mapper[name][1], {}))
            return self.__dict__[mapper[name][0]]
        return None
