import yaml

from homesystems.lib.asyncio_files import is_file, open


async def yaml_loader(hs, fname):
    if await is_file(fname):
        async with open(fname) as f:
            return yaml.safe_load(await f.read())
    else:
        hs.log.error(f"file not found '{fname}'")
        return {}


class NoAliasDumper(yaml.SafeDumper):
    def ignore_aliases(self, data):
        return True


async def yaml_safe(fname, data):
    async with open(fname, "w") as f:
        await f.write(yaml.dump(data, Dumper=NoAliasDumper))
