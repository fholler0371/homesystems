import os
import asyncio

import yaml


class Loader(yaml.SafeLoader):
    """YAML Loader with `!include` constructor."""

    def __init__(self, stream):
        """Initialise Loader."""
        try:
            self._root = os.path.split(stream.name)[0]
        except AttributeError:
            self._root = os.path.curdir
        super().__init__(stream)


def construct_include(loader: Loader, node: yaml.Node):
    """Include file referenced at node."""
    with open(os.path.abspath(os.path.join(loader._root,
                                           loader.construct_scalar(node))) +
              ".yaml", 'r') as f:
        return yaml.load(f, Loader)


def construct_secret(loader: Loader, node: yaml.Node):
    """Include file referenced at node."""
    with open(os.path.abspath(os.path.join(loader._root, "secrets")) +
              ".yaml", 'r') as f:
        return yaml.load(f, Loader).get(node.value, "")


def construct_list(loader: Loader, node: yaml.Node):
    """Include file referenced at node."""
    out = []
    folder = os.path.abspath(os.path.join(loader._root,
                                          loader.construct_scalar(node)))
    for file in os.listdir(folder):
        fname = os.path.join(folder, file)
        if os.path.isfile(fname) and fname.endswith(".yaml"):
            with open(fname, "r") as f:
                data = yaml.load(f, Loader)
                if "id" not in data:
                    data["id"] = file[:-5]
                out.append(data)
    return out


def construct_dict(loader: Loader, node: yaml.Node):
    """Include file referenced at node."""
    out = {}
    folder = os.path.abspath(os.path.join(loader._root,
                                          loader.construct_scalar(node)))
    for file in os.listdir(folder):
        fname = os.path.join(folder, file)
        if os.path.isfile(fname) and fname.endswith(".yaml"):
            with open(fname, "r") as f:
                out[file[:-5]] = yaml.load(f, Loader)
    return out


yaml.add_constructor('!include', construct_include, Loader)
yaml.add_constructor('!secret', construct_secret, Loader)
yaml.add_constructor('!include_dir_list', construct_list, Loader)
yaml.add_constructor('!include_dir_dict', construct_dict, Loader)


def wrapped(hs, fname):
    with open(fname) as f:
        return yaml.load(f, Loader=Loader)


def yaml_load(hs, fname):
    return asyncio.to_thread(wrapped, hs, fname)
