from multiprocessing import Queue as mp_Queue, Lock as mp_Lock
from threading import Thread, Lock as t_Lock
from collections import deque
from concurrent.futures import Future
from asyncio import wrap_future, wait_for
import time

FUTURE_FACTOR = 16


class QueueFull(Exception):
    def __init__(self, queue):
        self.msg = f'Cound not handle message {queue} is full'
        super().__init__(self.msg)


class FutureListFull(Exception):
    def __init__(self, queue):
        self.msg = f'Future list full {queue} is full'
        super().__init__(self.msg)


class mq_queue_connector(Thread):
    def __init__(self, q, put):
        Thread.__init__(self)
        self.daemon = True
        self.put = put
        self.q = q

    def run(self):
        while True:
            try:
                msg = self.q.get(timeout=10)
            except Exception as e:
                msg = None
            if msg is not None:
                try:
                    self.put(msg)
                except Exception as e:
                    print('queue line:41', repr(e))


class AsyncMpQueue:
    def __init__(self, hs, queue=None, rx_only=False, maxsize=0):
        self.hs = hs
        self.rx = None
        self.tx = None
        self.mp_mutex = None
        self.maxsize = maxsize
        if queue is not None:
            self.rx = queue.rx
            self.tx = queue.tx
            self.mp_mutex = queue.mp_mutex
            self.maxsize = queue.maxsize
        if self.rx is None:
            self.rx = mp_Queue(maxsize=self.maxsize)
        if self.tx is None and not rx_only:
            self.tx = mp_Queue(maxsize=self.maxsize)
        if self.mp_mutex is None:
            self.mp_mutex = mp_Lock()
        self.running = True
        ###################
        self.t_mutex = t_Lock()
        self.items = deque()
        self.maxitems = 16
        self.getters = deque()
        self.putters = deque()
        ###################
        self.mp_putters = deque()

###########################

    def mirror(self):
        cls = AsyncMpQueue(self.hs, queue=self)
        cls.tx, cls.rx = cls.rx, cls.tx
        return cls

###########################

    async def bind_async_to_mp_queue(self, hs):
        # self.t_mutex = t_Lock()
        self.hs = hs  # hs wird auf den aktuellen Prozess angepasst
        self.thread = mq_queue_connector(self.rx, self.put_nowait_rx)
        self.thread.start()

###########################
###########################

    def put_nowait(self, item):
        if self.maxsize > 0 and self.tx.qsize() >= self.maxsize:
            raise QueueFull('process_tx')
        self.tx.put(item)

###########################

    async def put(self, item):
        hs = self.hs
        fut = self._future_put(item)
        if fut:
            return await wait_for(wrap_future(fut, loop=hs.loop), None)
        return True

###########################
###########################

    def put_nowait_rx(self, item):
        # wrapper for better code reading
        while True:
            fut = self._put_noblock(item)
            if fut is None:
                return
            fut.result()

###########################
###########################

    def get_noblock(self):
        with self.t_mutex:
            if self.items:
                if self.putters:
                    self.putters.popleft().set_result(True)
                return self.items.popleft(), None
            else:
                fut = Future()
                self.getters.append(fut)
                return None, fut

###########################

    async def get(self):
        hs = self.hs
        item, fut = self.get_noblock()
        if fut:
            item = await wait_for(wrap_future(fut, loop=hs.loop), None)
        return item

##############################################################################
#
#  private functions
#
##############################################################################

    def _put_noblock(self, item):
        with self.t_mutex:
            if len(self.items) < self.maxitems:
                self.items.append(item)
                if self.getters:
                    self.getters.popleft().set_result(self.items.popleft())
            else:
                fut = Future()
                self.putters.append(fut)
                return Future()

###########################

    def _check_mp_putters(self):
        while (self.maxsize == 0 or self.tx.qsize() < self.maxsize) and \
                len(self.mp_putters) > 0:
            entry = self.mp_putters.popleft()
            self.tx.put(entry[1])
            entry[0].set_result(True)

###########################

    def _future_put(self, item):
        self._check_mp_putters()
        if self.maxsize > 0 and self.tx.qsize() >= self.maxsize:
            hs = self.hs
            hs.log.warn(f"Queue {hs.name} size {self.tx.qsize()}")
            if self.maxsize > 0 and \
                    len(self.mp_putters) >= self.maxsize*FUTURE_FACTOR:
                raise QueueFull('mp_putters')
                return None
            else:
                fut = Future()
                self.mp_putters.append((fut, item))
                return fut
        self.tx.put(item)
        return None
