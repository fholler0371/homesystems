import yaml
import os
import aiofiles

from homesystems.process import c_process_base
from homesystems.lib.helper import overload

#import time
#import os
#import sys

from pprint import pprint

from homesystems.const import (
#    EVENT_PROCESS_RUNNING,
    EVENT_PROCESS_STOPPING,
#    EVENT_RCP_PORT_GET
)

from .const import STATIC_DEVICE, DYNAMIC_DEVICE

from .const import (
    EVENT_SYSTEM_NEW_VALUE,
    EVENT_SYSTEM_STATIC_DATA_GET
)

from homesystems.lib.modul_states import (
    EVENT_STATES_DEVICE_ADD,
    EVENT_STATES_CHANNEL_ADD,
    EVENT_STATES_VALUE_NEW,
    EVENT_STATES_DEVICE_GET
)

from homesystems.lib.aio_eventbus import (
    EVENT_RESPONSE
)

from .const import (
    EVENT_SYSTEM_NEW_VALUE,
    EVENT_SYSTEM_STATIC_DATA_GET,
    EVENT_SYSTEM_DYNAMIC_HOST_GET
)

class c_modul(c_process_base):
    def __init__(self, *args, **kwargs):
        c_process_base.__init__(self, *args, **kwargs)
        self.modul_name = 'main'
        self._devices = {}
        self._filename = ""
        self._datachange = False
    
##############################################

    @overload
    async def doInit(self):
        hs = self.hs
#        await core.bus.add_listener(EVENT_PROCESS_RUNNING, self.doStart)
        await hs.bus.add_listener(EVENT_PROCESS_STOPPING, self.doStop)
        await hs.bus.add_listener(EVENT_SYSTEM_NEW_VALUE, self.new_value)
        await hs.bus.add_listener(EVENT_SYSTEM_STATIC_DATA_GET, self.static_data_get)
        await hs.bus.add_listener(EVENT_SYSTEM_DYNAMIC_HOST_GET, self.dynamic_hosts_get)
        
##############################################
##############################################

    async def new_value(self, msg):
        hs = self.hs
        device, channel = msg.sensor.split('.')
        if device not in self._devices or not (self._devices[device]) :
            self._devices[device] = True
            self._datachange = True
            if device.endswith('_static'):
                host = device.split('_')[0]
                label = hs.config.data.get('hosts', {}).get(host, '')
                if label == '':
                    hs.log.error(f"host label not defined: {host}")
                    return
                await hs.bus.emit(EVENT_STATES_DEVICE_ADD, context={'name': device, 'label': label, 'type': STATIC_DEVICE['type']})
                for ch_data in STATIC_DEVICE['channels']:
                    await hs.bus.emit(EVENT_STATES_CHANNEL_ADD, sensor=device, context=ch_data)
            elif device.endswith('_dynamic'):
                host = device.split('_')[0]
                label = hs.config.data.get('hosts', {}).get(host, '')
                if label == '':
                    hs.log.error(f"host label not defined: {host}")
                    return
                await hs.bus.emit(EVENT_STATES_DEVICE_ADD, context={'name': device, 'label': label, 'type': DYNAMIC_DEVICE['type']})
                for ch_data in DYNAMIC_DEVICE['channels']:
                    await hs.bus.emit(EVENT_STATES_CHANNEL_ADD, sensor=device, context=ch_data)
        await hs.bus.emit(EVENT_STATES_VALUE_NEW, sensor=msg.sensor, context={'value': msg.context['value']})

##############################################

    async def static_data_get(self, msg):
        hs = self.hs
        out = []
        for device in self._devices:
           if device.endswith('_static'):
                resp = await hs.bus.emit_wait(EVENT_STATES_DEVICE_GET, sensor=device)
                if resp != {}:
                    out.append(resp)
        out = sorted(out, key=lambda d: d['label']) 
        await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                            context={'result': out}, \
                                            target_app=msg.context['app'])

##############################################

    async def dynamic_hosts_get(self, msg):
        hs = self.hs
        out = []
        for device in self._devices:
            if device.endswith('_dynamic'):
                resp = await hs.bus.emit_wait(EVENT_STATES_DEVICE_GET, sensor=device)
                if resp != {}:
                    out.append({'device': device, 'label': resp['label']})
        out = sorted(out, key=lambda d: d['label']) 
        await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                            context={'result': out}, \
                                            target_app=msg.context['app'])

##############################################
##############################################

    @overload
    async def doStart(self):
        hs = self.hs
        self._filename = f"{hs.path.data}/{hs.manifest['name']}.yaml"
        try:
            if os.path.isfile(self._filename):
                with open(self._filename, "r") as yaml_file:
                    self._devices = yaml.load(yaml_file, Loader=yaml.SafeLoader)
                for device in self._devices:
                    self._devices[device] = False
        except:
            pass
        await hs.run_delay(3600, self.save)

##############################################

    async def save(self):
        hs = self.hs
        if self.running:
            await hs.run_delay(3600, self.save)
        if self._datachange:
            with open(self._filename, 'w') as yaml_file:
                yaml.dump(self._devices, yaml_file, default_flow_style=False)
            self._datachange = False

##############################################

    @overload
    async def doStop(self, msg):
        self.running = False
        await self.save()
        self.stopped = True
