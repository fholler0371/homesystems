EVENT_SYSTEM_NEW_VALUE = "systemnewvalue"
EVENT_SYSTEM_STATIC_DATA_GET = "system_static_get"
EVENT_SYSTEM_DYNAMIC_HOST_GET = "system_dynamic_hosts_get"

STATIC_DEVICE = {'type': 'system_static',
                 'channels': [{'name': 'pyversion', 'type': 'str'},
                              {'name': 'hostname', 'type': 'str'},
                              {'name': 'ip', 'type': 'str'},
                              {'name': 'kernel', 'type': 'str'},
                              {'name': 'release', 'type': 'str'},
                              {'name': 'board', 'type': 'str'},
                              {'name': 'serial', 'type': 'str'},
                              {'name': 'board', 'type': 'str'},
                              {'name': 'revision', 'type': 'str'}]}

DYNAMIC_DEVICE = {'type': 'system_dynamic',
                  'channels': [{'name': 'temperature', 'type': 'float'},
                               {'name': 'cpu_freq', 'type': 'float'},
                               {'name': 'load5', 'type': 'float'},                               
                               {'name': 'load15', 'type': 'float'},                               
                               {'name': 'uptime', 'type': 'int'},
                               {'name': 'runtime', 'type': 'int'},
                               {'name': 'apt', 'type': 'int'},                                             
                               {'name': 'git', 'type': 'int'},
                               {'name': 'net_tx', 'type': 'float'},                                                                                        
                               {'name': 'net_rx', 'type': 'float'}]}              