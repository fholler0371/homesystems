import os
import aiosqlite
import asyncio
import shutil
from datetime import date, timedelta
from decimal import Decimal, getcontext

from homesystems.process import c_process_base
from homesystems.lib.helper import overload

from homesystems.lib.aio_mqtt import (
    EVENT_MQTT_CONNECT,
    EVENT_MQTT_CONNECTED,
    EVENT_MQTT_SUBSCRIBE,
    EVENT_MQTT_MESSAGE,
    EVENT_MQTT_PUBLISH
)

from homesystems.lib.modul_states import (
    EVENT_STATES_DEVICE_ADD,
    EVENT_STATES_CHANNEL_ADD,
    EVENT_STATES_VALUE_NEW,
    EVENT_STATES_DEVICE_GET,
    EVENT_STATES_CHANNEL_GET,
    EVENT_STATES_CHANNEL_SET_VALUE
)

from homesystems.components.history_day import (
    EVENT_HISTORY_DAY_PUSH
)

from .const import DEVICES

DAYHISTORY_INTERVAL = 900

class c_modul(c_process_base):
    def __init__(self, *args, **kwargs):
        c_process_base.__init__(self, *args, **kwargs)
        self.modul_name = 'main'
        self._devices = {}
        self._ignored_types = []
        self._ignored_devices = []
    
##############################################

    @overload
    async def doInit(self):
        hs = self.hs
        await hs.bus.add_listener(EVENT_MQTT_CONNECTED, self.connected)
        await hs.bus.add_listener(EVENT_MQTT_MESSAGE, self.newValue)
        await hs.bus.add_listener(EVENT_STATES_CHANNEL_SET_VALUE, self.setValue)

##############################################
##############################################

    async def connected(self, _):
        hs = self.hs
        await hs.bus.emit(EVENT_MQTT_SUBSCRIBE, context={'value': 'shellies/#'})
        hs.log.debug('MQTT Connected')
        
##############################################

    async def newValue(self, msg):
        hs = self.hs
        device = msg.sensor.split("/")[1]
        if device not in self._devices:
            if not await self.newDevice(device):
                return 
        if (self._devices[device]['version'] == 1):
            channel = msg.sensor.split('/')[-1]
            if channel == "0":
                channel = msg.sensor.split('/')[-2]
            try:
                value = msg.context['value']
            except:
                value = 0
            found = False
            for cb_channel in DEVICES[self._devices[device]['type']]['channels']:
                #print(cb_channel, channel, cb_channel['topic'])
                #print(self._devices[device])
                if channel == cb_channel['topic']:
                    found = True
                    if "v_convert" in  cb_channel:
                        value = cb_channel["v_convert"](value)
                    await hs.bus.emit(EVENT_STATES_VALUE_NEW, sensor=f"{self._devices[device]['device']}.{cb_channel['name']}", 
                                      context={'value': value})
                    if 'follow_script' in cb_channel:
                        await getattr(self, cb_channel['follow_script'])(self._devices[device]['device'], cb_channel['name'], value)
                    #print(f"{self._devices[device]['device']}.{cb_channel['name']}", value)
            if not found:
                pass
                #print(device, channel, value)
        elif (self._devices[device]['version'] == 2):
            for cb_channel in DEVICES[self._devices[device]['type']]['channels']:
                value = msg.context
                try:
                    for a_topic in cb_channel['topic'].split('.'):
                        value = value[a_topic]
                except:
                    pass
                if not isinstance(value, dict):
                    #print(cb_channel)
                    if 'v_convert' in cb_channel:
                        value = cb_channel['v_convert'](value)
                    await hs.bus.emit(EVENT_STATES_VALUE_NEW, sensor=f"{self._devices[device]['device']}.{cb_channel['name']}", 
                                      context={'value': value})
                    if 'follow_script' in cb_channel:
                        await getattr(self, cb_channel['follow_script'])(self._devices[device]['device'], cb_channel['name'], value)
                    #print(self._devices[device]['device'], cb_channel['name'], value)
            #print(device)
            #print(self._devices[device])
            #print(msg.context)
                
##############################################

    async def calc_energy(self, device, channel, value):
        hs = self.hs
        try:
            #calc sum to care about device reset
            energy_device = await hs.bus.emit_wait(EVENT_STATES_CHANNEL_GET, sensor= f"{device}.{channel}")
            energy_history = await hs.bus.emit_wait(EVENT_STATES_CHANNEL_GET, sensor= f"{device}.energy_history")
            energy_sum = energy_history['value']
            if energy_device['value'] < energy_device['last']:
                energy_sum += energy_device['last']
                await hs.bus.emit(EVENT_STATES_VALUE_NEW, sensor=f"{device}.{channel}", context={'value': 0.000000001})
                await hs.bus.emit(EVENT_STATES_VALUE_NEW, sensor=f"{device}.{channel}", context={'value': value})
                await hs.bus.emit(EVENT_STATES_VALUE_NEW, sensor=f"{device}.energy_history", context={'value': energy_sum})
            energy_sum += energy_device['value']
            #calc today value
            energy_last = await hs.bus.emit_wait(EVENT_STATES_CHANNEL_GET, sensor= f"{device}.energy_last_yesterday")
            energy_today = energy_sum - energy_last['value']
            await hs.bus.emit(EVENT_STATES_VALUE_NEW, sensor=f"{device}.energy_today", context={'value': energy_today})
            #check date change
            cur_date = date.today().strftime('%Y%m%d')
            energy_date = await hs.bus.emit_wait(EVENT_STATES_CHANNEL_GET, sensor= f"{device}.date")
            if energy_date['value'] == "":
                await hs.bus.emit(EVENT_STATES_VALUE_NEW, sensor=f"{device}.date", context={'value': cur_date})
                await hs.bus.emit(EVENT_STATES_VALUE_NEW, sensor=f"{device}.energy_last_yesterday", context={'value': energy_sum})
            elif energy_date['value'] != cur_date:
                await hs.bus.emit(EVENT_STATES_VALUE_NEW, sensor=f"{device}.date", context={'value': cur_date})
                await hs.bus.emit(EVENT_STATES_VALUE_NEW, sensor=f"{device}.energy_yesterday", context={'value': energy_today})
                await hs.bus.emit(EVENT_STATES_VALUE_NEW, sensor=f"{device}.energy_last_yesterday", context={'value': energy_sum})
        except Exception as e:
            hs.log.warn(repr(e))

##############################################

    async def newDevice(self, device):
        hs = self.hs
        type = device.split('-')[0]
        if type not in DEVICES:
            if type not in self._ignored_types:
                hs.log.warn(f"unkonwn device-type {type}")
                self._ignored_types.append(type)
            return False
        cfg_devices = hs.config.data.get('devices', {})
        if device not in cfg_devices:
            if device not in self._ignored_devices:
                self._ignored_devices.append(device)
                hs.log.warn(f"unkonwn device {device}")
            return False
        _device_data = cfg_devices.get(device, device)
        if "|" in _device_data:
            name, label = _device_data.split("|", 1)
        self._devices[device] = {'id': device, 'type': type, 'version': DEVICES[type]['version'], 'device': name}        
        await hs.bus.emit(EVENT_STATES_DEVICE_ADD, context={'name': name, 'label': label, 'type': type})
        for ch_data in DEVICES[type]['channels']:
            await hs.bus.emit(EVENT_STATES_CHANNEL_ADD, sensor=name, context={'name': ch_data['name'], 'type': ch_data['type']})

##############################################

    async def setValue(self, msg):
        hs = self.hs
        device, channel = msg.sensor.split('.')
        value = msg.context['value']
        for label in self._devices:
            if self._devices[label]['device'] == device:
                if self._devices[label]['type'] in DEVICES:
                    if self._devices[label]['version'] == 1:
                        for ch_data in DEVICES[self._devices[label]['type']]['channels']:
                            if ch_data['name'] == channel:
                                if 'vs_convert' in ch_data:
                                    value = ch_data['vs_convert'](value)
                                topic = f"shellies/{label}{ch_data['send_topic']}"
                                await hs.bus.emit(EVENT_MQTT_PUBLISH, sensor=topic, context={'value': value})
                    elif self._devices[label]['version'] == 2:
                        for ch_data in DEVICES[self._devices[label]['type']]['channels']:
                            if ch_data['name'] == channel:
                                topic = f"shellies/{label}/rpc"
                                payload = ch_data['vs_convert'](value)
                                await hs.bus.emit(EVENT_MQTT_PUBLISH, sensor=topic, context={'value': payload})

##############################################
##############################################

    async def send_energy_history(self):
        hs = self.hs
        cur_date = date.today().strftime('%Y%m%d')
        last_date = (date.today()-timedelta(days=1)).strftime('%Y-%m-%d')
        if self.running:
            await hs.run_delay(DAYHISTORY_INTERVAL, self.send_energy_history)
        for device_id in self._devices:
            device = self._devices[device_id]
            if device['type'] in ['shellyplug', 'shellyplus1pm']: # Steckdosen
                energy_date = await hs.bus.emit_wait(EVENT_STATES_CHANNEL_GET, sensor= f"{device['device']}.date")
                if energy_date['value'] != cur_date:
                    await self.calc_energy(device['device'], 'energy_total', 0)
                else:
                    energy = await hs.bus.emit_wait(EVENT_STATES_CHANNEL_GET, sensor= f"{device['device']}.energy_yesterday")
                    if energy['value'] > 0:
                        await hs.bus.emit(EVENT_HISTORY_DAY_PUSH, sensor=f"{hs.manifest['name']}.{device['device']}.energy", 
                                        context={'value': energy['value'], 'date': last_date}, 
                                        target_app="*.history_day")

##############################################
##############################################

    @overload
    async def doStart(self):
        hs = self.hs
        await hs.bus.emit(EVENT_MQTT_CONNECT)
        await hs.run_delay(DAYHISTORY_INTERVAL // 10, self.send_energy_history)
#        await hs.run_delay(15, self.send_15) 
#        await hs.run_delay(60, self.send_60) 
#        await hs.run_delay(120, self.push_day_hist) 
#        await hs.run_delay(300, self.import_db_files)

