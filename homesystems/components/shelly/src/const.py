def onoff_int(value):
    if value == "on":
        return 1
    return 0

def int_onoff(value):
    if value == 1:
        return 'on'
    return 'off'

def bool_int(value):
    if value :
        return 1
    return 0

def energy(value):
    return float(value)/60

def out_switch_int_bool(value):
    _value = 'false'
    if value == 1:
        _value = 'true'
    return '{"method": "Switch.Set", "params": {"id": 0, "on": '+_value+'}}'

DEVICES = {'shellyplug': {'version': 1,
                          'channels': [{'name': 'power', 'type': 'float', 'topic': 'power'},
                                       {'name': 'energy_last', 'type': 'float', 'topic': 'energy', 'v_convert': energy, 
                                        'follow_script': 'calc_energy'},
                                       {'name': 'state', 'type': 'int', 'topic': 'relay', 'v_convert': onoff_int, 
                                        'send_topic': '/relay/0/command', 'vs_convert': int_onoff},
                                       {'name': 'energy_history', 'type': 'float', 'topic': ''},
                                       {'name': 'energy_last_yesterday', 'type': 'float', 'topic': ''},
                                       {'name': 'energy_today', 'type': 'float', 'topic': ''},
                                       {'name': 'date', 'type': 'str', 'topic': ''},
                                       {'name': 'energy_yesterday', 'type': 'float', 'topic': ''}]
                          },
           'shellyplus1pm': {'version': 2,
                             'channels': [{'name': 'state', 'type': 'int', 'topic': 'output', 'v_convert': bool_int,
                                           'vs_convert': out_switch_int_bool},
                                          {'name': 'power', 'type': 'float', 'topic': 'apower'},
                                          {'name': 'voltage', 'type': 'float', 'topic': 'voltage'},
                                          {'name': 'current', 'type': 'float', 'topic': 'current'},
                                          {'name': 'energy_last', 'type': 'float', 'topic': 'aenergy.total', 
                                           'follow_script': 'calc_energy'},
                                          {'name': 'energy_history', 'type': 'float', 'topic': ''},
                                          {'name': 'energy_last_yesterday', 'type': 'float', 'topic': ''},
                                          {'name': 'energy_today', 'type': 'float', 'topic': ''},
                                          {'name': 'date', 'type': 'str', 'topic': ''},
                                          {'name': 'energy_yesterday', 'type': 'float', 'topic': ''}]}}