import os
import aiosqlite
import asyncio
import shutil
from datetime import date
from decimal import Decimal, getcontext

from homesystems.process import c_process_base
from homesystems.lib.helper import overload

from homesystems.lib.aio_mqtt import (
    EVENT_MQTT_CONNECT,
    EVENT_MQTT_CONNECTED,
    EVENT_MQTT_SUBSCRIBE,
    EVENT_MQTT_MESSAGE
)

from homesystems.lib.modul_states import (
    EVENT_STATES_DEVICE_ADD,
    EVENT_STATES_CHANNEL_ADD,
    EVENT_STATES_VALUE_NEW,
    EVENT_STATES_DEVICE_GET
)

from homesystems.components.history_day import (
    EVENT_HISTORY_DAY_PUSH
)

class c_modul(c_process_base):
    def __init__(self, *args, **kwargs):
        c_process_base.__init__(self, *args, **kwargs)
        self.modul_name = 'main'
        self.val_15 = {}
        self.val_60 = {}
        self.etot = 0
        self.today = ''
        self.value_yesterday = 0
    
##############################################

    @overload
    async def doInit(self):
        hs = self.hs
        await hs.bus.add_listener(EVENT_MQTT_CONNECTED, self.connected)
        await hs.bus.add_listener(EVENT_MQTT_MESSAGE, self.newValue)

##############################################
##############################################

    async def connected(self, _):
        hs = self.hs
        await hs.bus.emit(EVENT_MQTT_SUBSCRIBE, context={'value': 'smartpi/status/#'})
        hs.log.debug('MQTT Connected')

##############################################

    async def newValue(self, msg):
        hs = self.hs
        channel = msg.sensor.split('/')[-1].lower()
        value = float(msg.context['value'])
        if channel not in ['ec1m', 'ectot', 'ep1m', 'eptot', 'ec1', 'ec2', 'ec3', 'ep1', 'ep2', 'ep3'] :
            if channel not in self.val_15:
                self.val_15[channel] = {'sum': value, 'count': 1}
            else: 
                self.val_15[channel]['sum'] += value
                self.val_15[channel]['count'] += 1
            if channel not in self.val_60:
                self.val_60[channel] = {'sum': value, 'count': 1}
            else: 
                self.val_60[channel]['sum'] += value
                self.val_60[channel]['count'] += 1
        if channel in ['ec1m', 'ectot']:
            if channel == 'ectot':
                await self.energy_history(value)
            
##############################################

    async def energy_history(self, value):
        hs = self.hs
        daily_history = None
        today = date.today()
        if self.today == "":
            daily_history = await hs.bus.emit_wait(EVENT_STATES_DEVICE_GET, sensor='history_daily')
            if daily_history['channels']['date_cur']['value'] == '':
                await hs.bus.emit(EVENT_STATES_VALUE_NEW, sensor='history_daily.date_cur', 
                                                            context={'value': today})
                self.today = today
            else:
                self.today = daily_history['channels']['date_cur']['value']
                self.value_yesterday = daily_history['channels']['value_yesterday']['value']
        if str(self.today) != str(today):
            self.today = today
            await hs.bus.emit(EVENT_STATES_VALUE_NEW, sensor='history_daily.date_cur', 
                                                            context={'value': today})
            if daily_history is None:
                daily_history = await hs.bus.emit_wait(EVENT_STATES_DEVICE_GET, sensor='history_daily')
            if daily_history['channels']['value_yesterday']['value'] > 0:
                diff = value - daily_history['channels']['value_yesterday']['value']
                if diff > 0 and daily_history['channels']['value_yesterday']['value']:
                    await hs.bus.emit(EVENT_STATES_VALUE_NEW, sensor='history_daily.value_1', 
                                                                context={'value': diff})
            await hs.bus.emit(EVENT_STATES_VALUE_NEW, sensor='history_daily.value_yesterday', 
                                                        context={'value': value})
            self.value_yesterday = value
        diff = value - self.value_yesterday
        if diff > 0 and self.value_yesterday > 0:
            await hs.bus.emit(EVENT_STATES_VALUE_NEW, sensor='history_daily.value_cur', 
                                                        context={'value': diff})

##############################################
##############################################

    async def send_15(self):
        hs = self.hs
        await hs.run_delay(15, self.send_15) 
        for sensor in self.val_15:
            try:
                await hs.bus.emit(EVENT_STATES_VALUE_NEW, sensor=f"main_15.{sensor}", 
                                                            context={'value': Decimal(self.val_15[sensor]['sum']/self.val_15[sensor]['count'])})
                self.val_15[sensor] = {'sum': 0, 'count': 0}
            except Exception as e:
                print('sensor', sensor)

##############################################

    async def send_60(self):
        hs = self.hs
        await hs.run_delay(60, self.send_60) 
        for sensor in self.val_60:
            try:
                await hs.bus.emit(EVENT_STATES_VALUE_NEW, sensor=f"main_60.{sensor}", 
                                                            context={'value': Decimal(self.val_60[sensor]['sum']/self.val_60[sensor]['count'])})
                self.val_60[sensor] = {'sum': 0, 'count': 0}
            except:
                print('sensor', sensor)

##############################################

    async def push_day_hist(self):
        hs = self.hs
        await hs.run_delay(3600, self.push_day_hist) 
        daily_history = await hs.bus.emit_wait(EVENT_STATES_DEVICE_GET, sensor='history_daily')
        date = daily_history['channels']['date_cur']['last']
        value = daily_history['channels']['value_1']['value']
        await hs.bus.emit(EVENT_HISTORY_DAY_PUSH, sensor=f"{hs.manifest['name']}.history_daily.value",
                                                    context={'value': value, 'date': date}, target_app="*.history_day") 

##############################################

    async def import_db_files(self):
        hs = self.hs
        try:
            files = os.listdir(f"{hs.path.db}/import")
            for file in files:
                table = file.split('.')[0]
                hs.log.debug(f'start log file import: {file}')
                async with aiosqlite.connect(f"{hs.path.db}/import/{file}") as db:
                    try:
                        async with db.execute(f"select substr(date,0,11), sum(energy_pos_balanced) from {table} group by substr(date,0,12)") as cursor:
                            async for row in cursor:
                                await hs.bus.emit(EVENT_HISTORY_DAY_PUSH, sensor=f"{hs.manifest['name']}.history_daily.value",
                                                                            context={'value': row[1], 'date': row[0]}, target_app="*.history_day") 
                                await asyncio.sleep(1) 
                    except:
                        async with db.execute(f"select substr(date,0,11), sum(power_1+power_2+power_3) from {table} group by substr(date,0,12)") as cursor:
                            async for row in cursor:
                                await hs.bus.emit(EVENT_HISTORY_DAY_PUSH, sensor=f"{hs.manifest['name']}.history_daily.value",
                                                                            context={'value': row[1]/30, 'date': row[0]}, target_app="*.history_day") 
                                await asyncio.sleep(1) 
                try:
                    shutil.move(f"{hs.path.db}/import/{file}", f"{hs.path.db}/archive/{file}")
                except:
                    shutil.move(f"{hs.path.db}/import/{file}", f"{hs.path.db}/archive/{file}.1")
        except Exception as e:
            print(repr(e))
        await hs.run_delay(300, self.import_db_files)

##############################################
##############################################

    async def init_states(self):
        hs = self.hs
        await hs.bus.emit(EVENT_STATES_DEVICE_ADD, context={'name': 'main_15', 'label': 'Main 15s', 'type': 'energy_main'})
        await hs.bus.emit(EVENT_STATES_DEVICE_ADD, context={'name': 'main_60', 'label': 'Main 1m', 'type': 'energy_main'})
        for device in ['main_15', 'main_60']: 
            for i in range(1,4):
                await hs.bus.emit(EVENT_STATES_CHANNEL_ADD, sensor=device, context={'name': f"i{i}", 'type': 'decimal'})
                await hs.bus.emit(EVENT_STATES_CHANNEL_ADD, sensor=device, context={'name': f"v{i}", 'type': 'decimal'})
                await hs.bus.emit(EVENT_STATES_CHANNEL_ADD, sensor=device, context={'name': f"p{i}", 'type': 'decimal'})
                await hs.bus.emit(EVENT_STATES_CHANNEL_ADD, sensor=device, context={'name': f"cos{i}", 'type': 'decimal'})
                await hs.bus.emit(EVENT_STATES_CHANNEL_ADD, sensor=device, context={'name': f"f{i}", 'type': 'decimal'})
            await hs.bus.emit(EVENT_STATES_CHANNEL_ADD, sensor=device, context={'name': f"i4", 'type': 'decimal'})
            await hs.bus.emit(EVENT_STATES_CHANNEL_ADD, sensor=device, context={'name': f"ptot", 'type': 'decimal'})
        await hs.bus.emit(EVENT_STATES_DEVICE_ADD, context={'name': 'history_daily', 'label': 'Tag', 'type': 'history_daily'})
        await hs.bus.emit(EVENT_STATES_CHANNEL_ADD, sensor='history_daily', context={'name': "date_cur", 'type': 'str'})
        await hs.bus.emit(EVENT_STATES_CHANNEL_ADD, sensor='history_daily', context={'name': "value_yesterday", 'type': 'decimal'})
        await hs.bus.emit(EVENT_STATES_CHANNEL_ADD, sensor='history_daily', context={'name': "value_cur", 'type': 'decimal'})
        await hs.bus.emit(EVENT_STATES_CHANNEL_ADD, sensor='history_daily', context={'name': "value_1", 'type': 'decimal'})

##############################################

    @overload
    async def doStart(self):
        hs = self.hs
        await hs.bus.emit(EVENT_MQTT_CONNECT)
        await self.init_states()
        await hs.run_delay(15, self.send_15) 
        await hs.run_delay(60, self.send_60) 
        await hs.run_delay(120, self.push_day_hist) 
        await hs.run_delay(300, self.import_db_files)

