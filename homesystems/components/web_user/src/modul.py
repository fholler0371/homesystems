import os
import time
import aiohttp
import aiofiles
import mimetypes
import urllib.parse
import jwt

from homesystems.lib.aio_http import http_simple

from homesystems.process import c_process_base
from homesystems.lib.helper import overload

from homesystems.lib.aio_eventbus import (
    EVENT_RESPONSE
)

from homesystems.components.web_master import (
    EVENT_WEB_REGISTER_URL,
    EVENT_WEB_REGISTER_URL_DONE,

    EVENT_WEB_GET_ACTION
)

from homesystems.components.authentication import (
    EVENT_AUTH_APP_REGISTER,
    EVENT_AUTH_APP_SECRET_ID,
    EVENT_AUTH_USERS_GET,
    EVENT_AUTH_USER_ADD,
    EVENT_AUTH_USER_DEL,
    EVENT_AUTH_REC_GET,
    EVENT_AUTH_REC_SET,
    EVENT_AUTH_PASSWORD,
    EVENT_AUTH_MFA_GET,
    EVENT_AUTH_RIGHTS_SET,
    EVENT_TRANSFERTOKEN_CHECK
)

from homesystems.components.web_main import (
    EVENT_WEB_MAIN_REGISTER,
    EVENT_WEB_MAIN_REGISTERED
)

class c_modul(c_process_base):
    def __init__(self, *args, **kwargs):
        c_process_base.__init__(self, *args, **kwargs)
        self.modul_name = 'main'
        self.http_port = -1
        self.domain_registered = False
        self.auth_registered = False
        self.menu_registered = False
        self.secret_id = ''
        self.auth_domain = ''
        self.auth_path = ''
        self.public_key = None
    
##############################################

    @overload
    async def doInit(self):
        hs = self.hs
        await hs.bus.add_listener(EVENT_WEB_REGISTER_URL_DONE, self.domain_register_done)
        await hs.bus.add_listener(EVENT_WEB_GET_ACTION, self.handle_action)
        await hs.bus.add_listener(EVENT_AUTH_APP_SECRET_ID, self.set_secret_id)
        await hs.bus.add_listener(EVENT_WEB_MAIN_REGISTERED, self.menu_registered_done)

##############################################

    async def get_app_list(self, data):
        out = []
        for entry in data['scope'].split(' '):
            if entry in ['auth_manager', 'auth_users']:
                out.append({'url': f"/js/{entry}", 'modul': entry})
        return {'user': data['firstname'], 'modul': out}

##############################################

    async def decode_token(self, token):
        hs = self.hs
        try: 
            key = "\n".join([l.lstrip() for l in self.public_key.split("\n")])
            data = jwt.decode(token, key, algorithms=['RS256'])
            if data['expire'] > time.time():
                return data        #await self.main_menu_register()
        except:
            pass
        return None

##############################################
 
    async def users_get(self):
        hs = self.hs
        resp_data = await hs.bus.emit_wait(EVENT_AUTH_USERS_GET, target_app="*.authentication", 
                                           context={'app': f"{hs.hostname}.{hs.manifest['name']}"})
        return resp_data 

##############################################

    async def user_add(self, name):
        hs = self.hs
        await hs.bus.emit(EVENT_AUTH_USER_ADD, target_app="*.authentication", 
                                               context={'app': f"{hs.hostname}.{hs.manifest['name']}", 'name': name})

##############################################

    async def user_del(self, name):
        hs = self.hs
        await hs.bus.emit(EVENT_AUTH_USER_DEL, target_app="*.authentication", 
                                             context={'app': f"{hs.hostname}.{hs.manifest['name']}", 'name': name})

##############################################

    async def user_rec_get(self, name, rights=True):
        hs = self.hs
        resp = await hs.bus.emit_wait(EVENT_AUTH_REC_GET, target_app="*.authentication", 
                                      context={'app': f"{hs.hostname}.{hs.manifest['name']}", 'name': name, 'rights': rights})
        return resp

##############################################

    async def user_rec_set(self, name, firstname, lastname, mail):
        hs = self.hs
        await hs.bus.emit(EVENT_AUTH_REC_SET, target_app="*.authentication", 
                            context={'app': f"{hs.hostname}.{hs.manifest['name']}", 'name': name, 'firstname': firstname, 'lastname': lastname, 'mail': mail})

##############################################

    async def user_password(self, name, password):
        hs = self.hs
        await hs.bus.emit(EVENT_AUTH_PASSWORD, target_app="*.authentication", 
                            context={'app': f"{hs.hostname}.{hs.manifest['name']}", 'name': name, 'password': password})

##############################################

    async def user_mfa(self, name):
        hs = self.hs
        resp = await hs.bus.emit_wait(EVENT_AUTH_MFA_GET, target_app="*.authentication", 
                                        context={'app': f"{hs.hostname}.{hs.manifest['name']}", 'name': name})
        return resp

##############################################

    async def user_rights_set(self, name, rights, rights_mfa):
        hs = self.hs
        resp = await hs.bus.emit(EVENT_AUTH_RIGHTS_SET, target_app="*.authentication", 
                                  context={'app': f"{hs.hostname}.{hs.manifest['name']}", 'name': name, 'rights': rights, 'rights_mfa': rights_mfa})

##############################################

    async def check_transfer_token(self, token):
        hs = self.hs
        resp = await hs.bus.emit_wait(EVENT_TRANSFERTOKEN_CHECK, target_app="*.authentication", 
                                        context={'app': f"{hs.hostname}.{hs.manifest['name']}", 'token': token})
        return resp

##############################################

    async def handle_action(self, msg):
        hs = self.hs
        data = msg.context
        if data['action'] == 'get_login_url':
            url = f"https://{self.auth_domain}.{hs.config.base_domain}/{self.auth_path}?response_type=code"
            url += f"&client_id={hs.config.data['client_id']}"
            url += f"&redirect_uri="
            url += urllib.parse.quote_plus(f"https://{hs.config.data['http_domain']}.{hs.config.base_domain}/auth_callback")
            url += f"&scope={'+'.join(hs.config.data['scopes'])}&state="
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                              context={'result': {'url': url}}, \
                              target_app= data['response_app'])
        elif data['action'] == 'check_code':
            url = f"https://{self.auth_domain}.{hs.config.base_domain}/oauth/token"
            post_data = f"grant_type=authorization_code&code={data['code']}"
            post_data += f"&client_id={hs.config.data['client_id']}&client_secret={self.secret_id}"
            async with aiohttp.ClientSession() as session:
                async with session.post(url, data=post_data) as resp:
                    await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                        context={'result': await resp.json()}, \
                        target_app= data['response_app'])
        elif data['action'] == 'refresh_token':
            url = f"https://{self.auth_domain}.{hs.config.base_domain}/oauth/token"
            post_data = f"grant_type=refresh_token&refresh_token={data['refresh_token']}"
            post_data += f"&client_id={hs.config.data['client_id']}&client_secret={self.secret_id}"
            async with aiohttp.ClientSession() as session:
                async with session.post(url, data=post_data) as resp:
                    await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                        context={'result': await resp.json()}, \
                        target_app= data['response_app'])
        elif data['action'] == 'check_transfer_token':
            resp_data = await self.check_transfer_token(data['transfer_token'])
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                              context={'result': resp_data}, \
                              target_app= data['response_app'])
        elif data['action'] == 'get_module':
            token_data = await self.decode_token(data['token'])
            if token_data is None:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                  context={'result': {'error': "token invalid"}}, \
                                  target_app= data['response_app'])
                return
            resp_data = await self.get_app_list(token_data)
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                              context={'result': resp_data}, \
                              target_app= data['response_app'])
        elif data['action'] == 'users_get':
            token_data = await self.decode_token(data['token'])
            if token_data is None:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "token invalid"}}, \
                                    target_app= data['response_app'])
                return
            if 'auth_manager' not in token_data['scope']:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "scope invalid"}}, \
                                    target_app= data['response_app'])
            resp_data = await self.users_get()
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                context={'result': resp_data}, \
                                target_app= data['response_app'])
        elif data['action'] == 'user_add':
            token_data = await self.decode_token(data['token'])
            if token_data is None:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "token invalid"}}, \
                                    target_app= data['response_app'])
                return
            if 'auth_manager' not in token_data['scope']:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "scope invalid"}}, \
                                    target_app= data['response_app'])
            await self.user_add(data['user'])
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'ok': True}}, \
                                    target_app= data['response_app'])
        elif data['action'] == 'user_del':
            token_data = await self.decode_token(data['token'])
            if token_data is None:
                await core.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "token invalid"}}, \
                                    target_app= data['response_app'])
                return
            if 'auth_manager' not in token_data['scope']:
                await core.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "scope invalid"}}, \
                                    target_app= data['response_app'])
            await self.user_del(data['user'])
            await core.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'ok': True}}, \
                                    target_app= data['response_app'])
        elif data['action'] == 'user_rec_get':
            token_data = await self.decode_token(data['token'])
            if token_data is None:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "token invalid"}}, \
                                    target_app= data['response_app'])
                return
            if 'auth_manager' not in token_data['scope']:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "scope invalid"}}, \
                                    target_app= data['response_app'])
            resp_data = await self.user_rec_get(data['name'])
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                context={'result': resp_data}, \
                                target_app= data['response_app'])
        elif data['action'] == 'user_rec_set':
            token_data = await self.decode_token(data['token'])
            if token_data is None:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "token invalid"}}, \
                                    target_app= data['response_app'])
                return
            if 'auth_manager' not in token_data['scope']:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "scope invalid"}}, \
                                    target_app= data['response_app'])
            await self.user_rec_set(data['name'], data['firstname'], data['lastname'], data['mail'])
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                context={'result': {'ok': True}}, \
                                target_app= data['response_app'])
        elif data['action'] == 'user_password':
            token_data = await self.decode_token(data['token'])
            if token_data is None:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "token invalid"}}, \
                                    target_app= data['response_app'])
                return
            if 'auth_manager' not in token_data['scope']:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "scope invalid"}}, \
                                    target_app= data['response_app'])
            await self.user_password(data['name'], data['password'])
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                context={'result': {'ok': True}}, \
                                target_app= data['response_app'])
        elif data['action'] == 'user_mfa':
            token_data = await self.decode_token(data['token'])
            if token_data is None:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "token invalid"}}, \
                                    target_app= data['response_app'])
                return
            if 'auth_manager' not in token_data['scope']:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "scope invalid"}}, \
                                    target_app= data['response_app'])
            resp = await self.user_mfa(data['name'])
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                context={'result': resp}, \
                                target_app= data['response_app'])
        elif data['action'] == 'user_rights_set':
            token_data = await self.decode_token(data['token'])
            if token_data is None:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "token invalid"}}, \
                                    target_app= data['response_app'])
                return
            if 'auth_manager' not in token_data['scope']:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "token invalid"}}, \
                                    target_app= data['response_app'])
                return
            resp_data = await self.user_rec_get(token_data['name'], rights=False)
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                context={'result': resp_data}, \
                                target_app= data['response_app'])
        elif data['action'] == 'owner_rec_get':
            token_data = await self.decode_token(data['token'])
            if token_data is None:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "token invalid"}}, \
                                    target_app= data['response_app'])
                return
            if 'auth_users' not in token_data['scope']:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "scope invalid"}}, \
                                    target_app= data['response_app'])
            resp_data = await self.user_rec_get(token_data['name'], rights=False)
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                context={'result': resp_data}, \
                                target_app= data['response_app'])
        elif data['action'] == 'owner_rec_set':
            token_data = await self.decode_token(data['token'])
            if token_data is None:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "token invalid"}}, \
                                    target_app= data['response_app'])
                return
            if 'auth_users' not in token_data['scope']:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "scope invalid"}}, \
                                    target_app= data['response_app'])
            await self.user_rec_set(token_data['name'], data['firstname'], data['lastname'], data['mail'])
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                context={'result': {'ok': True}}, \
                                target_app= data['response_app'])
        elif data['action'] == 'owner_password':
            token_data = await self.decode_token(data['token'])
            if token_data is None:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "token invalid"}}, \
                                    target_app= data['response_app'])
                return
            if 'auth_users' not in token_data['scope']:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "scope invalid"}}, \
                                    target_app= data['response_app'])
            await self.user_password(token_data['name'], data['password'])
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                context={'result': {'ok': True}}, \
                                target_app= data['response_app'])
        else:
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                context={'result': {'error': f"'{data['action']}' not handled"}}, \
                                target_app= data['response_app'])

##############################################

    async def handler(self, request):
        hs = self.hs
        headers = {}
        for key in request.headers:
            headers[key] = request.headers[key]
        if request.method == 'GET':
            url_path = request.path
            if url_path == '/':
                url_path = '/index.html'
            elif url_path == '/auth_callback':
                url_path = '/index.html'
            elif url_path == '/favicon.ico':
                url_path = '/account.svg'
            elif url_path == '/js/auth_manager.js':
                url_path = '/auth_manager.js'
            elif url_path == '/js/auth_users.js':
                url_path = '/auth_users.js'
            path = f"{hs.path.comp}/{hs.manifest['name']}/web{url_path}"
            if os.path.isfile(path):
                r = aiohttp.web.StreamResponse(headers={'Content-Type': mimetypes.guess_type(path)[0]})
                await r.prepare(request)
                try:
                    async with aiofiles.open(path, mode='rb') as f:
                        if url_path == '/index.html':
                            content = (await f.read()).decode()
                            params = request.rel_url.query
                            do_in = '<script>\n'
                            for key in params:
                                do_in += f'      var data_{key} = "{params[key]}"\n'
                            do_in += '    </script>'
                            content.replace('<script></script>', do_in)
                            await r.write(content.replace('<script></script>', do_in).encode())
                        else:
                            await r.write(await f.read())
                    await r.write_eof()
                    return r
                except aiohttp.ClientConnectorError:
                    pass
                except ConnectionResetError:
                    pass
                except Exception as e:
                    hs.log.error(repr(e))
        else:
            print(request.method)
        return aiohttp.web.Response(text="OK", status=999)

##############################################
##############################################

    async def domain_register(self):
        hs = self.hs
        await hs.bus.emit(EVENT_WEB_REGISTER_URL, target_app="*.web_master", 
                          context={'app': f"{hs.hostname}.{hs.manifest['name']}", 
                                   'domain': f"{hs.config.data.get('http_domain', 'noname')}", 
                                   'url':f"http://{hs.ip}:{self.http_port}"})
        if self.domain_registered:
            await hs.run_delay(43200, self.domain_register)
        else:
            await hs.run_delay(15, self.domain_register)

##############################################

    async def domain_register_done(self, msg):
        self.domain_registered = True

##############################################
##############################################

    async def auth_register(self):
        hs = self.hs
        await hs.bus.emit(EVENT_AUTH_APP_REGISTER, target_app="*.authentication", 
                          context={'app': f"{hs.hostname}.{hs.manifest['name']}", 
                                   'client_id': hs.config.data['client_id'], 
                                   'scopes': hs.config.data['scopes'],
                                   'label': hs.config.data['label'],
                                   'icon': hs.config.data['icon'],
                                   'callback': f"https://{hs.config.data['http_domain']}.{hs.config.base_domain}/auth_callback"})
        if self.auth_registered:
            await hs.run_delay(900, self.auth_register)
        else:
            await hs.run_delay(15, self.auth_register)

##############################################

    async def set_secret_id(self, msg):
        hs = self.hs
        self.secret_id = msg.context['secret_id']
        self.auth_domain = msg.context['domain']
        self.auth_path = msg.context['path']
        self.public_key = msg.context['public_key']
        self.auth_registered = True

##############################################
##############################################

    async def main_menu_register(self):
        hs = self.hs
        await hs.bus.emit(EVENT_WEB_MAIN_REGISTER, target_app="*.web_main", 
                            context={'app': f"{hs.hostname}.{hs.manifest['name']}",
                                     'domain': hs.config.data['http_domain'], 
                                     'scopes': hs.config.data['scopes'],
                                     'label': hs.config.data['label'],
                                     'icon': hs.config.data['icon']})
        if self.menu_registered:
            await hs.run_delay(900, self.main_menu_register)
        else:
            await hs.run_delay(15, self.main_menu_register)

##############################################

    async def menu_registered_done(self, msg):
        self.menu_registered = True

##############################################
##############################################

    @overload
    async def doStart(self):
        hs = self.hs
        self.http_port = hs.reserved_ports[0]
        await self.domain_register()
        await self.auth_register()
        await self.main_menu_register()
        try:
            self.server = http_simple(hs.log, self.http_port, self.handler)
            await self.server.setup()
        except Exception as e:
            hs.log.error(repr(e))
            hs.kill.set()
        hs.log.info(f"{hs.name} running http server on port: {self.http_port}")

