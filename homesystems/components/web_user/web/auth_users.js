define(function () {
  var _modul = {
    icon: '/js/img/mdi/account.svg',
    already_init : false,
    label: 'Nutzereinstellungen',
    init: function() {
      if (!modul.auth_users.already_init) {
        modul.auth_users.already_init = true
      }
    },
    show: function() {
      console.log('show auth_user')
      $('#header_appname').text(modul.auth_users.label)
      require(['jqxinput', 'jqxbuttons', 'jqxpasswordinput'], function() {
        if (!($('#content_modul_auth_user').length)) {
          var html = '<div id="content_modul_auth_user" class="content_modul jqx-widget-content-material"></div>'
          $(".main_content").append(html)
          var html = '<table style="border-collapse: collapse; margin-left: 10px" class=><tr><td style="height: 16px"></td>'          
          html += '<tr><td>Vorname: </td><td><input typ="text" id="auth_user_firstname"></td><td></td></tr>'
          html += '<tr><td style="height: 16px"></td></tr><tr><td>Nachname: </td><td><input typ="text" id="auth_user_lastname"></td>'
          html += '<td></td></tr><tr><td style="height: 16px"></td></tr><tr><td>E-Mail: </td>'
          html += '<td><input typ="text" id="auth_user_mail"></td><td><input type="button" value="Aktualiesieren" id="auth_user_rec_send" />'
          html += '</td></tr><tr style="border-bottom: 1pt solid #99c"><td style="height: 4px"></td></tr><tr><td style="height: 16px"></td></tr>'
          html += '<tr><td>Passwort: </td><td><input type="password" id="auth_user_password"></td><td>'
          html += '<input type="button" value="Senden" id="auth_user_password_send" /></td></tr></table>'
          $('#content_modul_auth_user').append(html)
          $('#auth_user_firstname').jqxInput({placeHolder: "Vorname", height: 30, width: 300, minLength: 1, theme: 'material' })
          $('#auth_user_lastname').jqxInput({placeHolder: "Nachname", height: 30, width: 300, minLength: 1, theme: 'material' })
          $('#auth_user_mail').jqxInput({placeHolder: "E-Mail", height: 30, width: 300, minLength: 1, theme: 'material' })
          $('#auth_user_password').jqxPasswordInput({placeHolder: "Passwort", height: 30, width: 300, minLength: 1, theme: 'material' })
          $("#auth_user_rec_send, #auth_user_password_send")
              .jqxButton({ width: 120, height: 40, theme: 'material' })
          $("#auth_user_rec_send").on('click', function() {
            var firstname = $('#auth_user_firstname').jqxInput('val'),
                lastname = $('#auth_user_lastname').jqxInput('val'),
                mail = $('#auth_user_mail').jqxInput('val')
            postData('/api', { 'action': 'owner_rec_set', 'firstname': firstname, 
                                                         'lastname': lastname, 'mail': mail }, token=true).then(data => {
              if (data.error != null) {
                console.error(data)
              }
            })
          })
          $('#auth_user_password_send').on('click', function() {
            var password = $('#auth_user_password').jqxPasswordInput('val')
            postData('/api', { 'action': 'owner_password', 'password': password}, token=true).then(data => {
              if (data.error != null) {
                console.error(data)
              }
            })
          })
        }
        $('#content_modul_auth_user').show()
        postData('/api', { 'action': 'owner_rec_get' }, token=true).then(data => {
          if (data.error != null) {
            console.error(data)
          } else {
            if (data.firstname != null) {
              $('#auth_user_firstname').jqxInput('val', data.firstname)  
            } else {
              $('#auth_user_firstname').jqxInput('val', '')
            }
            if (data.lastname != null) {
              $('#auth_user_lastname').jqxInput('val', data.lastname)  
            } else {
              $('#auth_user_lastname').jqxInput('val', '')
            }
            if (data.mail != null) {
              $('#auth_user_mail').jqxInput('val', data.mail)  
            } else {
              $('#auth_user_mail').jqxInput('val', '')
            }
          }
        })
      })
    }
  }
  window.modul.auth_users = _modul
  console.log("auth_users")
  return _modul
})
  