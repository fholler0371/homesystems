define(function () {
  var _modul = {
    icon: '/js/img/mdi/energy_main.svg',
    already_init : false,
    label: 'Energie Verteiler',
    init: function() {
      if (!modul.energy_main.already_init) {
        modul.energy_main.already_init = true
      }
    },
    update_data : function() {
      if ($('#content_modul_energy_main').is(":visible")) {
        setTimeout(modul.energy_main.update_data, 30000)
        postData('/api', { 'action': 'main_get' }, token=true).then(data => {
          if (data.error != null) {
            console.error(data)
          } else {
            $('.sh_card_item_highlight').removeClass('sh_card_item_highlight')
            Object.keys(data).forEach(function(key) {
              var ele = $('#main_'+key)
              if (ele.length) {
                var span = $('#main_'+key+'>span')
                var last = span.text(),
                    cur = data[key].value
                try {
                  if (Math.abs((last/cur)-1) > 0.05) {
                    ele.addClass('sh_card_item_highlight')
                  }
                } catch (error) {}
                if (key.startsWith('p') || key.startsWith('v')) {
                  span.text(data[key].value.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1}))
                }
                if (key.startsWith('i') || key.startsWith('f') || key.startsWith('cos')) {
                  span.text(data[key].value.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}))
                }
              }
            })
          }
        })
      }
    },
    show: function() {
      console.log('show energy_main')
      $('#header_appname').text(modul.energy_main.label)
      require(['jqxtabs', 'inject'], function() {
        if (!($('#content_modul_energy_main').length)) {
          var html = '<div id="content_modul_energy_main" class="content_modul">'
          html += '<table class="sh_card sh_card_item_labels sh_table" ><tr><td></td><td style="width:25px"></td><td>Phase 1</td><td style="width:25px"></td>'
          html += '<td>Phase 2</td><td style="width:25px"></td><td>Phase 3</td><td style="width:25px"></td><td>Gesamt / (N)</td></tr>'
          html += '<tr><td>Leistung</td><td></td><td id="main_p1" class="sh_card_item_value"><span></span> W</td><td></td>'
          html += '<td id="main_p2" class="sh_card_item_value"><span></span> W</td><td></td>'
          html += '<td id="main_p3" class="sh_card_item_value"><span></span> W</td><td></td>'
          html += '<td id="main_ptot" class="sh_card_item_value"><span></span> W</td></tr>'
          html += '<tr><td>Spannung</td><td></td><td id="main_v1" class="sh_card_item_value"><span></span> V</td><td></td>'
          html += '<td id="main_v2" class="sh_card_item_value"><span></span> V</td><td></td>'
          html += '<td id="main_v3" class="sh_card_item_value"><span></span> V</td></tr>'
          html += '<tr><td>Strom</td><td></td><td id="main_i1" class="sh_card_item_value"><span></span> A</td><td></td>'
          html += '<td id="main_i2" class="sh_card_item_value"><span></span> A</td><td></td>'
          html += '<td id="main_i3" class="sh_card_item_value"><span></span> A</td><td></td>'
          html += '<td id="main_i4" class="sh_card_item_value"><span></span> A</td></tr>'
          html += '<tr><td>Frequenz</td><td></td><td id="main_f1" class="sh_card_item_value"><span></span> Hz</td><td></td>'
          html += '<td id="main_f2" class="sh_card_item_value"><span></span> Hz</td><td></td>'
          html += '<td id="main_f3" class="sh_card_item_value"><span></span> Hz</td></tr>'
          html += '<tr><td>Φ</td><td></td><td id="main_cos1" class="sh_card_item_value"><span></span></td><td></td>'
          html += '<td id="main_cos2" class="sh_card_item_value"><span></span></td><td></td>'
          html += '<td id="main_cos3" class="sh_card_item_value"><span></span></td></tr>'
          html += '</table>'
          html += '</div>'
          $(".main_content").append(html)
        }
        $('#content_modul_energy_main').show()
        var packery = $('#content_modul_energy_main').data('packery')
        if (!(packery == undefined)) {
          $('#content_modul_energy_main').data('packery').layout()
        }
      })
      setTimeout(this.update_data, 1200)
    }
  }
  window.modul.energy_main = _modul
  console.log("energy_main")
  return _modul
})