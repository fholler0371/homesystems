define(function () {
  var _modul = {
    icon: '/js/img/mdi/letter_m.svg',
    already_init : false,
    label: 'Monatlicher Verbrauch',
    init: function() {
      if (!modul.energy_month.already_init) {
        modul.energy_month.already_init = true
      }
    },
    update_data : function() {
      if ($('#content_modul_energy_month').is(":visible")) {
        setTimeout(modul.energy_month.update_data, 30000)
        postData('/api', { 'action': 'energy_month' }, token=true).then(data => {
          if (data.error != null) {
            console.error(data)
          } else {
            $('#modul_energy_month_value_0>span').text((data['days30']/1000).toLocaleString('de-DE', {minimumFractionDigits: 2, 
                                                                                                                 maximumFractionDigits: 2}))
            for (var i=0; i<25; i++) {
              var date = new Date(data.history[i].month)
              $('#modul_energy_month_label_'+i).text(date.toLocaleString('de-DE', {month: 'long'}))
              $('#modul_energy_month_value_'+(i+1)+'>span').text((data.history[i].value/1000).toLocaleString('de-DE', {minimumFractionDigits: 2, 
                                                                                                               maximumFractionDigits: 2})+' kW/h')
            }
          }
        })
      }
    },
    show: function() {
      console.log('show energy_month')
      $('#header_appname').text(modul.energy_month.label)
      require(['jqxtabs', 'inject'], function() {
        if (!($('#content_modul_energy_month').length)) {
          var html = '<div id="content_modul_energy_month" class="content_modul">'
          html += '<table class="sh_card sh_card_item_labels sh_table" ><tr><td></td><td style="width:25px"></td><td>Werte</td><td></td><td>12 Monate davor</td></tr>'
          html += '<tr><td>30-Tage</td><td></td><td id="modul_energy_month_value_0" class="sh_card_item_value"><span></span> kW/h</td></tr>'
          html += '<tr><td id="modul_energy_month_label_0"></td><td></td><td id="modul_energy_month_value_1" class="sh_card_item_value"><span></span></td>'
          html += '<td></td><td id="modul_energy_month_value_13" class="sh_card_item_value"><span></span></td></tr>'
          html += '<tr><td id="modul_energy_month_label_1"></td><td></td><td id="modul_energy_month_value_2" class="sh_card_item_value"><span></span></td>'
          html += '<td></td><td id="modul_energy_month_value_14" class="sh_card_item_value"><span></span></td></tr>'
          html += '<tr><td id="modul_energy_month_label_2"></td><td></td><td id="modul_energy_month_value_3" class="sh_card_item_value"><span></span></td>'
          html += '<td></td><td id="modul_energy_month_value_15" class="sh_card_item_value"><span></span></td></tr>'
          html += '<tr><td id="modul_energy_month_label_3"></td><td></td><td id="modul_energy_month_value_4" class="sh_card_item_value"><span></span></td>'
          html += '<td></td><td id="modul_energy_month_value_16" class="sh_card_item_value"><span></span></td></tr>'
          html += '<tr><td id="modul_energy_month_label_4"></td><td></td><td id="modul_energy_month_value_5" class="sh_card_item_value"><span></span></td>'
          html += '<td></td><td id="modul_energy_month_value_17" class="sh_card_item_value"><span></span></td></tr>'
          html += '<tr><td id="modul_energy_month_label_5"></td><td></td><td id="modul_energy_month_value_6" class="sh_card_item_value"><span></span></td>'
          html += '<td></td><td id="modul_energy_month_value_18" class="sh_card_item_value"><span></span></td></tr>'
          html += '<tr><td id="modul_energy_month_label_6"></td><td></td><td id="modul_energy_month_value_7" class="sh_card_item_value"><span></span></td>'
          html += '<td></td><td id="modul_energy_month_value_19" class="sh_card_item_value"><span></span></td></tr>'
          html += '<tr><td id="modul_energy_month_label_7"></td><td></td><td id="modul_energy_month_value_8" class="sh_card_item_value"><span></span></td>'
          html += '<td></td><td id="modul_energy_month_value_20" class="sh_card_item_value"><span></span></td></tr>'
          html += '<tr><td id="modul_energy_month_label_8"></td><td></td><td id="modul_energy_month_value_9" class="sh_card_item_value"><span></span></td>'
          html += '<td></td><td id="modul_energy_month_value_21" class="sh_card_item_value"><span></span></td></tr>'
          html += '<tr><td id="modul_energy_month_label_9"></td><td></td><td id="modul_energy_month_value_10" class="sh_card_item_value"><span></span></td>'
          html += '<td></td><td id="modul_energy_month_value_22" class="sh_card_item_value"><span></span></td></tr>'
          html += '<tr><td id="modul_energy_month_label_10"></td><td></td><td id="modul_energy_month_value_11" class="sh_card_item_value"><span></span></td>'
          html += '<td></td><td id="modul_energy_month_value_23" class="sh_card_item_value"><span></span></td></tr>'
          html += '<tr><td id="modul_energy_month_label_11"></td><td></td><td id="modul_energy_month_value_12" class="sh_card_item_value"><span></span></td>'
          html += '<td></td><td id="modul_energy_month_value_24" class="sh_card_item_value"><span></span></td></tr>'
          html += '</table>'
          html += '</div>'
          $(".main_content").append(html)
        }
        $('#content_modul_energy_month').show()
        var packery = $('#content_modul_energy_month').data('packery')
        if (!(packery == undefined)) {
          $('#content_modul_energy_month').data('packery').layout()
        }
      })
      setTimeout(this.update_data, 1200)
    }
  }
  window.modul.energy_month = _modul
  console.log("energy_month")
  return _modul
})