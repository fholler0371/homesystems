define(function () {
  var _modul = {
    icon: '/js/img/mdi/letter_y.svg',
    already_init : false,
    label: 'Jährlicher Verbrauch',
    init: function() {
      if (!modul.energy_year.already_init) {
        modul.energy_year.already_init = true
      }
    },
    update_data : function() {
      if ($('#content_modul_energy_year').is(":visible")) {
        setTimeout(modul.energy_year.update_data, 30000)
        postData('/api', { 'action': 'energy_year' }, token=true).then(data => {
          if (data.error != null) {
            console.error(data)
          } else {
            $('#modul_energy_year_value_0>span').text((data['days365']/1000).toLocaleString('de-DE', {minimumFractionDigits: 2, 
                                                                                                                 maximumFractionDigits: 2}))
            for (var i=0; i<data.history.length; i++) {
              var date = new Date(data.history[i].year)
              $('#modul_energy_year_label_'+i).text(date.toLocaleString('de-DE', {year: 'numeric'}))
              $('#modul_energy_year_value_'+(i+1)+'>span').text((data.history[i].value/1000).toLocaleString('de-DE', {minimumFractionDigits: 2, 
                                                                                                               maximumFractionDigits: 2})+' kW/h')
            }
          }
        })
      }
    },
    show: function() {
      console.log('show energy_year')
      $('#header_appname').text(modul.energy_year.label)
      require(['jqxtabs', 'inject'], function() {
        if (!($('#content_modul_energy_year').length)) {
          var html = '<div id="content_modul_energy_year" class="content_modul">'
          html += '<table class="sh_card sh_card_item_labels sh_table" ><tr><td></td><td style="width:25px"></td><td>Werte</td></tr>'
          html += '<tr><td>365-Tage</td><td></td><td id="modul_energy_year_value_0" class="sh_card_item_value"><span></span> kW/h</td></tr>'
          html += '<tr><td id="modul_energy_year_label_0"></td><td></td><td id="modul_energy_year_value_1" class="sh_card_item_value"><span></span></td></tr>'
          html += '<tr><td id="modul_energy_year_label_1"></td><td></td><td id="modul_energy_year_value_2" class="sh_card_item_value"><span></span></td></tr>'
          html += '<tr><td id="modul_energy_year_label_2"></td><td></td><td id="modul_energy_year_value_3" class="sh_card_item_value"><span></span></td></tr>'
          html += '<tr><td id="modul_energy_year_label_3"></td><td></td><td id="modul_energy_year_value_4" class="sh_card_item_value"><span></span></td></tr>'
          html += '</table>'
          html += '</div>'
          $(".main_content").append(html)
        }
        $('#content_modul_energy_year').show()
        var packery = $('#content_modul_energy_year').data('packery')
        if (!(packery == undefined)) {
          $('#content_modul_energy_year').data('packery').layout()
        }
      })
      setTimeout(this.update_data, 1200)
    }
  }
  window.modul.energy_year = _modul
  console.log("energy_year")
  return _modul
})