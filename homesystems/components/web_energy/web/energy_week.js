define(function () {
  var _modul = {
    icon: '/js/img/mdi/letter_w.svg',
    already_init : false,
    label: 'Wöchentlicher Verbrauch',
    init: function() {
      if (!modul.energy_week.already_init) {
        modul.energy_week.already_init = true
      }
    },
    update_data : function() {
      if ($('#content_modul_energy_week').is(":visible")) {
        setTimeout(modul.energy_week.update_data, 30000)
        postData('/api', { 'action': 'energy_week' }, token=true).then(data => {
          if (data.error != null) {
            console.error(data)
          } else {
            $('#modul_energy_week_value_0>span').text((data['days7']/1000).toLocaleString('de-DE', {minimumFractionDigits: 2, 
                                                                                                                 maximumFractionDigits: 2}))
            for (var i=0; i<10; i++) {
              $('#modul_energy_week_label_'+i).text(data.history[i].week+'. Woche')
              $('#modul_energy_week_value_'+(i+1)+'>span').text((data.history[i].value/1000).toLocaleString('de-DE', {minimumFractionDigits: 2, 
                                                                                                               maximumFractionDigits: 2}))
            }
          }
        })
      }
    },
    show: function() {
      console.log('show energy_week')
      $('#header_appname').text(modul.energy_week.label)
      require(['jqxtabs', 'inject'], function() {
        if (!($('#content_modul_energy_week').length)) {
          var html = '<div id="content_modul_energy_week" class="content_modul">'
          html += '<table><tr><td style="vertical-align: top">'
          html += '<table class="sh_card sh_card_item_labels sh_table" ><tr><td></td><td style="width:25px"></td><td>Werte</td></tr>'
          html += '<tr><td>7-Tage</td><td></td><td id="modul_energy_week_value_0" class="sh_card_item_value"><span></span> kW/h</td></tr>'
          html += '<tr><td id="modul_energy_week_label_0" class="modul_energy_week"></td><td></td><td id="modul_energy_week_value_1" class="sh_card_item_value"><span></span> kW/h</td></tr>'
          html += '<tr><td id="modul_energy_week_label_1" class="modul_energy_week"></td><td></td><td id="modul_energy_week_value_2" class="sh_card_item_value"><span></span> kW/h</td>/tr>'
          html += '<tr><td id="modul_energy_week_label_2" class="modul_energy_week"></td><td></td><td id="modul_energy_week_value_3" class="sh_card_item_value"><span></span> kW/h</td></tr>'
          html += '<tr><td id="modul_energy_week_label_3" class="modul_energy_week"></td><td></td><td id="modul_energy_week_value_4" class="sh_card_item_value"><span></span> kW/h</td></tr>'
          html += '<tr><td id="modul_energy_week_label_4" class="modul_energy_week"></td><td></td><td id="modul_energy_week_value_5" class="sh_card_item_value"><span></span> kW/h</td></tr>'
          html += '<tr><td id="modul_energy_week_label_5" class="modul_energy_week"</td><td></td><td id="modul_energy_week_value_6" class="sh_card_item_value"><span></span> kW/h</td></tr>'
          html += '<tr><td id="modul_energy_week_label_6" class="modul_energy_week"></td><td></td><td id="modul_energy_week_value_7" class="sh_card_item_value"><span></span> kW/h</td></tr>'
          html += '<tr><td id="modul_energy_week_label_7" class="modul_energy_week"></td><td></td><td id="modul_energy_week_value_8" class="sh_card_item_value"><span></span> kW/h</td></tr>'
          html += '<tr><td id="modul_energy_week_label_8" class="modul_energy_week"></td><td></td><td id="modul_energy_week_value_9" class="sh_card_item_value"><span></span> kW/h</td></tr>'
          html += '<tr><td id="modul_energy_week_label_9" class="modul_energy_week"></td><td></td><td id="modul_energy_week_value_10" class="sh_card_item_value"><span></span> kW/h</td></tr>'
          html += '</table></td sty><td style="width:50px"></td><td id="modul_energy_week_percent"  style="vertical-align: top">'
          html += '</td></tr></table>'
          html += '</div>'
          $(".main_content").append(html)
          $(".modul_energy_week").css("cursor","pointer").off("click").on("click", function(event) {
            var week=$(event.currentTarget).text().split('.')[0]
            postData('/api', { 'action': 'get_parts', 'interval': 'week', 'value': week }, token=true).then(data => {
              if (data.error != null) {
                console.error(data)
              } else {
                html = '<table class="sh_card sh_card_item_labels sh_table"><tr><td colspan="5" style="text-align:center">'
                html += $(event.currentTarget).text()+'</td></tr><tr height="12px"></tr><tr><td></td><td style="width:25px"></td>'
                html += '<td>Verbrauch</td></td><td style="width:25px"></td><td>Anteil</td></tr>'
                for (var x=0; x<data.length; x++) {
                  if (data[x].percent > 0.01) {
                    html += "<tr><td>"+data[x].label+'</td><td></td><td>'
                    html += data[x].value.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2})+' kW/h</td><td></td><td>'
                    html += data[x].percent.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2})+' %</td></tr>'
                  }
                }
                html += '</table>'
                $('#modul_energy_week_percent').html(html)
              }
            })
          })
        }
        $('#content_modul_energy_week').show()
        var packery = $('#content_modul_energy_week').data('packery')
        if (!(packery == undefined)) {
          $('#content_modul_energy_week').data('packery').layout()
        }
      })
      setTimeout(this.update_data, 1200)
    }
  }
  window.modul.energy_week = _modul
  console.log("energy_week")
  return _modul
})