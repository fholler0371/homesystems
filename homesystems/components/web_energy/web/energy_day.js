define(function () {
  var _modul = {
    icon: '/js/img/mdi/letter_d.svg',
    already_init : false,
    label: 'Täglicher Verbrauch',
    init: function() {
      if (!modul.energy_day.already_init) {
        modul.energy_day.already_init = true
      }
    },
    update_data : function() {
      if ($('#content_modul_energy_day').is(":visible")) {
        setTimeout(modul.energy_day.update_data, 30000)
        postData('/api', { 'action': 'energy_day' }, token=true).then(data => {
          if (data.error != null) {
            console.error(data)
          } else {
            var weekday = new Date()
            for (var i=0; i<14; i++) {
              $('#modul_energy_day_label_'+i).text(weekday.toLocaleString('de-de', {  weekday: 'long' }))
              weekday.setDate(weekday.getDate()-1)
            }
            $('#modul_energy_day_value_0>span').text((data.sensor.value_cur.value/1000).toLocaleString('de-DE', {minimumFractionDigits: 2, 
                                                                                                                 maximumFractionDigits: 2}))
            $('#modul_energy_day_value_1>span').text((data.sensor.value_1.value/1000).toLocaleString('de-DE', {minimumFractionDigits: 2, 
                                                                                                               maximumFractionDigits: 2}))
            var show_date = new Date()
            show_date.setDate(show_date.getDate()-1)
            for (var i=2; i<14; i++) {
              show_date.setDate(show_date.getDate()-1)
              var str_date = show_date.getFullYear() + "-" + ("0"+(show_date.getMonth()+1)).slice(-2) + "-"  + ("0" + show_date.getDate()).slice(-2) 
              var h_data = data.history
              for (var j=0; j<h_data.length; j++) {
                if (h_data[j].date == str_date) {
                  $('#modul_energy_day_value_'+i+'>span').text((h_data[j].value/1000).toLocaleString('de-DE', {minimumFractionDigits: 2, 
                                                                                                               maximumFractionDigits: 2}))
                }
              }
            }
          }
        })
      }
    },
    show: function() {
      console.log('show energy_day')
      $('#header_appname').text(modul.energy_day.label)
      require(['jqxtabs', 'inject'], function() {
        if (!($('#content_modul_energy_day').length)) {
          var html = '<div id="content_modul_energy_day" class="content_modul">'
          html += '<table><tr><td style="vertical-align: top">'
          html += '<table class="sh_card sh_card_item_labels sh_table" ><tr><td></td><td style="width:25px"></td><td>aktuelle Woche</td>'
          html += '<td style="width:25px"></td><td>letzte Woche</td></tr>'
          html += '<tr><td id="modul_energy_day_label_0"></td><td></td><td id="modul_energy_day_value_0" class="sh_card_item_value"><span></span> kW/h</td>'
          html += '<td></td><td id="modul_energy_day_value_7" class="sh_card_item_value"><span></span> kW/h</td></tr>'
          html += '<tr><td id="modul_energy_day_label_1" class="modul_energy_day"></td><td></td><td id="modul_energy_day_value_1" class="sh_card_item_value">'
          html += '<span></span> kW/h</td><td></td><td id="modul_energy_day_value_8" class="sh_card_item_value"><span></span> kW/h</td></tr>'
          html += '<tr><td id="modul_energy_day_label_2" class="modul_energy_day"></td><td></td><td id="modul_energy_day_value_2" class="sh_card_item_value">'
          html += '<span></span> kW/h</td><td></td><td id="modul_energy_day_value_9" class="sh_card_item_value"><span></span> kW/h</td></tr>'
          html += '<tr><td id="modul_energy_day_label_3" class="modul_energy_day"></td><td></td><td id="modul_energy_day_value_3" class="sh_card_item_value">'
          html += '<span></span> kW/h</td><td></td><td id="modul_energy_day_value_10" class="sh_card_item_value"><span></span> kW/h</td></tr>'
          html += '<tr><td id="modul_energy_day_label_4" class="modul_energy_day"></td><td></td><td id="modul_energy_day_value_4" class="sh_card_item_value">'
          html += '<span></span> kW/h</td><td></td><td id="modul_energy_day_value_11" class="sh_card_item_value"><span></span> kW/h</td></tr>'
          html += '<tr><td id="modul_energy_day_label_5" class="modul_energy_day"></td><td></td><td id="modul_energy_day_value_5" class="sh_card_item_value">'
          html += '<span></span> kW/h</td><td></td><td id="modul_energy_day_value_12" class="sh_card_item_value"><span></span> kW/h</td></tr>'
          html += '<tr><td id="modul_energy_day_label_6" class="modul_energy_day"></td><td></td><td id="modul_energy_day_value_6" class="sh_card_item_value">'
          html += '<span></span> kW/h</td><td></td><td id="modul_energy_day_value_13" class="sh_card_item_value"><span></span> kW/h</td></tr>'
          html += '</table></td sty><td style="width:50px"></td><td id="modul_energy_day_percent" style="vertical-align: top">'
          html += '</td></tr></table>'
          html += '</div>'
          $(".main_content").append(html)
          $(".modul_energy_day").css("cursor","pointer").off("click").on("click", function(event) {
            var day=$(event.currentTarget).attr('id').split('_')[4]
            postData('/api', { 'action': 'get_parts', 'interval': 'day', 'value': day }, token=true).then(data => {
              if (data.error != null) {
                console.error(data)
              } else {
                html = '<table class="sh_card sh_card_item_labels sh_table"><tr><td colspan="5" style="text-align:center">'
                html += $('#modul_energy_day_label_'+day).text()+'</td></tr><tr height="12px"></tr><tr><td></td><td style="width:25px"></td>'
                html += '<td>Verbrauch</td></td><td style="width:25px"></td><td>Anteil</td></tr>'
                for (var x=0; x<data.length; x++) {
                  if (data[x].percent > 0.01) {
                    html += "<tr><td>"+data[x].label+'</td><td></td><td>'
                    html += data[x].value.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2})+' kW/h</td><td></td><td>'
                    html += data[x].percent.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2})+' %</td></tr>'
                  }
                }
                html += '</table>'
                $('#modul_energy_day_percent').html(html)
              }
            })
          })
        }
        $('#content_modul_energy_day').show()
        var packery = $('#content_modul_energy_day').data('packery')
        if (!(packery == undefined)) {
          $('#content_modul_energy_day').data('packery').layout()
        }
      })
      setTimeout(this.update_data, 1200)
    }
  }
  window.modul.energy_day = _modul
  console.log("energy_day")
  return _modul
})