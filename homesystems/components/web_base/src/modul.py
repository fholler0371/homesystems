import os
import aiohttp
import aiofiles
import mimetypes

from homesystems.lib.aio_http import http_simple

from homesystems.process import c_process_base
from homesystems.lib.helper import overload

#import time

#from pprint import pprint

#from aio_process import (
#    EVENT_PROCESS_RUNNING,
#    EVENT_PROCESS_STOPPING,
#    EVENT_RCP_PORT_GET
#)

from homesystems.components.web_master import (
    EVENT_WEB_REGISTER_URL,
    EVENT_WEB_REGISTER_URL_DONE
)

class c_modul(c_process_base):
    def __init__(self, *args, **kwargs):
        c_process_base.__init__(self, *args, **kwargs)
        self.modul_name = 'main'
        self.http_port = -1
        self.domain_registered = False

#        self.domain_registered = False
    
##############################################

    async def doInit(self):
        hs = self.hs
#        await core.bus.add_listener(EVENT_PROCESS_RUNNING, self.doStart)
#        await core.bus.add_listener(EVENT_PROCESS_STOPPING, self.doStop)
        await hs.bus.add_listener(EVENT_WEB_REGISTER_URL_DONE, self.domain_register_done)

##############################################
##############################################

    async def handler(self, request):
        hs = self.hs
        headers = {}
        for key in request.headers:
            headers[key] = request.headers[key]
        if request.method == 'GET':
            url_path = request.path
            path = ""
            if url_path.startswith('/css/images'):
                path = hs.path.web + "/" + url_path.replace('/css', 'lib/jqwidgets/styles')
            elif url_path.startswith('/css/'):
                path = hs.path.web + "/" + url_path[1:]
            elif url_path.startswith('/js/'):
                path = hs.path.web + "/" + url_path.replace('/js/', 'lib/')
            if os.path.isfile(path):
                r = aiohttp.web.StreamResponse(headers={'Content-Type': mimetypes.guess_type(path)[0]})
                await r.prepare(request)
                try:
                    async with aiofiles.open(path, mode='rb') as f:
                        await r.write(await f.read())
                        await r.write_eof()
                    return r
                except Exception as e:
                    hs.log.error(repr(e))
            else:
                pass
                #print(path)
            return aiohttp.web.Response(text=f"{url_path} not found", status=404)
        else:
            hs.log.error(request.method)
        return aiohttp.web.Response(text="OK", status=999)

##############################################
##############################################

    async def domain_register(self):
        hs = self.hs
        await hs.bus.emit(EVENT_WEB_REGISTER_URL, target_app="*.web_master", 
                            context={'app': f"{hs.hostname}.{hs.manifest['name']}", 
                                     'domain': "__base__", 
                                     'url':f"http://{hs.ip}:{self.http_port}"})
        if self.domain_registered:
            await hs.run_delay(900, self.domain_register)
        else:
            await hs.run_delay(15, self.domain_register)

##############################################

    async def domain_register_done(self, msg):
        self.domain_registered = True

##############################################
##############################################

    @overload
    async def doStart(self):
        hs = self.hs
        self.http_port = hs.reserved_ports[0]
        await self.domain_register()
        #await self.main_menu_register()
        try:
            self.server = http_simple(hs.log, self.http_port, self.handler)
            await self.server.setup()
        except Exception as e:
            hs.log.error(repr(e))
            hs.kill.set()
        hs.log.info(f"{hs.name} running http server on port: {self.http_port}")

##############################################

#    async def doStop(self, msg):
#        self.running = False
