import os
import time
import aiohttp
import mimetypes
import aiofiles
import urllib.parse
import jwt

from homesystems.lib.aio_http import http_simple

from homesystems.process import c_process_base
from homesystems.lib.helper import overload

from homesystems.lib.aio_eventbus import (
    EVENT_RESPONSE
)

from homesystems.lib.modul_states import ( 
    EVENT_STATES_DEVICE_GET
)

from homesystems.components.web_master import (
    EVENT_WEB_REGISTER_URL,
    EVENT_WEB_REGISTER_URL_DONE,

    EVENT_WEB_GET_ACTION
)

from homesystems.components.authentication import (
    EVENT_AUTH_APP_REGISTER,
    EVENT_AUTH_APP_SECRET_ID,
    EVENT_TRANSFERTOKEN_CHECK
)

from homesystems.components.web_main import (
    EVENT_WEB_MAIN_REGISTER,
    EVENT_WEB_MAIN_REGISTERED
)

from homesystems.components.system import (
    EVENT_SYSTEM_STATIC_DATA_GET,
    EVENT_SYSTEM_DYNAMIC_HOST_GET
)

from homesystems.components.system_pi import (
    EVENT_SYSTEM_PI_UPGRADE,
    EVENT_SYSTEM_PI_GIT_PUSH,
    EVENT_SYSTEM_PI_GIT_PULL,
    EVENT_SYSTEM_PI_RELOAD
)

class c_modul(c_process_base):
    def __init__(self, *args, **kwargs):
        c_process_base.__init__(self, *args, **kwargs)
        self.modul_name = 'main'
        self.http_port = -1
        self.domain_registered = False
        self.auth_registered = False
        self.menu_registered = True
        self.secret_id = ''
        self.auth_domain = ''
        self.auth_path = ''
        self.public_key = None
    
##############################################

    @overload
    async def doInit(self):
        hs = self.hs
        await hs.bus.add_listener(EVENT_WEB_REGISTER_URL_DONE, self.domain_register_done)
        await hs.bus.add_listener(EVENT_WEB_GET_ACTION, self.handle_action)
        await hs.bus.add_listener(EVENT_AUTH_APP_SECRET_ID, self.set_secret_id)
        await hs.bus.add_listener(EVENT_WEB_MAIN_REGISTERED, self.menu_registered_done)

##############################################

    async def get_app_list(self, data):
        out = []
        for entry in data['scope'].split(' '):
            if entry in ['sys_static']:
                out.append({'url': f"/js/{entry}", 'modul': entry})
                out.append({'url': f"/js/sys_dynamic", 'modul': 'sys_dynamic'})
            elif entry in ['sys_action']:
                out.append({'url': f"/js/{entry}", 'modul': entry})
        return {'user': data['firstname'], 'modul': out}

##############################################

    async def decode_token(self, token):
        hs = self.hs
        try: 
            key = "\n".join([l.lstrip() for l in self.public_key.split("\n")])
            data = jwt.decode(token, key, algorithms=['RS256'])
            if data['expire'] > time.time():
                return data
        except:
            pass
        return None

##############################################

    async def check_transfer_token(self, token):
        hs = self.hs
        resp = await hs.bus.emit_wait(EVENT_TRANSFERTOKEN_CHECK, target_app="*.authentication", 
                                        context={'app': f"{hs.hostname}.{hs.manifest['name']}", 'token': token})
        return resp

##############################################
 
    async def static_data_get(self):
        hs = self.hs
        resp_data = await hs.bus.emit_wait(EVENT_SYSTEM_STATIC_DATA_GET, target_app="*.system", 
                                             context={'app': f"{hs.hostname}.{hs.manifest['name']}"})
        return resp_data 

##############################################
 
    async def dynamic_hosts_get(self):
        hs = self.hs
        resp_data = await hs.bus.emit_wait(EVENT_SYSTEM_DYNAMIC_HOST_GET, target_app="*.system", 
                                             context={'app': f"{hs.hostname}.{hs.manifest['name']}"})
        return resp_data 

##############################################
 
    async def dynamic_data_get(self, device, id):
        hs = self.hs
        resp_data = await hs.bus.emit_wait(EVENT_STATES_DEVICE_GET, sensor=device, target_app="*.system", 
                                             context={'app': f"{hs.hostname}.{hs.manifest['name']}"})
        if resp_data is None:
            hs.log.error(f"dynamic_data_get>cannot get data for {device}")   
            return {'id': id, 'channels': {}}                                  
        return {'id': id, 'channels': resp_data['channels']} 

##############################################

    async def send_action(self, host, cmd):
        hs = self.hs
        if cmd == 'apt_upgrade':
            event = EVENT_SYSTEM_PI_UPGRADE
        elif cmd == 'git_push':
            event = EVENT_SYSTEM_PI_GIT_PUSH
        elif cmd == 'git_pull':
            event = EVENT_SYSTEM_PI_GIT_PULL
        elif cmd == 'reload':
            event = EVENT_SYSTEM_PI_RELOAD
        resp_data = await hs.bus.emit_wait(event, target_app=host+".system_pi", 
                                             context={'app': f"{hs.hostname}.{hs.manifest['name']}"})
        return resp_data
        
##############################################

    async def handle_action(self, msg):
        hs = self.hs
        data = msg.context
        ### allgemeine Anfragen ###
        if data['action'] == 'get_login_url':
            url = f"https://{self.auth_domain}.{hs.config.base_domain}/{self.auth_path}?response_type=code"
            url += f"&client_id={hs.config.data['client_id']}"
            url += f"&redirect_uri="
            url += urllib.parse.quote_plus(f"https://{hs.config.data['http_domain']}.{hs.config.base_domain}/auth_callback")
            url += f"&scope={'+'.join(hs.config.data['scopes'])}&state="
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                context={'result': {'url': url}}, \
                                target_app= data['response_app'])
        elif data['action'] == 'check_code':
            url = f"https://{self.auth_domain}.{hs.config.base_domain}/oauth/token"
            post_data = f"grant_type=authorization_code&code={data['code']}"
            post_data += f"&client_id={hs.config.data['client_id']}&client_secret={self.secret_id}"
            async with aiohttp.ClientSession() as session:
                async with session.post(url, data=post_data) as resp:
                    await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                        context={'result': await resp.json()}, \
                        target_app= data['response_app'])
        elif data['action'] == 'refresh_token':
            url = f"https://{self.auth_domain}.{hs.config.base_domain}/oauth/token"
            post_data = f"grant_type=refresh_token&refresh_token={data['refresh_token']}"
            post_data += f"&client_id={hs.config.data['client_id']}&client_secret={self.secret_id}"
            async with aiohttp.ClientSession() as session:
                async with session.post(url, data=post_data) as resp:
                    await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                        context={'result': await resp.json()}, \
                        target_app= data['response_app'])
        elif data['action'] == 'check_transfer_token':
            resp_data = await self.check_transfer_token(data['transfer_token'])
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                context={'result': resp_data}, \
                                target_app= data['response_app'])
        elif data['action'] == 'get_module':
            token_data = await self.decode_token(data['token'])
            if token_data is None:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "token invalid"}}, \
                                    target_app= data['response_app'])
                return
            resp_data = await self.get_app_list(token_data)
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                context={'result': resp_data}, \
                                target_app= data['response_app'])
        ### Anwendungs Anfragen ###
        elif data['action'] == 'get_static_data':
            token_data = await self.decode_token(data['token'])
            if token_data is None:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "token invalid"}}, \
                                    target_app= data['response_app'])
                return
            if 'sys_static' not in token_data['scope']:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "scope invalid"}}, \
                                    target_app= data['response_app'])
            resp_data = await self.static_data_get()
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                context={'result': resp_data}, \
                                target_app= data['response_app'])
        elif data['action'] == 'get_dynamic_hosts':
            token_data = await self.decode_token(data['token'])
            if token_data is None:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "token invalid"}}, \
                                    target_app= data['response_app'])
                return
            if 'sys_static' not in token_data['scope']:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "scope invalid"}}, \
                                    target_app= data['response_app'])
            resp_data = await self.dynamic_hosts_get()
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                context={'result': resp_data}, \
                                target_app= data['response_app'])
        elif data['action'] == 'get_dynamic_data':
            token_data = await self.decode_token(data['token'])
            if token_data is None:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "token invalid"}}, \
                                    target_app= data['response_app'])
                return
            if 'sys_static' not in token_data['scope']:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "scope invalid"}}, \
                                    target_app= data['response_app'])
            resp_data = await self.dynamic_data_get(data['device'], data['id'])
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                context={'result': resp_data}, \
                                target_app= data['response_app'])
        elif data['action'] == 'get_action_hosts':
            token_data = await self.decode_token(data['token'])
            if token_data is None:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "token invalid"}}, \
                                    target_app= data['response_app'])
                return
            if 'sys_action' not in token_data['scope']:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "scope invalid"}}, \
                                    target_app= data['response_app'])
            resp_data = await self.dynamic_hosts_get()
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                context={'result': resp_data}, \
                                target_app= data['response_app'])
        elif data['action'] == 'send_action':
            token_data = await self.decode_token(data['token'])
            if token_data is None:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "token invalid"}}, \
                                    target_app= data['response_app'])
                return
            if 'sys_action' not in token_data['scope']:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "scope invalid"}}, \
                                    target_app= data['response_app'])
            resp_data = await self.send_action(data['host'], data['cmd'])
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                context={'result': resp_data}, \
                                target_app= data['response_app'])
        ### nicht bearbeitet Anfragen ###
        else:
            hs.log.error(f"'{data['action']}' not handled")
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                context={'result': {'error': f"'{data['action']}' not handled"}}, \
                                target_app= data['response_app'])

##############################################

    async def handler(self, request):
        hs = self.hs
        headers = {}
        for key in request.headers:
            headers[key] = request.headers[key]
        if request.method == 'GET':
            url_path = request.path
            if url_path == '/':
                url_path = '/index.html'
            elif url_path == '/auth_callback':
                url_path = '/index.html'
            elif url_path == '/favicon.ico':
                url_path = '/raspberrypi.svg'
            elif url_path == '/js/sys_static.js':
                url_path = '/sys_static.js'
            elif url_path == '/js/sys_dynamic.js':
                url_path = '/sys_dynamic.js'
            elif url_path == '/js/sys_action.js':
                url_path = '/sys_action.js'
            path = f"{hs.path.comp}/{hs.manifest['name']}/web{url_path}"
            if os.path.isfile(path):
                r = aiohttp.web.StreamResponse(headers={'Content-Type': mimetypes.guess_type(path)[0]})
                await r.prepare(request)
                try:
                    async with aiofiles.open(path, mode='rb') as f:
                        if url_path == '/index.html':
                            content = (await f.read()).decode()
                            params = request.rel_url.query
                            do_in = '<script>\n'
                            for key in params:
                                do_in += f'      var data_{key} = "{params[key]}"\n'
                            do_in += '    </script>'
                            content.replace('<script></script>', do_in)
                            await r.write(content.replace('<script></script>', do_in).encode())
                        else:
                            await r.write(await f.read())
                    await r.write_eof()
                    return r
                except Exception as e:
                    hs.log.error(repr(e))
            return aiohttp.web.Response(text="", status=999)
        else:
            print(request.method)
        return aiohttp.web.Response(text="OK", status=999)

##############################################
##############################################

    async def domain_register(self):
        hs = self.hs
        await hs.bus.emit(EVENT_WEB_REGISTER_URL, target_app="*.web_master", 
                          context={'app': f"{hs.hostname}.{hs.manifest['name']}", 
                                   'domain': f"{hs.config.data.get('http_domain', 'noname')}", 
                                   'url':f"http://{hs.ip}:{self.http_port}"})
        if self.domain_registered:
            await hs.run_delay(43200, self.domain_register)
        else:
            await hs.run_delay(15, self.domain_register)

##############################################

    async def domain_register_done(self, msg):
        self.domain_registered = True

##############################################
##############################################

    async def auth_register(self):
        hs = self.hs
        await hs.bus.emit(EVENT_AUTH_APP_REGISTER, target_app="*.authentication", 
                          context={'app': f"{hs.hostname}.{hs.manifest['name']}", 
                                   'client_id': hs.config.data['client_id'], 
                                   'scopes': hs.config.data['scopes'],
                                   'label': hs.config.data['label'],
                                   'icon': hs.config.data['icon'],
                                   'callback': f"https://{hs.config.data['http_domain']}.{hs.config.base_domain}/auth_callback"})
        if self.auth_registered:
            await hs.run_delay(900, self.auth_register)
        else:
            await hs.run_delay(15, self.auth_register)

##############################################

    async def set_secret_id(self, msg):
        hs = self.hs
        self.secret_id = msg.context['secret_id']
        self.auth_domain = msg.context['domain']
        self.auth_path = msg.context['path']
        self.public_key = msg.context['public_key']
        self.auth_registered = True

##############################################
##############################################

    async def main_menu_register(self):
        hs = self.hs
        await hs.bus.emit(EVENT_WEB_MAIN_REGISTER, target_app="*.web_main", 
                            context={'app': f"{hs.hostname}.{hs.manifest['name']}",
                                     'domain': hs.config.data['http_domain'], 
                                     'scopes': hs.config.data['scopes'],
                                     'label': hs.config.data['label'],
                                     'icon': hs.config.data['icon']})
        if self.menu_registered:
            await hs.run_delay(900, self.main_menu_register)
        else:
            await hs.run_delay(15, self.main_menu_register)

##############################################

    async def menu_registered_done(self, msg):
        self.menu_registered = True

##############################################
##############################################

    @overload
    async def doStart(self):
        hs = self.hs
        self.http_port = hs.reserved_ports[0]
        await self.domain_register()
        await self.auth_register()
        await self.main_menu_register()
        try:
            self.server = http_simple(hs.log, self.http_port, self.handler)
            await self.server.setup()
        except Exception as e:
            hs.log.error(repr(e))
            hs.kill.set()
        hs.log.info(f"{hs.name} running http server on port: {self.http_port}")

