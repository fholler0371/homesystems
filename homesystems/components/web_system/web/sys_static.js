define(function () {
  var _modul = {
    icon: '/js/img/mdi/raspberrypi.svg',
    already_init : false,
    label: 'Pi-Statik Info',
    init: function() {
      if (!modul.sys_static.already_init) {
        modul.sys_static.already_init = true
      }
    },
    show: function() {
      console.log('show sys_static')
      $('#header_appname').text(modul.sys_static.label)
      require(['jqxtabs'], function() {
        if (!($('#content_modul_sys_static').length)) {
          var html = '<div id="content_modul_sys_static" class="content_modul">'
          html += '</div>'
          $(".main_content").append(html)
        }
        $('#content_modul_sys_static').show()
        postData('/api', { 'action': 'get_static_data'}, token=true).then(data => {
          if (data.error != null) {
            console.error(data)
          } else {
            var html_li = ""
            var html_div = ""
            for (var i=0; i<data.length; i++) {
              html_li += '<li>'+data[i].label+'</li>'
              html_div += '<div><table class="sh_card sh_table sh_card_item_labels"><tr><td>Board:</td><td>'+data[i].channels.board.value+'</td><td style="width: 25px"></td>'
              html_div += '<td>Release:</td><td>'+data[i].channels.release.value+'</td></tr>'
              html_div += '<tr><td>Serien-Nr.:</td><td>'+data[i].channels.serial.value+'</td><td style="width: 25px"></td>'
              html_div += '<td>Release:</td><td>'+data[i].channels.kernel.value+'</td></tr>'
              html_div += '<tr><td>Hostname:</td><td>'+data[i].channels.hostname.value+'</td><td style="width: 25px"></td>'
              html_div += '<td>Python:</td><td>'+data[i].channels.pyversion.value+'</td></tr>'
              html_div += '<tr><td>IP:</td><td>'+data[i].channels.ip.value+'</td></tr></table></div>'
            }
            var html = '<div id="modul_sys_static_tab"><ul>'+html_li+'</ul>'+html_div+'</div>'
            $('#content_modul_sys_static').html(html)
            $('#modul_sys_static_tab').jqxTabs({ width: '100%', height: '100%', position: 'top', theme: 'material'})
          }
        })      
      })
    }
  }
  window.modul.sys_static = _modul
  console.log("sys_static")
  return _modul
})