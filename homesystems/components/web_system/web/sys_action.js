define(function () {
  var _modul = {
    icon: '/js/img/mdi/button-tap.svg',
    already_init : false,
    label: 'Befehle',
    init: function() {
      if (!modul.sys_action.already_init) {
        modul.sys_action.already_init = true
      }
    },
    show: function() {
      console.log('show sys_action')
      $('#header_appname').text(modul.sys_action.label)
      require(['jqxtabs', 'jqxbuttons'], function() {
        if (!($('#content_modul_sys_action').length)) {
          var html = '<div id="content_modul_sys_action" class="content_modul">'
          html += '</div>'
          $(".main_content").append(html)
        }
        $('#content_modul_sys_action').show()
        postData('/api', { 'action': 'get_action_hosts'}, token=true).then(data => {
          if (data.error != null) {
            console.error(data)
          } else {
            var html_li = ""
            var html_div = ""
            for (var i=0; i<data.length; i++) {
              html_li += '<li>'+data[i].label+'</li>'
              html_div += '<div><table class="sh_card sh_table sh_card_item_labels"><tr><td width="30px" height="30px"></td>'
              html_div += '<td></td><td width="30px"></td><td></td></tr>'
              html_div += '<tr><td></td><td><div class="modul_sys_action_button" data-host="'+data[i].device.split('_')[0]+'" data-text="apt_upgrade"'
              html_div += '">apt upgrade</div></td></tr><tr><td height="30px"></td></tr><tr><td></td><td>'
              html_div += '<div class="modul_sys_action_button" data-host="'+data[i].device.split('_')[0]+'" data-text="git_push"'
              html_div += '>git push</div></td><td></td><td>'
              html_div += '<div class="modul_sys_action_button" data-host="'+data[i].device.split('_')[0]+'" data-text="git_pull"'
              html_div += '>git pull</div></td></tr></tr><tr><td height="30px"></td></tr><tr><tr><td></td><td>'
              html_div += '<div class="modul_sys_action_button" data-host="'+data[i].device.split('_')[0]+'" data-text="reload"'
              html_div += '>reload</div></td></table></div>'
            }
            var html = '<div id="modul_sys_action_tab"><ul>'+html_li+'</ul>'+html_div+'</div>'
            $('#content_modul_sys_action').html(html)
            $('.modul_sys_action_button').jqxButton({theme: 'material', template: 'default', width:100})
            $('.modul_sys_action_button').off('click').on('click', function(event) {
              var ele = $(event.currentTarget),
                  host = ele.data('host'),
                  cmd = ele.data('text')
              ele.jqxButton({template: 'info'})
              postData('/api', { 'action': 'send_action', 'host': host, 'cmd': cmd}, token=true).then(data => {
                if (data.error != null) {
                  console.error(data)
                } else {
                  ele.jqxButton({template: 'default'})
                }
              })
            })
            $('#modul_sys_action_tab').jqxTabs({ width: '100%', height: '100%', position: 'top', theme: 'material'})
          }
        })      
      })
    }
  }
  window.modul.sys_action = _modul
  console.log("sys_action")
  return _modul
})