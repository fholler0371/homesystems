define(function () {
  var _modul = {
    icon: '/js/img/mdi/chart-box-outline.svg',
    already_init : false,
    label: 'Pi-Dynamische Werte',
    last_device: '',
    last_id:0,
    update_timer: null,
    init: function() {
      if (!modul.sys_dynamic.already_init) {
        modul.sys_dynamic.already_init = true
      }
    },
    show: function() {
      console.log('show sys_dynamic')
      $('#header_appname').text(modul.sys_dynamic.label)
      require(['jqxtabs'], function() {
        if (!($('#content_modul_sys_dynamic').length)) {
          var html = '<div id="content_modul_sys_dynamic" class="content_modul">'
          html += '</div>'
          $(".main_content").append(html)
        }
        $('#content_modul_sys_dynamic').show()
        postData('/api', { 'action': 'get_dynamic_hosts'}, token=true).then(data => {
          if (data.error != null) {
            console.error(data)
          } else {
            var html_li = ""
            var html_div = ""
            for (var i=0; i<data.length; i++) {
              if (i==0) {
                modul.sys_dynamic.last_device = data[i].device
              }
              html_li += '<li data-device="'+data[i].device+'">'+data[i].label+'</li>'
              html_div += '<div><table class="sh_card sh_table"><tr><td></td><td width="25px"></td><td>Werte</td><tr>'
              html_div += '<tr class="sh_card_item_labels"><td>System-Updates</td><td></td><td id="apt" class="sh_card_item_value"></td></tr>'
              html_div += '<tr class="sh_card_item_labels"><td>Git-Updates</td><td></td><td id="git" class="sh_card_item_value"></td></tr>'
              html_div += '<tr class="sh_card_item_labels"><td>CPU-Frequenz</td><td></td><td id="cpu" class="sh_card_item_value"><span></span> MHz</td></tr>'
              html_div += '<tr class="sh_card_item_labels"><td>Temperatur</td><td></td><td id="temp" class="sh_card_item_value"><span></span> °C</td></tr>'
              html_div += '<tr class="sh_card_item_labels"><td>Load 5</td><td></td><td id="load5" class="sh_card_item_value"><span></span> %</td></tr>'
              html_div += '<tr class="sh_card_item_labels"><td>Load 15</td><td></td><td id="load" class="sh_card_item_value"><span></span> %</td></tr>'
              html_div += '<tr class="sh_card_item_labels"><td>Laufzeit Pi</td><td></td><td id="uptime" class="sh_card_item_value"></td></tr>'
              html_div += '<tr class="sh_card_item_labels"><td>Laufzeit</td><td></td><td id="runtime" class="sh_card_item_value"></td></tr>'
              html_div += '<tr class="sh_card_item_labels"><td>TX / RX</td><td></td><td id="net" class="sh_card_item_value"></td></tr></table></div>'
            }
            var html = '<div id="modul_sys_dynamic_tab"><ul>'+html_li+'</ul>'+html_div+'</div>'
            $('#content_modul_sys_dynamic').html(html)
            $('#modul_sys_dynamic_tab').jqxTabs({ width: '100%', height: '100%', position: 'top', theme: 'material'})
            $('#modul_sys_dynamic_tab').off('selected')
            $('#modul_sys_dynamic_tab').on('selected', window.modul.sys_dynamic.switch)
            window.modul.sys_dynamic.update()
          }
        })      
      })
    },
    switch: function(event) {
      var selectedTab = event.args.item
      modul.sys_dynamic.last_id = selectedTab
      var li_ele = $('#modul_sys_dynamic_tab').find('li')[selectedTab]
      modul.sys_dynamic.last_device = $(li_ele).data('device')
      window.modul.sys_dynamic.update()
    },
    update: function() {
      clearTimeout(window.modul.sys_dynamic.update_timer)
      if ($("#content_modul_sys_dynamic").is(":visible")) {
        postData('/api', { 'action': 'get_dynamic_data', 'device': modul.sys_dynamic.last_device, 'id': modul.sys_dynamic.last_id}, token=true).then(data => {
          if (data.error != null) {
            console.error(data)
          } else {
            var ele = $('#modul_sys_dynamic_tab>.jqx-tabs-content').children()[data.id]
            $(ele).find('#apt').text(data.channels.apt.value)
            $(ele).find('#git').text(data.channels.git.value)
            $(ele).find('#cpu>span').text(data.channels.cpu_freq.value)
            $(ele).find('#temp>span').text(data.channels.temperature.value.toLocaleString('de-DE', {minimumFractionDigits: 1, 
                                                                                                    maximumFractionDigits: 1}))
            $(ele).find('#load5>span').text((data.channels.load5.value*100).toLocaleString('de-DE', {minimumFractionDigits: 1, 
                                                                                          maximumFractionDigits: 1}))
            $(ele).find('#load>span').text((data.channels.load15.value*100).toLocaleString('de-DE', {minimumFractionDigits: 1, 
                                                                                          maximumFractionDigits: 1}))
            var uptime=data.channels.uptime.value,
                unit = 0,
                units = ['s', 'min', 'h', 'd'],
                fraction = ['0', '1', '2', '2'],
                unit_max = [300, 60, 30, 10000000],
                div = [60, 60, 24]
            while (uptime>unit_max[unit]) {
              uptime /= div[unit]
              unit += 1
            }
            $(ele).find('#uptime').text(uptime.toLocaleString('de-DE', {minimumFractionDigits: fraction[unit], 
                                                                        maximumFractionDigits: 2})+" "+units[unit])
            var uptime=data.channels.runtime.value,
                unit = 0,
                units = ['s', 'min', 'h', 'd'],
                fraction = ['0', '1', '2', '2'],
                unit_max = [300, 60, 30, 10000000],
                div = [60, 60, 24]
            while (uptime>unit_max[unit]) {
              uptime /= div[unit]
              unit += 1
            }
            $(ele).find('#runtime').text(uptime.toLocaleString('de-DE', {minimumFractionDigits: fraction[unit], 
                                                                         maximumFractionDigits: 2})+" "+units[unit])
            var tx = data.channels.net_tx.value,
                tx_unit = "kByte/s",
                rx = data.channels.net_rx.value,
                rx_unit = "kByte/s"
            if (tx > 1000) {
              tx = tx/1024
              tx_unit = "MByte/s"
            }
            if (rx > 1000) {
              rx = rx/1024
              rx_unit = "MByte/s"
            }
            $(ele).find('#net').text(tx.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2})+" "+tx_unit+' / '+
                                     rx.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2})+" "+rx_unit)
          }
          window.modul.sys_dynamic.update_timer = setTimeout(window.modul.sys_dynamic.update, 30000)
        })
      }
    }
  }
  window.modul.sys_dynamic = _modul
  console.log("sys_dynamic")
  return _modul
})