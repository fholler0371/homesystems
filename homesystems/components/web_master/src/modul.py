from homesystems.lib.aio_http import http_simple
from homesystems.process import c_process_base
from homesystems.lib.helper import overload
import aiohttp

from .const import (
    EVENT_WEB_REGISTER_URL,
    EVENT_WEB_REGISTER_URL_DONE,

    EVENT_WEB_GET_ACTION
)

class c_modul(c_process_base):
    def __init__(self, *args, **kwargs):
        c_process_base.__init__(self, *args, **kwargs)
        self.modul_name = 'main'
        self._urls = {}
    
##############################################

    @overload
    async def doInit(self):
        hs = self.hs
        await hs.bus.add_listener(EVENT_WEB_REGISTER_URL, self.register_urls)

##############################################
##############################################

    async def register_urls(self, msg):
        hs = self.hs
        self._urls[msg.context['domain']] = {'url': msg.context['url'], 'app': msg.context['app']}
        await hs.bus.emit(EVENT_WEB_REGISTER_URL_DONE, target_app=msg.context['app'])

##############################################

    async def handler(self, request):
        hs = self.hs
        sub = ".".join(request.headers['X-Host'].split('.')[:-2])
        headers = {}
        for key in request.headers:
            if key in ['Host']:
                continue
            headers[key] = request.headers[key]
        if request.method == 'GET':
            if sub in self._urls:
                async with aiohttp.ClientSession() as session:
                    status = 0
                    async with session.get(f"{self._urls[sub]['url']}{request.path_qs}", headers=headers) as resp:
                        status = resp.status
                        if status != 999:
                            try:
                                res_header = {}
                                for key in resp.headers:
                                    if key in ['Transfer-Encoding', 'Connection', 'Server']:
                                        continue
                                    res_header[key] = resp.headers[key] 
                                r = aiohttp.web.StreamResponse(headers=res_header, status=resp.status)
                                await r.prepare(request)
                                await r.write(await resp.read())
                                await r.write_eof()
                                return r
                            except Exception as e:
                                hs.log.error(repr(e))
                    if not '__base__' in self._urls:
                        return aiohttp.web.Response(text="no base", status=404)
                    async with session.get(f"{self._urls['__base__']['url']}{request.path}", headers=headers) as resp:
                        try:    
                            res_header = {}
                            for key in resp.headers:
                                if key in ['Transfer-Encoding', 'Connection', 'Server']:
                                    continue
                                res_header[key] = resp.headers[key]                        
                 #        self._urls = {}
                            r = aiohttp.web.StreamResponse(headers=res_header, status=resp.status)
                            await r.prepare(request)
                            await r.write(await resp.read())
                            await r.write_eof()
                            return r
                        except Exception as e:
                            hs.log.error(repr(e))
            else:
                hs.log.error(f"host not registered {sub}")
        elif request.method == 'POST':
            if sub not in self._urls:
                return aiohttp.web.json_response({'error': 'app not registered'}, status=503)
            try: #handle app json data
                data = await request.json()
                data['response_app'] = f"{hs.hostname}.{hs.manifest['name']}"
                data['remote_ip'] = request.headers['X-Real-IP']
                res = await hs.bus.emit_wait(EVENT_WEB_GET_ACTION, context=data, target_app=self._urls[sub]['app'])
                if res is None:
                    return aiohttp.web.json_response({'error': 'app not answer'}, status=504)
                if 'error' in res:
                    return aiohttp.web.json_response(res, status=418)
                return aiohttp.web.json_response(res)
            except Exception as e:
                hs.log.error(repr(e))
                async with aiohttp.ClientSession() as session:
                    async with session.post(f"{self._urls[sub]['url']}{request.path_qs}", headers=headers, data=await request.text()) as resp:
                        if resp.status == 200:
                            return aiohttp.web.json_response(await resp.json())
        return aiohttp.web.Response(text="OK")

##############################################
##############################################

    @overload
    async def doStart(self):
        hs = self.hs
        self.http_port = hs.reserved_ports[0]
        try:
            self.server = http_simple(hs.log, self.http_port, self.handler)
            await self.server.setup()
        except Exception as e:
            hs.log.error(repr(e))
            hs.kill.set()
        hs.log.info(f"{hs.name} running http server on port: {self.http_port}")

