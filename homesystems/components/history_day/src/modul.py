import aiosqlite
from datetime import datetime, timedelta, date

from homesystems.process import c_process_base
from homesystems.lib.helper import overload

from homesystems.lib.aio_eventbus import (
    EVENT_RESPONSE
)

from .const import (
    EVENT_HISTORY_DAY_PUSH,
    EVENT_HISTORY_DAY_GET,
    EVENT_HISTORY_DAY_WEEK,
    EVENT_HISTORY_DAY_MONTH,
    EVENT_HISTORY_DAY_YEAR,

    EVENT_HISTORY_DAY_GET_ALL_SUM,

    ENERGY_FAKTOR
)


class c_modul(c_process_base):
    def __init__(self, *args, **kwargs):
        c_process_base.__init__(self, *args, **kwargs)
        hs = self.hs
        self.modul_name = 'main'
        self._filename = f"{hs.path.db}/{hs.manifest['name']}.sqlite3"
    
##############################################

    @overload
    async def doInit(self):
        hs = self.hs
        await hs.bus.add_listener(EVENT_HISTORY_DAY_PUSH, self.history_add)
        await hs.bus.add_listener(EVENT_HISTORY_DAY_GET, self.history_get)
        await hs.bus.add_listener(EVENT_HISTORY_DAY_WEEK, self.history_week)
        await hs.bus.add_listener(EVENT_HISTORY_DAY_MONTH, self.history_month)
        await hs.bus.add_listener(EVENT_HISTORY_DAY_YEAR, self.history_year)
        await hs.bus.add_listener(EVENT_HISTORY_DAY_GET_ALL_SUM, self.history_day_all_sum)
        
##############################################
##############################################

    async def history_add(self, msg):
        hs = self.hs
        async with aiosqlite.connect(self._filename) as db:
            sql = "CREATE TABLE IF NOT EXISTS history (id INTEGER NOT NULL PRIMARY KEY, \
                                                       sensor TEXT, \
                                                       date TEXT, \
                                                       value REAL)"
            await db.execute(sql)
            await db.commit()
            id = -1
            async with db.execute(f"SELECT id FROM history WHERE sensor='{msg.sensor}' and date='{msg.context['date']}'") as cursor:
                async for row in cursor:
                    id = row[0]
            if id == -1:
                await db.execute(f"INSERT INTO history (`sensor`, `date`, `value`) VALUES ('{msg.sensor}', '{msg.context['date']}', '{msg.context['value']}')")
            else:
                await db.execute(f"UPDATE history SET value='{msg.context['value']}' WHERE id='{id}'")
            await db.commit()

##############################################

    async def history_get(self, msg):
        hs = self.hs
        limit = msg.context.get('limit', 1)
        target_app = msg.context.get('app', '')
        out = []
        async with aiosqlite.connect(self._filename) as db:
            async with db.execute(f"SELECT date, value FROM history WHERE sensor='{msg.sensor}' ORDER BY date desc LIMIT {limit}") as cursor:
                async for row in cursor:
                    out.append({'date': row[0], 'value': row[1]*ENERGY_FAKTOR})
        await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, target_app=target_app, \
                                                           context={'result': out})

##############################################

    async def history_week(self, msg):
        hs = self.hs
        limit = msg.context.get('limit', 1)
        target_app = msg.context.get('app', '')
        first_week = -1
        week_data = {}
        out = []
        async with aiosqlite.connect(self._filename) as db:
            async with db.execute(f"SELECT date, value FROM history WHERE sensor='{msg.sensor}' ORDER BY date desc LIMIT {(limit+2)*7}") as cursor:
                async for row in cursor:
                    week_nr = datetime.strptime(row[0], '%Y-%m-%d').isocalendar().week
                    if first_week == -1:
                        first_week = week_nr
                    if str(week_nr) in week_data:
                        week_data[str(week_nr)]['value'] += row[1]
                        week_data[str(week_nr)]['count'] += 1
                    else:
                        week_data[str(week_nr)] = {'count': 1, 'value': row[1]}
        if week_data[str(first_week)]['count'] < 7:
            del week_data[str(first_week)]
        for entry in week_data:
            if len(out) < limit:
                out.append({'week': entry, 'value': week_data[entry]['value']*ENERGY_FAKTOR})
        await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, target_app=target_app, \
                                                           context={'result': out})

##############################################

    async def history_month(self, msg):
        hs = self.hs
        limit = msg.context.get('limit', 1)
        target_app = msg.context.get('app', '')
        out = []
        async with aiosqlite.connect(self._filename) as db:
            async with db.execute(f"SELECT substr(date,0,8), sum(value), count(id) FROM history WHERE sensor='{msg.sensor}' GROUP BY substr(date,0,8) ORDER BY substr(date,0,8) DESC LIMIT {limit+1}") as cursor:
                async for row in cursor:
                    month = datetime.strptime(row[0]+'-01', '%Y-%m-%d')
                    days = (month.replace(month = month.month % 12 +1, day = 1)-timedelta(days=1)).day
                    if days == row[2] or len(out) > 0:
                        out.append({'month': row[0]+'-01', 'value': row[1]*ENERGY_FAKTOR})
        await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, target_app=target_app, \
                                                           context={'result': out[:limit]})
      
##############################################

    async def history_year(self, msg):
        hs = self.hs
        limit = msg.context.get('limit', 1)
        target_app = msg.context.get('app', '')
        out = []
        last_year = str(int(date.today().strftime('%Y'))-1)
        async with aiosqlite.connect(self._filename) as db:
            async with db.execute(f"SELECT substr(date,0,5), sum(value), count(id) FROM history WHERE sensor='{msg.sensor}' GROUP BY substr(date,0,5) ORDER BY substr(date,0,5) DESC") as cursor:
                async for row in cursor:
                    year = datetime.strptime(row[0] + '-01-01', '%Y-%m-%d')
                    days = (year.replace(year = year.year+1, month=1, day = 1) - datetime.strptime(row[0] + '-01-01', '%Y-%m-%d')).days
                    #if days == row[2] or True:
                    if row[0] <= last_year and row[0] >= '2021':
                        out.append({'year': row[0]+'-01-01', 'value': row[1]*ENERGY_FAKTOR})
        await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, target_app=target_app, \
                                                           context={'result': out[:limit]})

###########################################from datetime import date

    async def history_day_all_sum(self, msg):
        hs = self.hs
        out = {}
        data = msg.context
        if 'mode' in data and data['mode'] == "w":
            end_date = datetime.strptime(data['start'], "%Y-%m-%d")+timedelta(days=6)
            today = date.today().strftime('%Y-%m-%d')
            sql = f"SELECT sensor,sum(value) FROM history WHERE date>='{data['start']}' and date<='{end_date}' group by sensor"
        else:
            today = (date.today()-timedelta(days=int(data['start']))).strftime('%Y-%m-%d')
            sql = f"SELECT sensor,value FROM history WHERE date='{today}'"
        async with aiosqlite.connect(self._filename) as db:
            async with db.execute(sql) as cursor:
                async for row in cursor:
                    out[row[0]] = row[1]
        await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, target_app=data.get('app', ''), \
                                                           context={'result': out})

