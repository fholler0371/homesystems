import os
import sys
import time
import asyncio
from asyncio import subprocess
import aiofiles
import aiofiles.os
import psutil

from homesystems.process import c_process_base
from homesystems.lib.helper import overload

from homesystems.lib.aio_eventbus import (
    EVENT_RESPONSE
)

from homesystems.components.system import (
    EVENT_SYSTEM_NEW_VALUE
)

from .const import (
    EVENT_SYSTEM_PI_UPGRADE,
    EVENT_SYSTEM_PI_GIT_PUSH,
    EVENT_SYSTEM_PI_GIT_PULL,
    EVENT_SYSTEM_PI_RELOAD
)

from .const import BOARDS

class c_modul(c_process_base):
    def __init__(self, *args, **kwargs):
        c_process_base.__init__(self, *args, **kwargs)
        self.modul_name = 'main'
        self.static = {}
        self.last_net_time = 0
        self.last_tx = 0
        self.last_rx = 0
        self.start_time = time.time()
    
###############################################

    async def doInit(self):
        hs = self.hs
        await hs.bus.add_listener(EVENT_SYSTEM_PI_UPGRADE, self.apt_upgrade)
        await hs.bus.add_listener(EVENT_SYSTEM_PI_GIT_PUSH, self.git_push)
        await hs.bus.add_listener(EVENT_SYSTEM_PI_GIT_PULL, self.git_pull)
        await hs.bus.add_listener(EVENT_SYSTEM_PI_RELOAD, self.cmd_reload)

##############################################
##############################################

    async def apt_upgrade(self, msg):
        hs = self.hs
        await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                            context={'result': {}}, \
                                            target_app=msg.context['app'])
        process = await asyncio.create_subprocess_shell('sudo apt upgrade -y', stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        await process.wait()
        await self.send_dynamic5(update_only=True)

##############################################

    async def git_push(self, msg):
        hs = self.hs
        await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                            context={'result': {}}, \
                                            target_app=msg.context['app'])
        cmd = f"cd {hs.path.base} ; .venv/bin/python -m homesystems -s push"
        process = await asyncio.create_subprocess_shell(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        await process.wait()
        await self.send_dynamic5(update_only=True)

##############################################

    async def git_pull(self, msg):
        hs = self.hs
        await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                            context={'result': {}}, \
                                            target_app=msg.context['app'])
        cmd = f"cd {hs.path.base} ; .venv/bin/python -m homesystems -s pull"
        process = await asyncio.create_subprocess_shell(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        await process.wait()
        await self.send_dynamic5(update_only=True)

##############################################

    async def cmd_reload(self, msg):
        hs = self.hs
        await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                            context={'result': {}}, \
                                            target_app=msg.context['app'])
        cmd = f"cd {hs.path.base} ; .venv/bin/python -m homesystems -s restart"
        process = await asyncio.create_subprocess_shell(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        await process.wait()
        await self.send_dynamic5(update_only=True)

##############################################
##############################################

    async def get_stat_info(self):
        hs = self.hs
        if self.running:
            await hs.run_delay(3600, self.get_stat_info)
        async with aiofiles.open('/proc/cpuinfo') as f:
            async for line in f:
                for s in ['Serial', 'Revision']:
                    if s in line:
                        value = line.split(':')[1].split('\n')[0].strip()
                        self.static[s.lower()] = value
        if self.static['revision'] in BOARDS:
            self.static['board'] = BOARDS[self.static['revision']]
        else: 
            self.static['board'] = "unbekannt"
            hs.log.warn(f"unkonwn pi board: {self.static['revision']}")
        async with aiofiles.open('/etc/os-release') as f:
            async for line in f:
                if line.startswith("VERSION_CODENAME"):
                    self.static['release'] = line.split("=")[1].split('\n')[0]
        async with aiofiles.open('/proc/version') as f:
            async for line in f:
                self.static['kernel'] = line.split(' ')[2]
        self.static['ip'] = hs.ip
        self.static['hostname'] = hs.hostname
        self.static['pyversion'] = f"{sys.version_info.major}.{sys.version_info.minor}.{sys.version_info.micro}"
  
##############################################

    async def send_static(self):
        hs = self.hs
        if self.running:
            await hs.run_delay(600, self.send_static)
        for entry in self.static:
            await hs.bus.emit(EVENT_SYSTEM_NEW_VALUE, target_app="*.system", sensor=f"{hs.hostname}_static.{entry}", context={'value': self.static[entry]})

##############################################

    async def send_dynamic(self):
        hs = self.hs
        if self.running:
            await hs.run_delay(60, self.send_dynamic)
            async with aiofiles.open('/sys/class/thermal/thermal_zone0/temp') as f:
                async for line in f:
                    value = int(line)/1000
                    await hs.bus.emit(EVENT_SYSTEM_NEW_VALUE, target_app="*.system", sensor=f"{hs.hostname}_dynamic.temperature", 
                                                                context={'value': value})
            async with aiofiles.open('/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_cur_freq') as f:
                async for line in f:
                    value = int(line)/1000000
                    await hs.bus.emit(EVENT_SYSTEM_NEW_VALUE, target_app="*.system", sensor=f"{hs.hostname}_dynamic.cpu_freq", 
                                                                context={'value': value})
            async with aiofiles.open('/proc/loadavg') as f:
                async for line in f:
                    values = line.split(' ')
                    await hs.bus.emit(EVENT_SYSTEM_NEW_VALUE, target_app="*.system", sensor=f"{hs.hostname}_dynamic.load15", 
                                                                context={'value': float(values[2])/os.cpu_count()})
                    await hs.bus.emit(EVENT_SYSTEM_NEW_VALUE, target_app="*.system", sensor=f"{hs.hostname}_dynamic.load5", 
                                                                context={'value': float(values[1])/os.cpu_count()})
            async with aiofiles.open('/proc/uptime') as f:
                async for line in f:
                    value = line.split(' ')[0]
                    await hs.bus.emit(EVENT_SYSTEM_NEW_VALUE, target_app="*.system", sensor=f"{hs.hostname}_dynamic.uptime", 
                                                                context={'value': value})
            await hs.bus.emit(EVENT_SYSTEM_NEW_VALUE, target_app="*.system", sensor=f"{hs.hostname}_dynamic.runtime", 
                                                      context={'value': int(time.time()-self.start_time)})
            #network
            io_data = psutil.net_io_counters(True)
            for interface in io_data:
                if interface == hs.config.net['interface']:
                    cur_rx = io_data[interface].bytes_sent
                    cur_tx = io_data[interface].bytes_recv
                    if self.last_net_time > 0:
                        if cur_tx > self.last_tx:
                            await hs.bus.emit(EVENT_SYSTEM_NEW_VALUE, target_app="*.system", sensor=f"{hs.hostname}_dynamic.net_tx", 
                                                context={'value': int((cur_tx-self.last_tx)/(time.time()-self.last_net_time))/1024})
                        if cur_rx > self.last_rx:
                            await hs.bus.emit(EVENT_SYSTEM_NEW_VALUE, target_app="*.system", sensor=f"{hs.hostname}_dynamic.net_rx", 
                                                context={'value': int((cur_rx-self.last_rx)/(time.time()-self.last_net_time))/1024})
                    self.last_net_time = time.time()
                    self.last_rx = cur_rx
                    self.last_tx = cur_tx#

##############################################

    async def send_dynamic5(self, update_only=False):
        hs = self.hs
        if self.running:
            if not update_only:
                await hs.run_delay(300, self.send_dynamic5)
            #apt status
            fname = f"{hs.path.tmp}/apt_run.sh"
            async with aiofiles.open(fname, mode='w') as f:
                await f.write("apt list --upgradable | wc -l")
            process = await asyncio.create_subprocess_shell(f'/bin/bash {fname}', stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            await process.wait()
            apt_count = int((await process.stdout.read()).decode().split('\n')[0])-1
            await aiofiles.os.unlink(fname)
            async with aiofiles.open(fname, mode='w') as f:
                await f.write('[ -d /lib/modules/$(uname -r) ] || echo "*"\n')
            process = await asyncio.create_subprocess_shell(f'/bin/bash {fname}', stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            await process.wait()
            reboot = (await process.stdout.read()).decode().split('\n')[0]
            if reboot != "":
                if apt_count > 0:
                    apt_count *= -1
                else:
                    apt_count = -1
            await hs.bus.emit(EVENT_SYSTEM_NEW_VALUE, target_app="*.system", sensor=f"{hs.hostname}_dynamic.apt", 
                                                      context={'value': apt_count})
            await aiofiles.os.unlink(fname)
            #git status
            fname = f"{hs.path.tmp}/git_run.sh"
            async with aiofiles.open(fname, mode='w') as f:
                await f.write(f"cd {hs.path.base}/.git/homesystems ; git status -s -uall | wc -l")
            process = await asyncio.create_subprocess_shell(f'/bin/bash {fname}', stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            await process.wait()
            git_count = (await process.stdout.read()).decode().split('\n')[0]
            await aiofiles.os.unlink(fname)
            if git_count == '0' or git_count == 0:
                pass
                async with  aiofiles.open(fname, mode='w') as f:
                    await f.write(f"cd {hs.path.base}/.git/homesystems\ngit fetch > /dev/null 2> /dev/null\ngit diff --name-only main origin/main | wc -l > /dev/null 2> /dev/null\ngit diff --name-only main origin/main | wc -l\ngit diff --name-only main origin/main | wc -l\n")
                process = await asyncio.create_subprocess_shell(f'/bin/bash {fname}', stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                await process.wait()
                git_count = -1*int((await process.stdout.read()).decode().split('\n')[0])
            else:
                git_count = int(git_count)
            await hs.bus.emit(EVENT_SYSTEM_NEW_VALUE, target_app="*.system", sensor=f"{hs.hostname}_dynamic.git", 
                                                      context={'value': git_count})

##############################################

    async def prepare(self):
        await asyncio.create_subprocess_shell('sudo chmod 777 /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_cur_freq')

##############################################
##############################################

    @overload
    async def doStart(self):
        hs = self.hs
        await self.prepare()
        await self.get_stat_info()
        await hs.run_delay(10, self.send_dynamic)
        await hs.run_delay(15, self.send_static)
        await hs.run_delay(25, self.send_dynamic5)

