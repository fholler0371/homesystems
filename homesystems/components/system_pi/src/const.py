EVENT_SYSTEM_PI_UPGRADE = 'system_pi_upgrade'
EVENT_SYSTEM_PI_GIT_PUSH = 'system_pi_git_push'
EVENT_SYSTEM_PI_GIT_PULL = 'system_pi_git_pull'
EVENT_SYSTEM_PI_RELOAD = 'system_pi_reload'

BOARDS = {'a02082': "Pi 3B / 1GB V1.2 (Sony, UK)",
          'c03111': "Pi 4 / 4GB V1.1 (Sony, UK)",
          'd03114': "Pi 4 / 8GB V1.4 (Sony, UK)",
          '902120': "Zero 2 W / 512MB V1.0 (Sony, UK)"}