def bool_int(value):
    if value:
        return 1
    return 0  
  
def int_bool(value):
    return value == 1  
 
def t_state_send(device, channel):
    ch = channel[5:]
    if ch == "":
        ch = 1
    return f"{device}:{ch}"
    
DEVICES = {'HmIP-STHD': {'type': 'hmip_clima',
                         'channels': [{'name': 'temperature', 'type': 'float', 'hm_label': 'ACTUAL_TEMPERATURE'},
                                      {'name': 'humidity', 'type': 'float', 'hm_label': 'HUMIDITY'},
                                      {'name': 'temperature_set', 'type': 'float', 'hm_label': 'SET_POINT_TEMPERATURE'}]
                        },
           'HmIP-WTH-2': {'type': 'hmip_clima',
                          'channels': [{'name': 'temperature', 'type': 'float', 'hm_label': 'ACTUAL_TEMPERATURE'},
                                       {'name': 'humidity', 'type': 'float', 'hm_label': 'HUMIDITY'},
                                       {'name': 'temperature_set', 'type': 'float', 'hm_label': 'SET_POINT_TEMPERATURE'}]
                         },
           'HM-TC-IT-WM-W-EU': {'type': 'hmip_clima',
                                'channels': [{'name': 'temperature', 'type': 'float', 'hm_label': 'TEMPERATURE'},
                                             {'name': 'humidity', 'type': 'int', 'hm_label': 'HUMIDITY'}]
                               }, 
           'HmIP-SRH': {'type': 'hmip_window',
                         'channels': [{'name': 'window_state', 'type': 'int', 'hm_label': 'STATE'}]
                       },
           'HM-ES-PMSw1-Pl': {'type': 'hm_socket',
                              'channels': [{'name': 'voltage', 'type': 'float', 'hm_label': 'VOLTAGE'},
                                           {'name': 'state', 'type': 'int', 'hm_label': 'STATE', 'v_convert': bool_int,
                                            'ts_convert': t_state_send, 'vs_convert': int_bool},
                                           {'name': 'current', 'type': 'float', 'hm_label': 'CURRENT'},
                                           {'name': 'power', 'type': 'float', 'hm_label': 'POWER'},
                                           {'name': 'frequency', 'type': 'float', 'hm_label': 'FREQUENCY'},
                                           {'name': 'energy_total', 'type': 'float', 'hm_label': 'ENERGY_COUNTER', 
                                            'follow_script': 'calc_energy'},
                                           {'name': 'energy_history', 'type': 'float', 'hm_label': ''},
                                           {'name': 'energy_last_yesterday', 'type': 'float', 'hm_label': ''},
                                           {'name': 'date', 'type': 'str', 'hm_label': ''},
                                           {'name': 'energy_today', 'type': 'float', 'hm_label': ''},
                                           {'name': 'energy_yesterday', 'type': 'float', 'hm_label': ''}]
                             }
          } 

