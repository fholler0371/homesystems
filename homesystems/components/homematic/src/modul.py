import time
import asyncio
import subprocess
from functools import partial
from aiohttp import web
from datetime import date, timedelta
from homesystems.lib.aio_xmlrpc import rpc_server
from aiohttp_xmlrpc.client import ServerProxy
from aiohttp import ClientConnectorError

from homesystems.process import c_process_base
from homesystems.lib.helper import overload

from homesystems.lib.modul_states import (
    EVENT_STATES_DEVICE_ADD,
    EVENT_STATES_CHANNEL_ADD,
    EVENT_STATES_VALUE_NEW,
    EVENT_STATES_CHANNEL_GET,
    EVENT_STATES_CHANNEL_SET_VALUE
)

from homesystems.components.history_day import (
    EVENT_HISTORY_DAY_PUSH
)

from .const import DEVICES

DAYHISTORY_INTERVAL = 900
DUTYCYCLE_INTERVAL = 150

DUTY_CYCLE_MAX = 80

##############################################
##############################################

class Modul(c_process_base):
    def __init__(self, *args, **kwargs):
        c_process_base.__init__(self, *args, **kwargs)
        self.modul_name = 'main'
        self.rcpPort = None
        self.rpcServer = None
        self.hm = None
        self.last_hm = 0
        self.hmip = None
        self.last_hmip = 0
        self._ignored_types = []
        self._devices = {}
    
##############################################

    @overload
    async def doInit(self):
        hs = self.hs
        await hs.bus.subscribe(EVENT_STATES_CHANNEL_SET_VALUE, self.setValue)

##############################################
##############################################

    async def _rpcEvent(self, *args):
        hs = self.hs
        if args[0].startswith('hm_'):
            self.last_hm = time.time()
        if args[0].startswith('hmip_'):
            self.last_hmip = time.time()
        if len(args) == 4:
            device = f"{args[0].split('_')[0]}_{args[1].split(':')[0]}"
            if device in self._devices:
                d_data = self._devices[device]
                for channel in DEVICES[d_data['type']]['channels']:
                    value = args[3]
                    in_channel = args[2]
                    if 'in_topic' in channel:
                        in_channel = channel['in_topic'](args[1], in_channel)
                    if 'in_value' in channel:
                        value = channel['v_convert'](args[3])
                    if in_channel == channel['hm_label']:
                        await hs.bus.send(EVENT_STATES_VALUE_NEW, sensor=f"{d_data['device']}.{channel['name']}", data={'value': value})
                        if 'follow_script' in channel:
                            await getattr(self, channel['follow_script'])(d_data['device'], channel['name'], value)
                            
##############################################

    async def setValue(self, msg, resp):
        hs = self.hs
        device, channel = msg.sensor.split('.')
        value = msg.data['value']
        for label in self._devices:
            if self._devices[label]['device'] == device:
                for cb_channel in DEVICES[self._devices[label]['type']]['channels']:
                    if cb_channel['name'] == channel:
                        proxy, t_device = label.split('_', 1)
                        if 'ts_convert' in cb_channel:
                            t_device = cb_channel['ts_convert'](t_device, channel)
                        if 'vs_convert' in cb_channel:
                            value = cb_channel['vs_convert'](value)
                        if proxy == 'hm' and self.hm:
                            try:
                                await self.hm.setValue(t_device, cb_channel['hm_label'], value)
                            except Exception as e:
                                hs.log.error(repr(e))
                                self.hm = None

##############################################
     
    async def calc_energy(self, device, channel, value):
        hs = self.hs
        try:
            #calc sum to care about device reset
            energy_device = await hs.bus.emit_wait(EVENT_STATES_CHANNEL_GET, sensor= f"{device}.{channel}")
            energy_history = await hs.bus.emit_wait(EVENT_STATES_CHANNEL_GET, sensor= f"{device}.energy_history")
            energy_sum = energy_history['value']
            if energy_device['value'] < energy_device['last']:
                energy_sum += energy_device['last']
                await hs.bus.send(EVENT_STATES_VALUE_NEW, sensor=f"{device}.{channel}", data={'value': 0.000000001})
                await hs.bus.send(EVENT_STATES_VALUE_NEW, sensor=f"{device}.{channel}", data={'value': value})
                await hs.bus.send(EVENT_STATES_VALUE_NEW, sensor=f"{device}.energy_history", data={'value': energy_sum})
            energy_sum += energy_device['value']
            #calc today value
            energy_last = await hs.bus.emit_wait(EVENT_STATES_CHANNEL_GET, sensor= f"{device}.energy_last_yesterday")
            energy_today = energy_sum - energy_last['value']
            await hs.bus.send(EVENT_STATES_VALUE_NEW, sensor=f"{device}.energy_today", data={'value': energy_today})
            #check date change
            cur_date = date.today().strftime('%Y%m%d')
            energy_date = await hs.bus.emit_wait(EVENT_STATES_CHANNEL_GET, sensor= f"{device}.date")
            if energy_date['value'] == "":
                await hs.bus.send(EVENT_STATES_VALUE_NEW, sensor=f"{device}.date", data={'value': cur_date})
                await hs.bus.send(EVENT_STATES_VALUE_NEW, sensor=f"{device}.energy_last_yesterday", data={'value': energy_sum})
            elif energy_date['value'] != cur_date:
                await hs.bus.send(EVENT_STATES_VALUE_NEW, sensor=f"{device}.date", data={'value': cur_date})
                await hs.bus.send(EVENT_STATES_VALUE_NEW, sensor=f"{device}.energy_yesterday", data={'value': energy_today})
                await hs.bus.send(EVENT_STATES_VALUE_NEW, sensor=f"{device}.energy_last_yesterday", data={'value': energy_sum})
        except Exception as e:
            hs.log.warn(repr(e))
            
##############################################

    async def send_energy_history(self):
        hs = self.hs
        cur_date = date.today().strftime('%Y%m%d')
        last_date = (date.today()-timedelta(days=1)).strftime('%Y-%m-%d')
        if self.running:
            await hs.run_delay(DAYHISTORY_INTERVAL, self.send_energy_history)
        for device_id in self._devices:
            device = self._devices[device_id]
            if device['type'] == 'HM-ES-PMSw1-Pl': # Steckdosen
                energy_date = await hs.bus.emit_wait(EVENT_STATES_CHANNEL_GET, sensor= f"{device['device']}.date")
                if energy_date['value'] != cur_date:
                    await self.calc_energy(device['device'], 'energy_total', 0)
                else:
                    energy = await hs.bus.emit_wait(EVENT_STATES_CHANNEL_GET, sensor= f"{device['device']}.energy_yesterday")
                    if energy['value'] > 0:
                        await hs.bus.send(EVENT_HISTORY_DAY_PUSH, sensor=f"{hs.manifest['name']}.{device['device']}.energy", 
                                        data={'value': energy['value'], 'date': last_date}, 
                                        target="*.history_day")
                            
##############################################
##############################################

    async def _rpcListDevices(self, *args):
        """ Dummy Eintrag für die Schnittstelle sendet leere Liste zurück. Argumente werden nicht verarbeitet!""" 
        return []
    async def _rpcNewDevices(self, *args):
        """ Dummy Eintrag für die Schnittstelle. Argumente werden nicht verarbeitet!""" 
        return True
    async def _rpcNewDevice(self, *args):
        """ Dummy Eintrag für die Schnittstelle. Argumente werden nicht verarbeitet!""" 
        return True

##############################################
##############################################

    async def hm_init(self):
        hs = self.hs
        await hs.run_delay(15, self.hm_init)
        if self.last_hm + 180 < time.time() and self.hm is not None:
            self.last_hm = time.time()
            try:
                await self.hm.init(f"{hs.ip}:{self.rcpPort}", 'hm_hs')
                hs.log.debug("init hm")
            except Exception as e:
                hs.log.error("hm init")
                hs.log.error(repr(e))
                self.hm = None
        if (int(self.last_hm-time.time()) % 60) == 5 and self.hm is not None:
            try:
                await self.hm.ping('hm_aio')
            except Exception as e:
                hs.log.error("hm ping")
                hs.log.error(repr(e))
                self.hm = None

##############################################

    async def hmip_init(self):
        hs = self.hs
        await hs.run_delay(15, self.hmip_init)
        if self.last_hmip + 180 < time.time() and self.hmip is not None:
            try:
                await self.hmip.init(f"{hs.ip}:{self.rcpPort}", 'hmip_hs')
                self.last_hmip = time.time()
                hs.log.debug("init hmip")
                await self.hmip.close()
                self.hmip = None
            except Exception as e:
                hs.log.error("hmip init")
                hs.log.error(repr(e))
                self.hmip = None
        sec = int(self.last_hmip-time.time()) % 60
        if 2 < sec < 20 and self.hmip is not None:
            try:
                await self.hmip.ping('hmip_aio')
            except Exception as e:
                hs.log.error("hmip ping")
                hs.log.error(repr(e))
                self.hmip = None

##############################################

    async def create_rpc_hm(self):
        hs = self.hs
        await hs.run_delay(30, self.create_rpc_hm)
        if self.hm is None:
            self.hm = ServerProxy(f"http://{hs.config.data.get('ccu_ip')}:2001")
            hs.log.debug("Create hm proxy")

##############################################

    async def create_rpc_hmip(self):
        hs = self.hs
        await hs.run_delay(30, self.create_rpc_hmip)
        if self.hmip is None:
            self.hmip = ServerProxy(f"http://{hs.config.data.get('ccu_ip')}:2010")
            hs.log.debug("Create hmip proxy")

##############################################

    async def hm_list_devices(self):
        hs = self.hs
        if self.hm is not None:
            try:
                for device in await self.hm.listDevices('hm'):
                    if "PARENT" in device and device["PARENT"] == "":
                        await self.newDevice(f"hm_{device['ADDRESS']}",device['TYPE'])
                await hs.run_delay(3600, self.hm_list_devices)
            except Exception as e:
                await hs.run_delay(30, self.hm_list_devices)
                hs.log.error("hm listDevices")
                hs.log.error(repr(e))
                self.hm = None
        else:
            await hs.run_delay(30, self.hm_list_devices) 

##############################################

    async def hmip_list_devices(self):
        hs = self.hs
        if self.hmip is not None:
            try:
                for device in await self.hmip.listDevices('hmip'):
                    if "PARENT" in device and device["PARENT"] == "":
                        await self.newDevice(f"hmip_{device['ADDRESS']}",device['TYPE'])
                await hs.run_delay(3600, self.hmip_list_devices)
            except ClientConnectorError:
                hs.log.error(f"can not list hmip devices")
                self.hmip = None
            except Exception as e:
                await hs.run_delay(30, self.hmip_list_devices)
                hs.log.error("hmip listDevices")
                hs.log.error(repr(e))
                self.hmip = None
        else:
            await hs.run_delay(30, self.hmip_list_devices)

##############################################
##############################################

    async def newDevice(self, addr, type):
        hs = self.hs
        if type not in DEVICES:
            if type not in self._ignored_types:
                hs.log.warning(f"unkonwn device type {type}")
                self._ignored_types.append(type)
            return
        if addr in self._devices:
            #already registred
            return
        data = DEVICES[type]
        devices = hs.config.data.get('devices', {})
        _device_data = devices.get(addr, addr)
        if "|" in _device_data:
            device, label = _device_data.split("|", 1)
            await hs.bus.send(EVENT_STATES_DEVICE_ADD, data={'name': device, 'label': label, 'type': data['type']})
            self._devices[addr] = {'device': device, 'type': type}
            for ch_data in data['channels']:
                await hs.bus.send(EVENT_STATES_CHANNEL_ADD, sensor=device, data={'name': ch_data['name'], 'type': ch_data['type']})

##############################################
##############################################

    async def get_duty_cyle(self):
        hs = self.hs
        if self.running:
            await hs.run_delay(DUTYCYCLE_INTERVAL, self.get_duty_cyle)
        if self.hm is not None:
            try:
                # FIRMWARE_VERSION  4.4.22
                # TYPE              CCU2
                # DESCRIPTION       
                # DEFAULT           True
                # CONNECTED         True
                # ADDRESS           QEQ0694188
                # DUTY_CYCLE        53
                res = await self.hm.listBidcosInterfaces('hm')
                res = res[0]
                duty_cycle = res['DUTY_CYCLE']
                if duty_cycle > DUTY_CYCLE_MAX:
                    hs.log.warn(f'duty cycle too high: {duty_cycle} (max: {DUTY_CYCLE_MAX})')
                    process = await asyncio.create_subprocess_shell('sudo systemctl restart pivccu', stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            except Exception as e:
                print(repr(e))
                self.hm = None

##############################################
##############################################

    @overload    
    async def doStart(self):
        hs = self.hs
        self.rcpPort = hs.reserved_ports[0]
        hs.log.debug(f"start homematic rcp_port on port: {self.rcpPort}")
        self.rpcServer = rpc_server(hs.log, self.rcpPort)
        await self.rpcServer.setup()
        await self.rpcServer.register_function(self._rpcEvent, 'event')
        await self.rpcServer.register_function(self._rpcListDevices, 'listDevices')
        await self.rpcServer.register_function(self._rpcNewDevices, 'newDevices')
        await self.rpcServer.register_function(self._rpcNewDevice, 'newDevice')
        await hs.run_delay(2, self.create_rpc_hm)
        await hs.run_delay(3, self.hm_init)
        await hs.run_delay(4, self.hm_list_devices)
        await hs.run_delay(5, self.create_rpc_hmip)
        await hs.run_delay(6, self.hmip_init)
        await hs.run_delay(7, self.hmip_list_devices)
        await hs.run_delay(DAYHISTORY_INTERVAL // 10, self.send_energy_history)
        await hs.run_delay(DUTYCYCLE_INTERVAL // 10, self.get_duty_cyle)

