labels = ['Modul']


def __dir__():
    return [*globals().keys(), *labels]


def __getattr__(name):
    if name == "Modul":
        from .src.modul import Modul
        return Modul