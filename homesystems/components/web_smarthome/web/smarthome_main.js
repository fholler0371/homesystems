define(function () {
  var _modul = {
    icon: '/js/img/mdi/<!icon!>',
    already_init : false,
    label: '<!label!>',
    init: function() {
      if (!modul.page_<!modul!>.already_init) {
        modul.page_<!modul!>.already_init = true
      }
    },
    update_data : function() {
      if ($('#content_modul_page_<!modul!>').is(":visible")) {
        setTimeout(modul.page_<!modul!>.update_data, 15000)
        var sub_data = [],
            cards = $('#content_modul_page_<!modul!>').children()
        for (var i=0; i<cards.length; i++) {
          sub_data.push($(cards[i]).data('sources'))
        }
        postData('/api', { 'action': 'sensor_get', 'panel': '<!modul!>', 'sensors': sub_data }, token=true).then(data => {
          if (data.error != null) {
            console.error(data)
          } else {
            var cards = $('#content_modul_page_<!modul!>').children()
            for (var i=0; i<data.length; i++) {
              var update = window.cards[$(cards[i]).data('type')].update
              update('<!modul!>', i, data[i])
            }
          }
        })
      }
    },
    show: function() {
      console.log('show page_<!modul!>')
      $('#header_appname').text(modul.page_<!modul!>.label)
      require(['jqxtabs', 'inject'], function() {
        if (!($('#content_modul_page_<!modul!>').length)) {
          var html = '<div id="content_modul_page_<!modul!>" class="content_modul">'
          html += '</div>'
          $(".main_content").append(html)
          postData('/api', { 'action': 'cards_get', 'panel': '<!modul!>' }, token=true).then(data => {
            if (data.error != null) {
              console.error(data)
            } else {
              var req = ['packery']
              for (var i=0; i<data.cards.length; i++) {
                var card_entry = {}
                card_entry['card_'+data.cards[i].split('|')[0]] = "/js/cards/"+data.cards[i].split('|')[0]
                require.config({paths: card_entry})
                req.push('card_'+data.cards[i].split('|')[0])
              }
              var card_data = data.items
              require(req, function(Packery){
                for (var j=0; j<card_data.length; j++) {
                  cards[card_data[j]['type'].split('|')[0]].create('<!modul!>', card_data[j]['label'], j, card_data[j]['type'].split('|'))
                }
                $('#content_modul_page_<!modul!>').data('packery', new Packery( '#content_modul_page_<!modul!>', {itemSelector: '.sh_card', gutter: 20}))
              })
            }
          })  
        }
        $('#content_modul_page_<!modul!>').show()
        var packery = $('#content_modul_page_<!modul!>').data('packery')
        if (!(packery == undefined)) {
          $('#content_modul_page_<!modul!>').data('packery').layout()
        }
      })
      setTimeout(this.update_data, 1000)
    }
  }
  window.modul.page_<!modul!> = _modul
  console.log("page_<!modul!>")
  return _modul
})