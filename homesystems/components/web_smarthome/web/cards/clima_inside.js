define(function () {
  var card = {
    create : function(panel, label, id, _) {
      var html = '<div class="sh_card" data-panel="+panel+" data-id="+id+" data-type="clima_inside"><div class="sh_card_label">'+label+'</div></div>'
      $('#content_modul_page_'+panel).append(html)
      postData('/api', {'action': 'cards_source', 'panel': panel, 'card': id, 
                        'items': ['temperature', 'humidity', 'temperature_set', 'window_state'] }, token=true).then(data => {
        if (data.error != null) {
          console.error(data)
        } else {
          var height = 32
          var values = {}
          var sources = {}
          var ele = $('#content_modul_page_'+panel).children()[id]
          if (data.temperature != null && data.temperature.value != null) {
            height += 24
            var html = '<div data-entry="temperature"><img class="sh_card_item_img" src="/js/img/mdi/thermometer.svg" height="16px" width="16px" onload="SVGInject(this)">'
            html += '<span class="sh_card_item_labels">Temperatur</span>'
            html += '<div class="sh_card_item_value"><span></span> °C</div></div>'
            $(ele).append(html)
            values['temperature'] = data.temperature.value
            sources['temperature'] = data.temperature.source
          }
          if (data.temperature_set != null && data.temperature_set.value != null) {
            height += 24
            var html = '<div data-entry="temperature_set"><img class="sh_card_item_img" src="/js/img/mdi/thermometer.svg" height="16px" width="16px" onload="SVGInject(this)">'
            html += '<span class="sh_card_item_labels">(Set.Temp.)</span>'
            html += '<div class="sh_card_item_value"><span></span> °C</div></div>'
            $(ele).append(html)
            values['temperature_set'] = data.temperature_set.value
            sources['temperature_set'] = data.temperature_set.source
          }
          if (data.humidity != null && data.humidity.value != null) {
            height += 24
            var html = '<div data-entry="humidity"><img class="sh_card_item_img" src="/js/img/mdi/humidity.svg" height="16px" width="16px" onload="SVGInject(this)">'
            html += '<span class="sh_card_item_labels">Feuchtigkeit</span>'
            html += '<div class="sh_card_item_value"><span></span> %</div></div>'
            $(ele).append(html)
            values['humidity'] = data.humidity.value
            sources['humidity'] = data.humidity.source
          }
          if ((data.window_state != null) && (data.window_state.value != null)) {
            height += 24
            var html = '<div data-entry="window_state"><img class="sh_card_item_img" src="/js/img/mdi/window.svg" height="16px" width="16px" onload="SVGInject(this)">'
            html += '<span class="sh_card_item_labels">Fenster</span>'
            html += '<div class="sh_card_item_value"><span></span></div></div>'
            $(ele).append(html)
            values['window_state'] = data.window_state.value
            sources['window_state'] = data.window_state.source
          }
          $(ele).height(height)
          $(ele).data('sources', sources)
          cards.clima_inside.update(panel, id, values)
          var packery = $(ele).parent().data('packery')
          if (packery != undefined) {
            packery.layout()
          }
        }
      })
    },
    update : function(panel, id, values) {
      var eles = $($('#content_modul_page_'+panel).children()[id]).children()
      $($('#content_modul_page_'+panel).children()[id]).find('.sh_card_item_highlight').removeClass('sh_card_item_highlight')
      for (var i=0; i<eles.length; i++) {
        var ele = eles[i]
        if (($(ele).data('entry') == 'temperature') && (values['temperature'] != null)) {
          var div_ele = $(ele).find('.sh_card_item_value'),
              span_ele = $(div_ele[0]).find('span'),
              old_text = $(span_ele[0]).text(),
              new_text = values['temperature'].toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})
          if (old_text != new_text) { $(div_ele[0]).addClass('sh_card_item_highlight') }
          $(span_ele[0]).text(new_text)
        }
        if (($(ele).data('entry') == 'humidity') && (values['humidity'] != null)) {
          var div_ele = $(ele).find('.sh_card_item_value'),
              span_ele = $(div_ele[0]).find('span'),
              old_text = $(span_ele[0]).text(),
              new_text = values['humidity'].toLocaleString('de-DE', {maximumFractionDigits: 0})
          if (old_text != new_text) { $(div_ele[0]).addClass('sh_card_item_highlight') }
          $(span_ele[0]).text(new_text)
        }
        if (($(ele).data('entry') == 'temperature_set') && (values['temperature_set'] != null)) {
          var div_ele = $(ele).find('.sh_card_item_value'),
              span_ele = $(div_ele[0]).find('span'),
              old_text = $(span_ele[0]).text(),
              new_text = values['temperature_set'].toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})
          if (old_text != new_text) { $(div_ele[0]).addClass('sh_card_item_highlight') }
          $(span_ele[0]).text(new_text)
        }
        if (($(ele).data('entry') == 'window_state') && (values['window_state'] != null)) {
          var div_ele = $(ele).find('.sh_card_item_value'),
              span_ele = $(div_ele[0]).find('span'),
              old_text = $(span_ele[0]).text(),
              states = ['zu', 'gekippt', 'offen'],
              new_text = states[values['window_state']]
          if (old_text != new_text) { $(div_ele[0]).addClass('sh_card_item_highlight') }
          $(span_ele[0]).text(new_text)
        }
      }
    }
  }
  if (window.cards == undefined) { window.cards = {}}
  window.cards['clima_inside'] = card
})