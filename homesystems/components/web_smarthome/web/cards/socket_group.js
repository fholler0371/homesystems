define(function () {
  var card = {
    create : function(panel, label, id, params) {
      var html = '<div class="sh_card" data-panel="+panel+" data-id="+id+" data-type="socket_group" data-count="'
      html += (params[1]-1)+'"><div class="sh_card_label">'+label+'</div></div>'
      $('#content_modul_page_'+panel).append(html)
      items = []
      for (var i=0; i<params[1]; i++) {
        items.push('state_'+i)
      } 
      postData('/api', {'action': 'cards_source', 'panel': panel, 'card': id, 
                        'items': items }, token=true).then(data => {
        if (data.error != null) {
          console.error(data)
        } else {
          var height = 32
          var values = {}
          var sources = {}
          var ele = $('#content_modul_page_'+panel).children()[id]
          var keys = Object.keys(data)
          for (var i=0; i<keys.length; i++) {
            height += 24
            var html = '<div data-entry="'+keys[i]+'" class="sh_card_click"><img class="sh_card_item_img" src="/js/img/mdi/socket.svg" '
            html += 'height="16px" width="16px" onload="SVGInject(this)">'
            html += '<span class="sh_card_item_labels">'+data[keys[i]].label+'</span>'
            html += '<div class="sh_card_item_value"></div></div>'
            $(ele).append(html)
            values[keys[i]] = data[keys[i]].value
            sources[keys[i]] = data[keys[i]].source
          }
          $(ele).height(height)
          $(ele).data('sources', sources)
          cards.socket_group.update(panel, id, values)
          var packery = $(ele).data('packery')
          if (packery != undefined) {
            packery.layout()
          }
          $('.sh_card_click').css('cursor', 'pointer').off('click').on('click', function(event) {
            var ele = $(event.currentTarget),
                parent = $(ele[0]).closest('.sh_card'),
                channel = parent.data('sources')[ele.data("entry")],
                value = 1-$(ele).data('value')
            postData('/api', {'action': 'channel_set', 'sensor': channel, 'value': value}, token=true).then(data => {
              if (data.error != null) {
                console.error(data)
              }
            })
           })
        }
      })
    },
    update : function(panel, id, values) {
      var eles = $($('#content_modul_page_'+panel).children()[id]).children()
      for (var i=0; i<eles.length; i++) {
        var ele = eles[i],
            entry = $(ele).data('entry')
        if (values[entry] != null) {
          var value = "an"
          if (values[entry] == 0) {
            value = "aus"
          }
          $(ele).data('value', values[entry])
          $(ele).find('.sh_card_item_value').text(value)
        }
      }
    }
  }
  if (window.cards == undefined) { window.cards = {}}
  window.cards['socket_group'] = card
})