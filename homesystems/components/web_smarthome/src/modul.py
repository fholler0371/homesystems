import os
import time
import mimetypes
import aiohttp
import aiofiles
import urllib.parse
import jwt

from homesystems.lib.aio_http import http_simple

from homesystems.process import c_process_base
from homesystems.lib.helper import overload

#import asyncio

#from aio_process import c_modulcore
#from aio_server import http_simple, web

#from pprint import pprint

from homesystems.lib.aio_eventbus import (
    EVENT_RESPONSE
)

#from aio_process import (
#    EVENT_PROCESS_RUNNING,
#    EVENT_PROCESS_STOPPING,
#    EVENT_RCP_PORT_GET
#)

from homesystems.lib.modul_states import (
    EVENT_STATES_DEVICE_GET,
    EVENT_STATES_CHANNEL_GET,
    EVENT_STATES_CHANNEL_SUBSCRIBE,
    EVENT_STATES_CHANNEL_DATA,
    EVENT_STATES_CHANNEL_SET_VALUE
)

from homesystems.components.web_master import (
    EVENT_WEB_REGISTER_URL,
    EVENT_WEB_REGISTER_URL_DONE,

    EVENT_WEB_GET_ACTION
)

from homesystems.components.authentication import (
    EVENT_AUTH_APP_REGISTER,
    EVENT_AUTH_APP_SECRET_ID,
    EVENT_TRANSFERTOKEN_CHECK
)

from homesystems.components.web_main import (
    EVENT_WEB_MAIN_REGISTER,
    EVENT_WEB_MAIN_REGISTERED
)

#from comp.system import (
#    EVENT_SYSTEM_STATIC_DATA_GET
#)

class c_modul(c_process_base):
    def __init__(self, *args, **kwargs):
        c_process_base.__init__(self, *args, **kwargs)
        self.modul_name = 'main'
        self.http_port = -1
        self.domain_registered = False
        self.auth_registered = False
        self.menu_registered = True
        self.secret_id = ''
        self.auth_domain = ''
        self.auth_path = ''
        self.public_key = None
        self._sensors = {}
    
##############################################

    @overload
    async def doInit(self):
        hs = self.hs
#        await core.bus.add_listener(EVENT_PROCESS_RUNNING, self.doStart)
#        await core.bus.add_listener(EVENT_PROCESS_STOPPING, self.doStop)
        await hs.bus.add_listener(EVENT_WEB_REGISTER_URL_DONE, self.domain_register_done)
        await hs.bus.add_listener(EVENT_WEB_GET_ACTION, self.handle_action)
        await hs.bus.add_listener(EVENT_AUTH_APP_SECRET_ID, self.set_secret_id)
        await hs.bus.add_listener(EVENT_STATES_CHANNEL_DATA, self.channel_data)

##############################################

    async def get_app_list(self, data):
        hs = self.hs
        out = []
        for entry in data['scope'].split(' '):
            if entry in ['sh_view']:
                for entry in hs.config.front:
                    out.append({'url': f"/js/page_{entry}", 'modul': f"page_{entry}"})
        return {'user': data['firstname'], 'modul': out}

##############################################

    async def decode_token(self, token):
        hs = self.hs
        try: 
            key = "\n".join([l.lstrip() for l in self.public_key.split("\n")])
            data = jwt.decode(token, key, algorithms=['RS256'])
            if data['expire'] > time.time():
                return data
        except:
            pass
        return None

##############################################

    async def check_transfer_token(self, token):
        hs = self.hs
        resp = await hs.bus.emit_wait(EVENT_TRANSFERTOKEN_CHECK, target_app="*.authentication", 
                                        context={'app': f"{hs.hostname}.{hs.manifest['name']}", 'token': token})
        return resp

##############################################
    
    async def cards_get(self, panel):
        hs = self.hs
        data = hs.config.front[panel]['cards']
        cards = []
        items = []
        for entry in data:
            if entry['type'] not in cards:
                cards.append(entry['type'])
            items.append({'label': entry['label'], 'type': entry['type']})
        out = []
        return {'cards': cards, 'items': items}

##############################################

    async def sensor_get(self, panel, req_data):
        hs = self.hs
        resp_data = []
        for card in req_data:
            resp_card = {}
            if card is not None:
                for channel in card:
                    source = card[channel]
                    subscribe = False
                    value = None
                    if source in self._sensors:
                        try:
                            value = self._sensors[source]['value']
                            if time.time() > self._sensors[source]['ts'] + 300:
                                hs.log.debug(f"value too old {source}")
                                subscribe = True
                        except:
                            subscribe = True
                    else:
                        subscribe = True
                    if value is None:
                        app, sensor = source.split('.', 1)
                        resp = await hs.bus.emit_wait(EVENT_STATES_CHANNEL_GET, sensor=sensor, target_app=f"*.{app}", 
                                                                                context={'app': f"{hs.hostname}.web_smarthome"})
                        try:
                            value = resp['value']
                            self._sensors[source] = resp
                        except:
                            pass
                    if value is not None:
                        resp_card[channel] = value
                    if subscribe:
                        app, sensor = source.split('.', 1)
                        hs.log.debug(f"subscribe {source}")
                        resp = await hs.bus.emit(EVENT_STATES_CHANNEL_SUBSCRIBE, sensor=sensor, target_app=f"*.{app}", 
                                                                                context={'app': f"{hs.hostname}.web_smarthome"})
                resp_data.append(resp_card)
        return resp_data

##############################################

    async def card_source(self, panel, card, request_channels):
        hs = self.hs
        channels = {}
        for entry in request_channels:
            channels[entry] = {'source': None, 'value': None}
        if 'devices' in hs.config.front[panel]['cards'][card]:
            for entry in hs.config.front[panel]['cards'][card]['devices']:
                app, sensor = entry.split('.')
                resp = await hs.bus.emit_wait(EVENT_STATES_DEVICE_GET, sensor=sensor, target_app=f"*.{app}", 
                                                                        context={'app': f"{hs.hostname}.web_smarthome"})
                try:
                    for channel in resp['channels']:
                        if channel in channels:
                            channels[channel]['value'] = resp['channels'][channel]['value']
                            channels[channel]['source'] = f"{app}.{sensor}.{channel}"
                except:
                    pass
        elif 'channels' in hs.config.front[panel]['cards'][card]:
            i = 0
            for a, b in zip(hs.config.front[panel]['cards'][card]['channels'], channels.keys()):
                channels[b]['source'] = a['channel']
                sensor = a['channel']
                app = sensor.split('.')[0]
                sensor = '.'.join(sensor.split('.')[1:])
                try:
                    resp = await hs.bus.emit_wait(EVENT_STATES_CHANNEL_GET, sensor=sensor, target_app=f"*.{app}", 
                                                                         context={'app': f"{hs.hostname}.web_smarthome"})
                    channels[b]['value'] = resp['value']
                    channels[b]['label'] = a['label']
                except:
                    pass
        return channels

##############################################

    async def channel_data(self, msg):
        hs = self.hs
        if msg.sensor in self._sensors:
            self._sensors[msg.sensor] = msg.context

##############################################

    async def channel_set(self, sensor, value):
        hs = self.hs
        app = sensor.split('.')[0]
        sensor_set = '.'.join(sensor.split('.')[1:])
        resp = await hs.bus.emit(EVENT_STATES_CHANNEL_SET_VALUE, sensor=sensor_set, target_app=f"*.{app}", 
                                                                  context={'app': f"{hs.hostname}.web_smarthome",
                                                                           'value': value})

##############################################
 
    async def handle_action(self, msg):
        hs = self.hs
        data = msg.context
        ### allgemeine Anfragen ###
        if data['action'] == 'get_login_url':
            url = f"https://{self.auth_domain}.{hs.config.base_domain}/{self.auth_path}?response_type=code"
            url += f"&client_id={hs.config.data['client_id']}"
            url += f"&redirect_uri="
            url += urllib.parse.quote_plus(f"https://{hs.config.data['http_domain']}.{hs.config.base_domain}/auth_callback")
            url += f"&scope={'+'.join(hs.config.data['scopes'])}&state="
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                context={'result': {'url': url}}, \
                                target_app= data['response_app'])
        elif data['action'] == 'check_code':
            url = f"https://{self.auth_domain}.{hs.config.base_domain}/oauth/token"
            post_data = f"grant_type=authorization_code&code={data['code']}"
            post_data += f"&client_id={hs.config.data['client_id']}&client_secret={self.secret_id}"
            async with aiohttp.ClientSession() as session:
                async with session.post(url, data=post_data) as resp:
                    await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                        context={'result': await resp.json()}, \
                        target_app= data['response_app'])
        elif data['action'] == 'refresh_token':
            url = f"https://{self.auth_domain}.{hs.config.base_domain}/oauth/token"
            post_data = f"grant_type=refresh_token&refresh_token={data['refresh_token']}"
            post_data += f"&client_id={hs.config.data['client_id']}&client_secret={self.secret_id}"
            async with aiohttp.ClientSession() as session:
                async with session.post(url, data=post_data) as resp:
                    await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                        context={'result': await resp.json()}, \
                        target_app= data['response_app'])
        elif data['action'] == 'check_transfer_token':
            resp_data = await self.check_transfer_token(data['transfer_token'])
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                context={'result': resp_data}, \
                                target_app= data['response_app'])
        elif data['action'] == 'get_module':
            token_data = await self.decode_token(data['token'])
            if token_data is None:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "token invalid"}}, \
                                    target_app= data['response_app'])
                return
            resp_data = await self.get_app_list(token_data)
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                context={'result': resp_data}, \
                                target_app= data['response_app'])
        ### Anwendungs Anfragen ###
        elif data['action'] == 'get_static_data':
            token_data = await self.decode_token(data['token'])
            if token_data is None:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "token invalid"}}, \
                                    target_app= data['response_app'])
                return
            if 'sh_view' not in token_data['scope']:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "scope invalid"}}, \
                                    target_app= data['response_app'])
            resp_data = await self.static_data_get()
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                context={'result': resp_data}, \
                                target_app= data['response_app'])
        elif data['action'] == 'cards_get':
            token_data = await self.decode_token(data['token'])
            if token_data is None:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "token invalid"}}, \
                                    target_app= data['response_app'])
                return
            if 'sh_view' not in token_data['scope']:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "scope invalid"}}, \
                                    target_app= data['response_app'])
            resp_data = await self.cards_get(data['panel'])
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                context={'result': resp_data}, \
                                target_app= data['response_app'])
        elif data['action'] == 'cards_source':
            token_data = await self.decode_token(data['token'])
            if token_data is None:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "token invalid"}}, \
                                    target_app= data['response_app'])
                return
            if 'sh_view' not in token_data['scope']:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "scope invalid"}}, \
                                    target_app= data['response_app'])
            resp_data = await self.card_source(data['panel'], data['card'], data['items'])
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                context={'result': resp_data}, \
                                target_app= data['response_app'])
        elif data['action'] == 'sensor_get':
            token_data = await self.decode_token(data['token'])
            if token_data is None:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "token invalid"}}, \
                                    target_app= data['response_app'])
                return
            if 'sh_view' not in token_data['scope']:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "scope invalid"}}, \
                                    target_app= data['response_app'])
            resp_data = await self.sensor_get(data['panel'], data['sensors'])
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                context={'result': resp_data}, \
                                target_app= data['response_app'])
        elif data['action'] == 'channel_set':
            token_data = await self.decode_token(data['token'])
            if token_data is None:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "token invalid"}}, \
                                    target_app= data['response_app'])
                return
            if 'sh_action' not in token_data['scope']:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "scope invalid"}}, \
                                    target_app= data['response_app'])
            await self.channel_set(data['sensor'], data['value'])
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                context={'result': {'ok': True}}, \
                                target_app= data['response_app'])
        ### nicht bearbeitet Anfragen ###
        else:
            hs.log.error(f"'{data['action']}' not handled")
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                context={'result': {'error': f"'{data['action']}' not handled"}}, \
                                target_app= data['response_app'])

##############################################

    async def handler(self, request):
        hs = self.hs
        headers = {}
        for key in request.headers:
            headers[key] = request.headers[key]
        if request.method == 'GET':
            page_modul = ""
            url_path = request.path
            if url_path == '/':
                url_path = '/index.html'
            elif url_path == '/auth_callback':
                url_path = '/index.html'
            elif url_path == '/favicon.ico':
                url_path = '/house.svg'
            elif url_path.startswith("/js/page_"):
                page_modul = ".".join("_".join(url_path.split("_")[1:]).split(".")[:-1])
                url_path = '/smarthome_main.js'
            elif url_path.startswith("/js/cards/"):
                url_path = "/"+"/".join(url_path.split("/")[2:])
            path = f"{hs.path.comp}/{hs.manifest['name']}/web{url_path}"
            if os.path.isfile(path):
                r = aiohttp.web.StreamResponse(headers={'Content-Type': mimetypes.guess_type(path)[0]})
                await r.prepare(request)
                try:
                    async with aiofiles.open(path, mode='rb') as f:
                        if url_path == '/index.html':
                            content = (await f.read()).decode()
                            params = request.rel_url.query
                            do_in = '<script>\n'
                            for key in params:
                                do_in += f'      var data_{key} = "{params[key]}"\n'
                            do_in += '    </script>'
                            content.replace('<script></script>', do_in)
                            await r.write(content.replace('<script></script>', do_in).encode())
                        elif url_path == '/smarthome_main.js' and len(page_modul) > 0:
                            content = (await f.read()).decode()
                            content = content.replace("<!modul!>", page_modul)
                            content = content.replace("<!icon!>", hs.config.front[page_modul]["icon"])
                            content = content.replace("<!label!>", hs.config.front[page_modul]["label"])
                            await r.write(content.encode())
                        else:
                            await r.write(await f.read())
                    await r.write_eof()
                    return r
                except Exception as e:
                    print(repr(e))
        else:
            print(request.method)
        return aiohttp.web.Response(text="OK", status=999)

##############################################
##############################################

    async def domain_register(self):
        hs = self.hs
        await hs.bus.emit(EVENT_WEB_REGISTER_URL, target_app="*.web_master", 
                          context={'app': f"{hs.hostname}.{hs.manifest['name']}", 
                                   'domain': f"{hs.config.data.get('http_domain', 'noname')}", 
                                   'url':f"http://{hs.ip}:{self.http_port}"})
        if self.domain_registered:
            await hs.run_delay(43200, self.domain_register)
        else:
            await hs.run_delay(15, self.domain_register)

##############################################

    async def domain_register_done(self, msg):
        self.domain_registered = True

##############################################
##############################################

    async def auth_register(self):
        hs = self.hs
        await hs.bus.emit(EVENT_AUTH_APP_REGISTER, target_app="*.authentication", 
                          context={'app': f"{hs.hostname}.{hs.manifest['name']}", 
                                   'client_id': hs.config.data['client_id'], 
                                   'scopes': hs.config.data['scopes'],
                                   'label': hs.config.data['label'],
                                   'icon': hs.config.data['icon'],
                                   'callback': f"https://{hs.config.data['http_domain']}.{hs.config.base_domain}/auth_callback"})
        if self.auth_registered:
            await hs.run_delay(900, self.auth_register)
        else:
            await hs.run_delay(15, self.auth_register)

##############################################

    async def set_secret_id(self, msg):
        hs = self.hs
        self.secret_id = msg.context['secret_id']
        self.auth_domain = msg.context['domain']
        self.auth_path = msg.context['path']
        self.public_key = msg.context['public_key']
        self.auth_registered = True

##############################################
##############################################

    async def main_menu_register(self):
        hs = self.hs
        await hs.bus.emit(EVENT_WEB_MAIN_REGISTER, target_app="*.web_main", 
                            context={'app': f"{hs.hostname}.{hs.manifest['name']}",
                                     'domain': hs.config.data['http_domain'], 
                                     'scopes': hs.config.data['scopes'],
                                     'label': hs.config.data['label'],
                                     'icon': hs.config.data['icon']})
        if self.menu_registered:
            await hs.run_delay(900, self.main_menu_register)
        else:
            await hs.run_delay(15, self.main_menu_register)

##############################################

    async def menu_registered_done(self, msg):
        self.menu_registered = True

##############################################
##############################################

    @overload
    async def doStart(self):
        hs = self.hs
        self.http_port = hs.reserved_ports[0]
        await self.domain_register()
        await self.auth_register()
        await self.main_menu_register()
        try:
            self.server = http_simple(hs.log, self.http_port, self.handler)
            await self.server.setup()
        except Exception as e:
            hs.log.error(repr(e))
            hs.kill.set()
        hs.log.info(f"{hs.name} running http server on port: {self.http_port}")

