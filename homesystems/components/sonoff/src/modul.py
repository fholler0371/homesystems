import os
import json
from datetime import date, timedelta

from homesystems.process import c_process_base
from homesystems.lib.helper import overload

from .const import DEVICES

from homesystems.const import (
    EVENT_PROCESS_STOPPING
)

from homesystems.lib.aio_mqtt import (
    EVENT_MQTT_CONNECT,
    EVENT_MQTT_CONNECTED,
    EVENT_MQTT_SUBSCRIBE,
    EVENT_MQTT_MESSAGE,
    EVENT_MQTT_PUBLISH
)

from homesystems.lib.modul_states import (
    EVENT_STATES_DEVICE_ADD,
    EVENT_STATES_CHANNEL_ADD,
    EVENT_STATES_VALUE_NEW,
    EVENT_STATES_DEVICE_GET,
    EVENT_STATES_CHANNEL_GET,
    EVENT_STATES_CHANNEL_SET_VALUE
)

from homesystems.components.history_day import (
    EVENT_HISTORY_DAY_PUSH
)

class c_modul(c_process_base):
    def __init__(self, *args, **kwargs):
        c_process_base.__init__(self, *args, **kwargs)
        self.modul_name = 'main'
        self._unknown_device = ['YTF IR Bridge', 'Sonoff ZbBridge']
        self._devices = {}
        self._registered_filename = ''
    
##############################################

    @overload
    async def doInit(self):
        hs = self.hs
        await hs.bus.add_listener(EVENT_MQTT_CONNECTED, self.connected)
        await hs.bus.add_listener(EVENT_PROCESS_STOPPING, self.doStop)
        await hs.bus.add_listener(EVENT_MQTT_MESSAGE, self.newValue)
        await hs.bus.add_listener(EVENT_STATES_CHANNEL_SET_VALUE, self.setValue)

##############################################
#########################.name#####################

    async def connected(self, _):
        hs = self.hs
        await hs.bus.emit(EVENT_MQTT_SUBSCRIBE, context={'value': 'tasmota/discovery/+/config'})
        await hs.bus.emit(EVENT_MQTT_SUBSCRIBE, context={'value': 'sonoff/+/tele/SENSOR'})
        await hs.bus.emit(EVENT_MQTT_SUBSCRIBE, context={'value': 'sonoff/+/stat/RESULT'})
        await hs.bus.emit(EVENT_MQTT_SUBSCRIBE, context={'value': 'sonoff/+/tele/STATE'})
        hs.log.debug('MQTT Connected')

##############################################

    async def newValue(self, msg):
        hs = self.hs
        if msg.sensor.startswith('tasmota/discovery/'):
            await self.discovery(msg.context)
        elif msg.sensor.endswith('/tele/SENSOR'):
            device = msg.sensor.split('/')[1]        
            if device in self._devices:
                d_data = self._devices[device]
                found = False
                for channel in DEVICES[d_data['type']]['channels']:
                    value = None
                    if 'topic_path' in channel:
                        x_value = msg.context
                        try:
                            for entry in channel['topic_path'].split('.'):
                                x_value = x_value[entry]
                        except:
                            x_value = None
                        value = x_value
                    if value is not None:
                        found = True
                        await hs.bus.emit(EVENT_STATES_VALUE_NEW, sensor=f"{d_data['device']}.{channel['name']}", context={'value': value})
                if not found:
                    print(msg)
        elif msg.sensor.endswith('stat/RESULT') or msg.sensor.endswith('tele/STATE'):
            device = msg.sensor.split('/')[1]        
            if device in self._devices:
                d_data = self._devices[device]
                label = self._devices[device]['device']
                for cb_channel in DEVICES[self._devices[device]['type']]['channels']:
                    if 'topic' in cb_channel and cb_channel['topic'] in msg.context:
                        value = msg.context[cb_channel['topic']]
                        if 'v_convert' in cb_channel:
                            value = cb_channel['v_convert'](value)
                        await hs.bus.emit(EVENT_STATES_VALUE_NEW, sensor=f"{label}.{cb_channel['name']}", context={'value': value})
        else:
            print(msg)

##############################################

    async def setValue(self, msg):
        hs = self.hs
        device, channel = msg.sensor.split('.')
        value = msg.context['value']
        for label in self._devices:
            if self._devices[label]['device'] == device:
                for cb_channel in DEVICES[self._devices[label]['type']]['channels']:
                    if cb_channel['name'] == channel:
                     await hs.bus.emit(EVENT_MQTT_PUBLISH, sensor=f"sonoff/{label}/cmnd/{cb_channel['topic']}", context={'value': value})

##############################################

    async def discovery(self, data):
        hs = self.hs
        if data['md'] not in DEVICES:
            if data['md'] not in self._unknown_device:
                self._unknown_device.append(data['md'])
                hs.log.warn(f"unkown device type {data['md']}")
            return
        if data['hn'] in self._devices:
            #already registred
            return
        devices = hs.config.data.get('devices', {})
        _device_data = devices.get(data['hn'], data['hn'])
        if "|" in _device_data:
            device, label = _device_data.split("|", 1)
        else:
            device = label = _device_data
        ddata = DEVICES[data['md']]
        await hs.bus.emit(EVENT_STATES_DEVICE_ADD, context={'name': device, 'label': label, 'type': ddata['type']})
        self._devices[data['hn']] = {'device': device, 'type': data['md']}
        for ch_data in ddata['channels']:
            await hs.bus.emit(EVENT_STATES_CHANNEL_ADD, sensor=device, context={'name': ch_data['name'], 'type': ch_data['type']})
            
##############################################

    async def send_history_day(self):
        hs = self.hs
        if self.running:
            await hs.run_delay(60, self.send_history_day)
        for type in DEVICES:
            for channel in DEVICES[type]['channels']:
                if 'history' in channel and channel['history'] == 'day':
                    for device in self._devices:
                        if type == self._devices[device]['type']:
                            data = await hs.bus.emit_wait(EVENT_STATES_CHANNEL_GET, sensor=f"{self._devices[device]['device']}.{channel['name']}")
                            c_name = channel['name']
                            if 'history_name' in channel:
                                c_name = channel['history_name']
                            if data is not None:
                                await hs.bus.emit(EVENT_HISTORY_DAY_PUSH, sensor=f"{hs.manifest['name']}.{self._devices[device]['device']}.{c_name}", 
                                        context={'value': data['value']*1000, 'date': (date.today()-timedelta(days=1)).strftime('%Y-%m-%d')}, 
                                        target_app="*.history_day")

##############################################
##############################################

    async def init_states(self):
        hs = self.hs
        self._registered_filename = f"{hs.path.data}/{hs.manifest['name']}_reg.json"
        if os.path.isfile(self._registered_filename):
            with open(self._registered_filename, "r") as f:
                self._devices = json.loads(f.read())

##############################################

    @overload
    async def doStart(self):
        hs = self.hs
        await hs.bus.emit(EVENT_MQTT_CONNECT)
        await self.init_states()
        await hs.run_delay(36000, self.save_devices)
        await hs.run_delay(20, self.send_history_day)

##############################################

    async def save_devices(self):
        hs = self.hs
        if self.running:
            await hs.run_delay(36000, self.save_devices)
        with open(self._registered_filename, "w") as f:
            f.write(json.dumps(self._devices))

##############################################

    @overload
    async def doStop(self, msg):
        self.running = False
        await self.save_devices()
        self.stopped = True
