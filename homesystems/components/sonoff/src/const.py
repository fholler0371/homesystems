def onoff_int(value):
    if value.lower() == 'on':
        return 1
    return 0

DEVICES = {'Sonoff Pow': {'type': 'sonoff_pow',
                          'channels': [{'name': 'state', 'type': 'int', 'topic_path': '', 'topic': 'POWER', 
                                        'v_convert': onoff_int},
                                       {'name': 'voltage', 'type': 'int', 'topic_path': 'ENERGY.Voltage'},
                                       {'name': 'current', 'type': 'float', 'topic_path': 'ENERGY.Current'},
                                       {'name': 'cos', 'type': 'float', 'topic_path': 'ENERGY.Factor'},
                                       {'name': 'power', 'type': 'float', 'topic_path': 'ENERGY.Power'},
                                       {'name': 'energy_today', 'type': 'float', 'topic_path': 'ENERGY.Today'},
                                       {'name': 'energy_yesterday', 'type': 'float', 'topic_path': 'ENERGY.Yesterday', 'history': 'day', 'history_name': 'energy'}]},
          'Sonoff S2X': {'type': 'sonoff_s20',
                         'channels': [{'name': 'state', 'type': 'int', 'topic_path': '', 'topic': 'POWER', 
                                       'v_convert': onoff_int}]}
          }