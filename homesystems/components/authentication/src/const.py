EVENT_AUTH_APP_REGISTER = 'authentication_applictaion_register'
EVENT_AUTH_APP_SECRET_ID = 'authentication_applictaion_secret'
EVENT_AUTH_USERS_GET = 'authentication_users_get'
EVENT_AUTH_USER_ADD = 'authentication_user_add'
EVENT_AUTH_USER_DEL = 'authentication_user_del'
EVENT_AUTH_REC_GET = 'authentication_rec_get'
EVENT_AUTH_REC_SET = 'authentication_rec_set'
EVENT_AUTH_PASSWORD = 'authentication_password'
EVENT_AUTH_MFA_GET = 'authentication_mfa_get'
EVENT_AUTH_RIGHTS_SET = 'authentication_rights_set'
EVENT_TRANSFERTOKEN_GET = 'authentication_transfer_get'
EVENT_TRANSFERTOKEN_CHECK = 'authentication_transfer_check'