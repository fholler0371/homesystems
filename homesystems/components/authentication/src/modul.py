import os
import time
import socket
import aiohttp
import mimetypes
import aiofiles
import aiosqlite
from uuid import uuid4 as uuid
from passlib.hash import pbkdf2_sha256
import pyotp
import jwt

from homesystems.lib.aio_http import http_simple

from homesystems.process import c_process_base
from homesystems.lib.helper import overload

from homesystems.lib.aio_eventbus import (
    EVENT_RESPONSE
)

from homesystems.components.web_master import (
    EVENT_WEB_REGISTER_URL,
    EVENT_WEB_REGISTER_URL_DONE,

    EVENT_WEB_GET_ACTION
)

from .const import (
    EVENT_AUTH_APP_REGISTER,
    EVENT_AUTH_APP_SECRET_ID,
    EVENT_AUTH_USERS_GET,
    EVENT_AUTH_USER_ADD,
    EVENT_AUTH_USER_DEL,
    EVENT_AUTH_REC_GET,
    EVENT_AUTH_REC_SET,
    EVENT_AUTH_PASSWORD,
    EVENT_AUTH_MFA_GET,
    EVENT_AUTH_RIGHTS_SET,
    EVENT_TRANSFERTOKEN_GET,
    EVENT_TRANSFERTOKEN_CHECK
)

TABLE_APP = "applications"
TABLE_USER = "user"
TABLE_SCOPE = "scope"
TABLE_SCOPE_MFA = "mfascope"
TABLE_SESSION = "session"
TABLE_RSA = "rsa_key"
TABLE_DEVICE_SETTINGS = "device_settings"
TABLE_TRANSFER = "transfer"

class c_modul(c_process_base):
    def __init__(self, *args, **kwargs):
        c_process_base.__init__(self, *args, **kwargs)
        self.modul_name = 'main'
        self.http_port = -1
        self.domain_registered = False
        self.private_key = None
        self.public_key = None
        self.db_path = None
        self.local_ip = ''
        self.local_ip_age = 0
    
##############################################

    @overload
    async def doInit(self):
        hs = self.hs
        await hs.bus.add_listener(EVENT_AUTH_APP_REGISTER, self.register_client_app)
        await self.init_db()
        await hs.bus.add_listener(EVENT_WEB_REGISTER_URL_DONE, self.domain_register_done)
        await hs.bus.add_listener(EVENT_WEB_GET_ACTION, self.handle_action)
        await hs.bus.add_listener(EVENT_AUTH_USERS_GET, self.users_get)
        await hs.bus.add_listener(EVENT_AUTH_USER_ADD, self.user_add)
        await hs.bus.add_listener(EVENT_AUTH_USER_DEL, self.user_del)
        await hs.bus.add_listener(EVENT_AUTH_REC_GET, self.rec_get)
        await hs.bus.add_listener(EVENT_AUTH_REC_SET, self.rec_set)
        await hs.bus.add_listener(EVENT_AUTH_PASSWORD, self.set_password)
        await hs.bus.add_listener(EVENT_AUTH_MFA_GET, self.mfa_get)
        await hs.bus.add_listener(EVENT_AUTH_RIGHTS_SET, self.rights_set)
        await hs.bus.add_listener(EVENT_TRANSFERTOKEN_GET, self.transfer_token_get)
        await hs.bus.add_listener(EVENT_TRANSFERTOKEN_CHECK, self.transfer_token_check)

##############################################

    async def init_db(self):
        hs = self.hs
        self.db_path = f"{hs.path.data}/{hs.manifest['name']}.sqlite"
        async with aiosqlite.connect(self.db_path) as db:
            #sql = f"drop table {TABLE_RSA_OLD}"
            #await db.execute(sql)
            # app, client_id, secret_id, scopes, label, icon,callback id
            sql = f"CREATE TABLE IF NOT EXISTS {TABLE_APP} ("
            sql += "id INTEGER PRIMARY KEY, "
            sql += "app TEXT, "
            sql += "client_id TEXT, "
            sql += "secret_id TEXT, "
            sql += "scopes TEXT, "
            sql += "label TEXT, "
            sql += "icon TEXT, "
            sql += "callback TEXT)"
            await db.execute(sql)
            # id, user, firstname, lastname, password, mfa, mail, created, last
            sql = f"CREATE TABLE IF NOT EXISTS {TABLE_USER} ("
            sql += "id INTEGER PRIMARY KEY, "
            sql += "user TEXT, "
            sql += "firstname TEXT, "
            sql += "lastname TEXT, "
            sql += "password TEXT, "
            sql += "mfa TEXT, "
            sql += "mail TEXT, "
            sql += "created INT, "
            sql += "last INT)"
            await db.execute(sql)
            # id, user_id, scope
            sql = f"CREATE TABLE IF NOT EXISTS {TABLE_SCOPE} ("
            sql += "id INTEGER PRIMARY KEY, "
            sql += "user_id INT, "
            sql += "scope TEXT)"
            await db.execute(sql)
            # id, useEVENT_TRANSFERTOKEN_CHECKr_id, scope
            sql = f"CREATE TABLE IF NOT EXISTS {TABLE_SCOPE_MFA} ("
            sql += "id INTEGER PRIMARY KEY, "
            sql += "user_id INT, "
            sql += "scope TEXT)"
            await db.execute(sql)
            # id code user_id, scope created last
            sql = f"CREATE TABLE IF NOT EXISTS {TABLE_SESSION} ("
            sql += "id INTEGER PRIMARY KEY, "
            sql += "user_id INT, "
            sql += "scope TEXT, "
            sql += "code TEXT, "
            sql += "access_token TEXT, "
            sql += "refresh_token TEXT, "
            sql += "access_token_timeout INT, "
            sql += "created INT, "
            sql += "last INT)"
            await db.execute(sql)
            # id code user_id, scope created last
            sql = f"CREATE TABLE IF NOT EXISTS {TABLE_TRANSFER} ("
            sql += "id INTEGER PRIMARY KEY, "
            sql += "session_id INT, "
            sql += "trasfer_token TEXT, "
            sql += "created INT)"
            await db.execute(sql)
            # id private public
            sql = f"CREATE TABLE IF NOT EXISTS {TABLE_RSA} ("
            sql += "id INTEGER PRIMARY KEY, "
            sql += "private_key TEXT, "
            sql += "public_key TEXT)"
            await db.execute(sql)
            await db.commit()
            # id user_id device_id timeout_clock timeout_logout created last
            sql = f"CREATE TABLE IF NOT EXISTS {TABLE_DEVICE_SETTINGS} ("
            sql += "id INTEGER PRIMARY KEY, "
            sql += "user_id INT, "
            sql += "device_id TEXT, "
            sql += "timeout_clock INT, "
            sql += "timeout_logout INT, "
            sql += "created INT, "
            sql += "last INT)"
            await db.execute(sql)
            await db.commit()
            async with db.execute(f"SELECT private_key, public_key FROM {TABLE_RSA} WHERE id='1' ") as cursor:
                async for row in cursor:
                    self.private_key = row[0]
                    self.public_key = row[1]
            if self.private_key is None:
                from cryptography.hazmat.backends import default_backend
                from cryptography.hazmat.primitives.asymmetric import rsa
                from cryptography.hazmat.primitives import serialization
                priv_key = rsa.generate_private_key(public_exponent=65537, key_size=2048, backend=default_backend())
                pub_key = priv_key.public_key()
                self.private_key = priv_key.private_bytes(encoding=serialization.Encoding.PEM, format=serialization.PrivateFormat.PKCS8,
                                                          encryption_algorithm=serialization.NoEncryption()).decode()
                self.public_key = pub_key.public_bytes(encoding=serialization.Encoding.PEM, format=serialization.PublicFormat.SubjectPublicKeyInfo).decode()
                sql = f"INSERT INTO {TABLE_RSA} (private_key, public_key) VALUES ('{self.private_key}', '{self.public_key}')"
                await db.execute(sql)
                await db.commit()        

##############################################
##############################################

    async def create_start_user(self):
        hs = self.hs
        data = hs.config.data.get("start_user", {})
        if data == {}:
            return
        password = pbkdf2_sha256.hash(data.get('password', str(time.time())))
        user = data.get("user", "")
        firstname = data.get("firstname", user)
        lastname = data.get("lastname", user)
        timestamp = int(time.time())
        scope = data.get('scope', [])
        async with aiosqlite.connect(self.db_path) as db:
            id = -1
            async with db.execute(f"SELECT id FROM {TABLE_USER} WHERE user='{user}' ") as cursor:
                async for row in cursor:
                    id = row[0]
            if id == -1:
                sql = f"INSERT INTO {TABLE_USER} (user, password, firstname, lastname, created, last) VALUES "
                sql += f"('{user}', '{password}', '{firstname}', '{lastname}', '{timestamp}', '{timestamp}')"
                await db.execute(sql)
                await db.commit()
                async with db.execute(f"SELECT id FROM {TABLE_USER} WHERE user='{user}' ") as cursor:
                    async for row in cursor:
                        id = row[0]
            for entry in scope:
                scope_id = -1
                async with db.execute(f"SELECT id FROM {TABLE_SCOPE_MFA} WHERE user_id='{id}' and scope='{entry}' ") as cursor:
                    async for row in cursor:
                        scope_id = row[0]
                if scope_id == -1:
                    sql = f"INSERT INTO {TABLE_SCOPE_MFA} (user_id, scope) VALUES ('{id}', '{entry}')"
                    await db.execute(sql)
                    await db.commit()

##############################################

    async def login_user(self, data):
        hs = self.hs
        level = 0
        user_id = -1
        password = ''
        mfa = ''
        timestamp = int(time.time())
        data['name'] = data['name'].replace("'", "")
        async with aiosqlite.connect(self.db_path) as db:
            async with db.execute(f"SELECT id, password, mfa FROM {TABLE_USER} WHERE user= '%s' " % (data['name'], )) as cursor:
                async for row in cursor:
                    user_id = row[0]
                    password = row[1]
                    mfa = row[2]
            if user_id == -1:
                return {'valid': False}
            if not pbkdf2_sha256.verify(data['password'], password):
                return {'valid': False}
            level = 1 #password is 
            scope = []
            async with db.execute(f"SELECT scope FROM {TABLE_SCOPE} WHERE user_id= '%i' " % (user_id, )) as cursor:
                async for row in cursor:
                    scope.append(row[0])
            if data['mfa'] == '' or mfa is None or mfa == '':
                if self.local_ip_age+300 < time.time():
                    try:
                        self.local_ip = socket.gethostbyname(f"home.{hs.config.base_domain}")
                        self.local_ip_age = time.time()
                    except:
                        hs.log.error("can not get external ip")
                if self.local_ip == data['remote_ip']:
                    level = 2
            else:
                if pyotp.TOTP(mfa).verify(data['mfa']):
                    level = 2
            if level == 2:
                async with db.execute(f"SELECT scope FROM {TABLE_SCOPE_MFA} WHERE user_id= '%i' " % (user_id, )) as cursor:
                    async for row in cursor:
                        scope.append(row[0])
            allowed_scopes = []
            for entry in data['scope'].split(' '):
                if entry in scope:
                    allowed_scopes.append(entry)
            if len(allowed_scopes) == 0:
                return {'valid': False}
            sql = f"UPDATE {TABLE_USER} SET last='{timestamp}' WHERE id='{user_id}'"
            await db.execute(sql)
            await db.commit()
            #check callback
            callback = ''
            data['client_id'] = data['client_id'].replace("'", "")
            async with db.execute(f"SELECT callback FROM {TABLE_APP} WHERE client_id= '%s' " % (data['client_id'], )) as cursor:
                    async for row in cursor:
                        callback = row[0]
            if callback != data['redirect_uri']:
                return {'valid': False}
            code = (str(uuid())+str(uuid())).replace('-', '')
            sql = f"INSERT INTO {TABLE_SESSION} (user_id, code, scope, created, last) VALUES ('%i', '%s', '%s', '%i', '%i')"
            await db.execute(sql % (user_id, code, " ".join(allowed_scopes), timestamp, timestamp))
            await db.commit()
            url = f"{callback}?code={code}&state={data['state']}"
            return {'valid': True, 'redirect_url': url}

##############################################

    async def get_token(self, data):
        hs = self.hs
        if data['grant_type'] == 'authorization_code':
            return await self.get_token_from_code(data)
        elif data['grant_type'] == 'refresh_token':
            return await self.get_toke_from_refresh_token(data)

##############################################

    async def encode_jwt_access_token(self, user, name, expire, scope):
        data = {'firstname': user, 'name': name, 'expire': expire, 'scope': scope}
        key = "\n".join([l.lstrip() for l in self.private_key.split("\n")])
        token = jwt.encode(data, key, algorithm='RS256')
        return token

##############################################

    async def get_toke_from_refresh_token(self, data):
        hs = self.hs
        async with aiosqlite.connect(self.db_path) as db:
            data['refresh_token'] = data['refresh_token'].replace("'", "")
            # id code user_id, scope created last
            session_id = -1
            async with db.execute(f"SELECT id, user_id, scope FROM {TABLE_SESSION} WHERE refresh_token= '%s' " % (data['refresh_token'], )) as cursor:
                async for row in cursor:
                    session_id = row[0]
                    user_id = row[1]
                    scope = row[2]
            if session_id == -1:
                return {'valid': False}
            # app, client_id, secret_id, scopes, label, icon, id
            app_id = -1
            data['client_id'] = data['client_id'].replace("'", "")
            async with db.execute(f"SELECT id, secret_id FROM {TABLE_APP} WHERE client_id= '%s' " % (data['client_id'], )) as cursor:
                async for row in cursor:
                    app_id = row[0]
                    secret_id = row[1]
            if app_id == -1:
                return {'valid': False}
            if secret_id != data['client_secret']:
                return {'valid': False}
            firstname = ""
            name = ""
            async with db.execute(f"SELECT firstname, user FROM {TABLE_USER} WHERE id= '%s' " % (user_id, )) as cursor:
                async for row in cursor:
                    firstname = row[0]
                    name = row[1]
            refresh_token = (str(uuid())+str(uuid())+str(uuid())+str(uuid())).replace('-', '')
            access_token_timeout = int(time.time()+7200)
            access_token = await self.encode_jwt_access_token(firstname, name, access_token_timeout, scope)
            sql = f"UPDATE {TABLE_SESSION} SET access_token='{access_token}', refresh_token='{refresh_token}', "
            sql += f"access_token_timeout='{access_token_timeout}' WHERE id='{session_id}'"
            await db.execute(sql)
            await db.commit()
            return {"access_token":access_token, "token_type":"Bearer", "expires_in":7200, "refresh_token":refresh_token}
        return {'valid': False}

##############################################

    async def get_token_from_code(self, data):
        hs = self.hs
        async with aiosqlite.connect(self.db_path) as db:
            data['code'] = data['code'].replace("'", "")
            session_id = -1
            async with db.execute(f"SELECT id, user_id, scope FROM {TABLE_SESSION} WHERE code= '%s' " % (data['code'], )) as cursor:
               async for row in cursor:
                    session_id = row[0]
                    user_id = row[1]
                    scope = row[2]
            if session_id == -1:
                return {'valid': False}
            # app, client_id, secret_id, scopes, label, icon, id
            app_id = -1
            data['client_id'] = data['client_id'].replace("'", "")
            async with db.execute(f"SELECT id, secret_id FROM {TABLE_APP} WHERE client_id= '%s' " % (data['client_id'], )) as cursor:
                async for row in cursor:
                    app_id = row[0]
                    secret_id = row[1]
            if app_id == -1:
                return {'valid': False}
            if secret_id != data['client_secret']:
                return {'valid': False}
            firstname = ''
            name = ''
            async with db.execute(f"SELECT firstname, user FROM {TABLE_USER} WHERE id= '%s' " % (user_id, )) as cursor:
                async for row in cursor:
                    firstname = row[0]
                    name = row[1]
            refresh_token = (str(uuid())+str(uuid())+str(uuid())+str(uuid())).replace('-', '')
            access_token_timeout = int(time.time()+7200)
            access_token = await self.encode_jwt_access_token(firstname, name, access_token_timeout, scope)
            sql = f"UPDATE {TABLE_SESSION} SET access_token='{access_token}', refresh_token='{refresh_token}', "
            sql += f"access_token_timeout='{access_token_timeout}', code='' WHERE id='{session_id}'"
            await db.execute(sql)
            await db.commit()
            return {"access_token":access_token, "token_type":"Bearer", "expires_in":7200, "refresh_token":refresh_token}
        return {'valid': False}

##############################################

    async def transfer_token_get(self, msg):
        hs = self.hs
        data = msg.context
        session_id = -1
        token = data['token'].replace("'", "")
        async with aiosqlite.connect(self.db_path) as db:
            async with db.execute(f"SELECT id FROM {TABLE_SESSION} WHERE access_token='{token}' ") as cursor:
                async for row in cursor:
                    session_id = row[0]
            timestamp = int(time.time())
            if session_id == -1:
                await hs.bus.emit(EVENT_RESPONSE, target_app=msg.context['app'], context={'result': 'no'}, org_id=msg.id)
            token = (str(uuid())+str(uuid())).replace('-', '')
            sql = f"INSERT INTO {TABLE_TRANSFER} (session_id, trasfer_token, created) VALUES ('{session_id}', '{token}', '{timestamp}')"
            await db.execute(sql)
            await db.commit()
            await hs.bus.emit(EVENT_RESPONSE, target_app=msg.context['app'], context={'result': token}, org_id=msg.id)

##############################################

    async def transfer_token_check(self, msg):
        hs = self.hs
        data = msg.context
        token = data['token']
        timestamp = (time.time())
        session_id = -1
        async with aiosqlite.connect(self.db_path) as db:
            async with db.execute(f"SELECT session_id FROM {TABLE_TRANSFER} WHERE trasfer_token='{token}' and created>'{timestamp-20}'") as cursor:
                async for row in cursor:
                    session_id = row[0]
            if session_id == -1:
                return
            user_id = -1
            async with db.execute(f"SELECT user_id, scope FROM {TABLE_SESSION} WHERE id= '%i' " % (session_id, )) as cursor:
                async for row in cursor:
                    user_id = row[0]
                    scope = row[1]
            if user_id == -1:
                return
            firstname = ""
            name = ""
            async with db.execute(f"SELECT firstname, user FROM {TABLE_USER} WHERE id= '%s' " % (user_id, )) as cursor:
                async for row in cursor:
                    firstname = row[0]
                    name = row[1]
            refresh_token = (str(uuid())+str(uuid())+str(uuid())+str(uuid())).replace('-', '')
            access_token_timeout = int(time.time()+7200)
            access_token = await self.encode_jwt_access_token(firstname, name, access_token_timeout, scope)
            sql = f"INSERT INTO {TABLE_SESSION} (user_id, scope, access_token, refresh_token, access_token_timeout, created, last) VALUES"
            sql += f" ('{user_id}', '{scope}', '{access_token}', '{refresh_token}', '{access_token_timeout}', '{timestamp}', '{timestamp}')"
            await db.execute(sql)
            await db.commit()  
            data = {'access_token': access_token, 'expires_in': 7200, 'refresh_token': refresh_token}
            await hs.bus.emit(EVENT_RESPONSE, target_app=msg.context['app'], context={'result': data}, org_id=msg.id)#

##############################################

    async def do_logout(self, token):
        hs = self.hs
        token = token.replace("'", "")
        timestamp = (time.time())
        session_id = -1
        async with aiosqlite.connect(self.db_path) as db:
            user_id = -1
            async with db.execute(f"SELECT user_id FROM {TABLE_SESSION} WHERE access_token='{token}'") as cursor:
                async for row in cursor:
                    user_id = row[0]
            if user_id == -1:
                return
            sql = f"DELETE FROM {TABLE_SESSION} WHERE user_id='{user_id}'"
            await db.execute(sql)
            await db.commit()  

##############################################
##############################################

    async def register_client_app(self, msg):
        hs = self.hs
        data = msg.context 
        async with aiosqlite.connect(self.db_path) as db:
            id = -1
            async with db.execute(f"SELECT id, secret_id FROM {TABLE_APP} WHERE client_id='{data['client_id']}' ") as cursor:
                async for row in cursor:
                    id = row[0]
                    secret_id = row[1]
            if "secret_id" not in data:
                data['secret_id'] = (str(uuid())+str(uuid())+str(uuid())+str(uuid())).replace('-', '')
            if id == -1:
                sql = f"INSERT INTO {TABLE_APP} ("
                sql += "app, client_id, secret_id, scopes, label, icon, callback"
                sql += ") VALUES ("
                sql += f"'{data['app']}', '{data['client_id']}', '{data['secret_id']}',  '{','.join(data['scopes'])}', "
                sql += f"'{data['label']}', '{data['icon']}', '{data['callback']}')"
            else:
                sql = f"UPDATE {TABLE_APP} SET "
                sql += f"app='{data['app']}', "
                sql += f"scopes='{','.join(data['scopes'])}', "
                sql += f"label='{data['label']}', "
                sql += f"app='{data['icon']}', "
                sql += f"callback='{data['callback']}' "
                sql += f"WHERE id='{id}'"
            await db.execute(sql)
            await db.commit()
            if id == -1:
                async with db.execute(f"SELECT secret_id FROM {TABLE_APP} WHERE client_id='{data['client_id']}' ") as cursor:
                    async for row in cursor:
                       secret_id = row[0]
        if self.public_key is not None:
            await hs.bus.emit(EVENT_AUTH_APP_SECRET_ID, context={'secret_id': secret_id, 'domain': hs.config.data['http_domain'],
                                'path': hs.config.data['auth_path'], 'public_key': self.public_key}, target_app=data['app'])

##############################################

    async def get_app_web_info(self, client_id):
        hs = self.hs
        async with aiosqlite.connect(self.db_path) as db:
            id = -1
            async with db.execute(f"SELECT label, icon FROM {TABLE_APP} WHERE client_id='{client_id}' ") as cursor:
                async for row in cursor:
                    return {'label': row[0], 'icon': row[1]}
        return {'error': 'no app found'}

##############################################

    async def users_get(self, msg):
        hs = self.hs
        users = []
        async with aiosqlite.connect(self.db_path) as db:
            async with db.execute(f"SELECT user FROM {TABLE_USER} ORDER BY user ") as cursor:
                async for row in cursor:
                    users.append(row[0])
        await hs.bus.emit(EVENT_RESPONSE, target_app=msg.context['app'], context={'result': users}, org_id=msg.id) 

##############################################

    async def user_add(self, msg):
        hs = self.hs
        name = msg.context['name'].replace("'", "")
        async with aiosqlite.connect(self.db_path) as db:
            user_id = -1
            async with db.execute(f"SELECT id FROM {TABLE_USER} WHERE user='%s' " % name) as cursor:
                async for row in cursor:
                    user_id = row[0]
            if user_id == -1:
                timestamp = int(time.time())
                sql = f"INSERT INTO {TABLE_USER} (user, password, firstname, lastname, created, last) VALUES "
                sql += f"('{name}', '', '{name}', '{name}', '{timestamp}', '{timestamp}')"
                await db.execute(sql)
                await db.commit()

##############################################

    async def user_del(self, msg):
        hs = self.hs
        name = msg.context['name'].replace("'", "")
        async with aiosqlite.connect(self.db_path) as db:
            user_id = -1
            async with db.execute(f"SELECT id FROM {TABLE_USER} WHERE user='%s' " % name) as cursor:
                async for row in cursor:
                    user_id = row[0]
            if user_id > -1:
                sql = f"DELETE FROM {TABLE_SCOPE} WHERE user_id='%i'"
                await db.execute(sql % (user_id, ))
                sql = f"DELETE FROM {TABLE_SCOPE_MFA} WHERE user_id='%i'"
                await db.execute(sql % (user_id, ))
                sql = f"DELETE FROM {TABLE_USER} WHERE id='%i'"
                await db.execute(sql % (user_id, ))
                await db.commit()

##############################################

    async def rec_get(self, msg):
        hs = self.hs
        name = msg.context['name'].replace("'", "")
        firstname = ''
        lastname = ''
        mail = ''
        rights_all = []
        rights = []
        rights_mfa = []
        async with aiosqlite.connect(self.db_path) as db:
            user_id = -1
            if msg.context['rights']:
                async with db.execute(f"SELECT scopes FROM {TABLE_APP}") as cursor:
                    async for row in cursor:
                        for entry in row[0].split(","):
                            rights_all.append(entry)
            async with db.execute(f"SELECT firstname, lastname, mail, id FROM {TABLE_USER} WHERE user='%s' " % name) as cursor:
                async for row in cursor:
                    firstname = row[0]
                    lastname = row[1]
                    mail = row[2]
                    user_id = row[3]
            if msg.context['rights']:
                async with db.execute(f"SELECT scope FROM {TABLE_SCOPE} WHERE user_id='%i' " % user_id) as cursor:
                    async for row in cursor:
                        rights.append(row[0])
                async with db.execute(f"SELECT scope FROM {TABLE_SCOPE_MFA} WHERE user_id='%i' " % user_id) as cursor:
                    async for row in cursor:
                        rights_mfa.append(row[0])
        if msg.context['rights']:
            rec = {'firstname': firstname, 'lastname': lastname, 'mail': mail, 'rights_avail': sorted(rights_all), 'rights': rights, 'rights_mfa': rights_mfa}
        else:
            rec = {'firstname': firstname, 'lastname': lastname, 'mail': mail}
        await hs.bus.emit(EVENT_RESPONSE, target_app=msg.context['app'], context={'result': rec}, org_id=msg.id)     

##############################################

    async def rec_set(self, msg):
        hs = self.hs
        name = msg.context['name'].replace("'", "")
        firstname = msg.context['firstname'].replace("'", "")
        lastname = msg.context['lastname'].replace("'", "")
        mail = msg.context['mail'].replace("'", "")
        async with aiosqlite.connect(self.db_path) as db:
            sql = f"UPDATE {TABLE_USER} SET firstname='{firstname}', lastname='{lastname}', mail='{mail}' WHERE user='{name}'"
            await db.execute(sql)
            await db.commit()

##############################################

    async def set_password(self, msg):
        hs = self.hs
        name = msg.context['name'].replace("'", "")
        password = msg.context['password'].replace("'", "")
        password = pbkdf2_sha256.hash(password)
        async with aiosqlite.connect(self.db_path) as db:
            sql = f"UPDATE {TABLE_USER} SET password='{password}' WHERE user='{name}'"
            await db.execute(sql)
            await db.commit()

##############################################

    async def mfa_get(self, msg):
        hs = self.hs
        name = msg.context['name'].replace("'", "")
        mfa = pyotp.random_base32()
        async with aiosqlite.connect(self.db_path) as db:
            sql = f"UPDATE {TABLE_USER} SET mfa='{mfa}' WHERE user='{name}'"
            await db.execute(sql)
            await db.commit()
        resp = pyotp.totp.TOTP(mfa).provisioning_uri(name=name, issuer_name='lcars.holler.pro')
        await hs.bus.emit(EVENT_RESPONSE, target_app=msg.context['app'], context={'result': resp}, org_id=msg.id)     

##############################################

    async def rights_set(self, msg):
        hs = self.hs
        name = msg.context['name'].replace("'", "")
        user_id = -1
        async with aiosqlite.connect(self.db_path) as db:
            async with db.execute(f"SELECT id FROM {TABLE_USER} WHERE user='{name}' ") as cursor:
                async for row in cursor:
                    user_id = row[0]
            if user_id == -1:
                return
            sql = f"DELETE FROM {TABLE_SCOPE} WHERE user_id='%i'"
            await db.execute(sql % (user_id, ))
            for entry in msg.context['rights']:
                right = entry.replace("'", "")
                sql = f"INSERT INTO {TABLE_SCOPE} (user_id, scope) VALUES ('%i', '%s')" % (user_id, right, )
                await db.execute(sql)
            await db.commit()
            sql = f"DELETE FROM {TABLE_SCOPE_MFA} WHERE user_id='%i'"
            await db.execute(sql % (user_id, ))
            for entry in msg.context['rights_mfa']:
                right = entry.replace("'", "")
                sql = f"INSERT INTO {TABLE_SCOPE_MFA} (user_id, scope) VALUES ('%i', '%s')" % (user_id, right, )
                await db.execute(sql)
            await db.commit()

##############################################
##############################################

    async def device_data_get(self, token, device_id):
        hs = self.hs
        user_id = -1
        token = token.replace("'", "")
        device_id = device_id.replace("'", "")
        async with aiosqlite.connect(self.db_path) as db:
            async with db.execute(f"SELECT user_id FROM {TABLE_SESSION} WHERE access_token='{token}'") as cursor:
                async for row in cursor:
                    user_id = row[0]
            if user_id == -1:
                return {'error': 'no valid (1)'}
            resp = {}
            if device_id == "":
                device_id = (str(uuid())+str(uuid())+str(uuid())+str(uuid())).replace('-', '')
                resp['new_device_id'] = device_id
            timeout_clock = 180
            timeout_logout = 180
            id = -1
            sql = f"SELECT id, timeout_clock, timeout_logout FROM {TABLE_DEVICE_SETTINGS} WHERE user_id='%i' and device_id='%s'"
            async with db.execute(sql % (user_id, device_id, )) as cursor:
                async for row in cursor:
                    id = row[0]
                    timeout_clock = row[1]
                    timeout_logout = row[2]
            timestamp = int(time.time())
            if id == -1:
                sql = f"INSERT INTO {TABLE_DEVICE_SETTINGS} (user_id, timeout_clock, timeout_logout, device_id, created, last) VALUES ('%i', '180', '180', '%s', '%i', '%i')"
                sql = sql % (user_id, device_id, timestamp, timestamp)
            else:
                sql = f"UPDATE {TABLE_DEVICE_SETTINGS} SET last='{timestamp}' WHERE id='{id}'"
            await db.execute(sql)
            await db.commit()
            resp['timeout_clock'] = timeout_clock
            resp['timeout_logout'] = timeout_logout
            return resp

##############################################

    async def device_data_set(self, token, device_id, clock, logout):
        hs = self.hs
        user_id = -1
        token = token.replace("'", "")
        device_id = device_id.replace("'", "")
        if not isinstance(clock, int):
            clock = 180
        if not isinstance(logout, int):
            logout = 180
        async with aiosqlite.connect(self.db_path) as db:
            async with db.execute(f"SELECT user_id FROM {TABLE_SESSION} WHERE access_token='{token}'") as cursor:
                async for row in cursor:
                    user_id = row[0]
            if user_id != 1:
                timestamp = int(time.time())
                sql = f"UPDATE {TABLE_DEVICE_SETTINGS} SET timeout_clock='{clock}', timeout_logout='{logout}', last='{timestamp}' WHERE user_id='{user_id}' and device_id='{device_id}'"
                await db.execute(sql)
                await db.commit()

##############################################
##############################################

    async def handle_action(self, msg):
        hs = self.hs
        data = msg.context
        if data['action'] == 'get_app_data':
            res_data = await self.get_app_web_info(data['client_id'])
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                              context={'result': res_data}, \
                              target_app= data['response_app'])
        elif data['action'] == 'login':
            res_data = await self.login_user(data)
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                             context={'result': res_data}, \
                             target_app= data['response_app'])
        elif data['action'] == 'get_device_data':
            res_data = await self.device_data_get(data['token'], data['device_id'])
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                              context={'result': res_data}, \
                              target_app= data['response_app'])
        elif data['action'] == 'save_device_data':
            await self.device_data_set(data['token'], data['device_id'], data['timeout_clock'], data['timeout_logout'])
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                              context={'result': {'ok': True}}, \
                              target_app= data['response_app'])
        elif data['action'] == 'logout':
            await self.do_logout(data['token'])
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                              context={'result': {'ok': True}}, \
                              target_app= data['response_app'])
        else:
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                              context={'result': {'error': f"'{data['action']}' not handled"}}, \
                              target_app= data['response_app'])

##############################################

    async def handler(self, request):
        hs = self.hs
        headers = {}
        for key in request.headers:
            headers[key] = request.headers[key]
        if request.method == 'GET':
            url_path = request.path
            if url_path == '/auth':
                url_path = '/index.html'
            elif url_path == '/favicon.ico':
                url_path = '/account.svg'
            elif url_path == '/js/main.js':
                url_path = '/main.js'
            elif url_path == '/device_id':
                url_path = '/device_id.html'
            elif url_path == '/js/device_id.js':
                url_path = '/device_id.js'
            path = f"{hs.path.comp}/{hs.manifest['name']}/web{url_path}"
            if os.path.isfile(path):
                r = aiohttp.web.StreamResponse(headers={'Content-Type': mimetypes.guess_type(path)[0]})
                await r.prepare(request)
                try:
                    async with aiofiles.open(path, mode='rb') as f:
                        if url_path == '/index.html':
                            content = (await f.read()).decode()
                            params = request.rel_url.query
                            do_in = '<script>\n'
                            for key in params:
                                do_in += f'      var data_{key} = "{params[key]}"\n'
                            do_in += '    </script>'
                            content.replace('<script></script>', do_in)
                            await r.write(content.replace('<script></script>', do_in).encode())
                        else:
                            await r.write(await f.read())
                    await r.write_eof()
                    return r
                except Exception as e:
                    hs.log.error(repr(e))
        elif request.method == 'POST':
            if request.path == '/oauth/token':
                data = {}
                for entry in (await request.text()).split('&'):
                    data[entry.split('=')[0]] = entry.split('=')[1]
                res_data = await self.get_token(data)
                if 'valid' not in res_data:
                    return aiohttp.web.json_response(res_data)
        return aiohttp.web.Response(text="OK", status=999)

##############################################
##############################################

    async def domain_register(self):
        hs = self.hs
        await hs.bus.emit(EVENT_WEB_REGISTER_URL, target_app="local.web_master", 
                            context={'app': f"{hs.hostname}.{hs.manifest['name']}", 
                                     'domain': f"{hs.config.data['http_domain']}", 
                                     'url':f"http://{hs.ip}:{self.http_port}"})
        if self.domain_registered:
            await hs.run_delay(900, self.domain_register)
        else:
            await hs.run_delay(15, self.domain_register)

##############################################

    async def domain_register_done(self, msg):
        self.domain_registered = True

##############################################
##############################################

    async def clean_db(self):
        hs = self.hs
        if self.running:
            await hs.run_delay(3600, self.clean_db)
            async with aiosqlite.connect(self.db_path) as db:
                timestamp = int(time.time()) - 10
                sql = f"DELETE FROM {TABLE_TRANSFER} WHERE created<'{timestamp}'"
                await db.execute(sql)
                timestamp = int(time.time()) - 604800
                sql = f"DELETE FROM {TABLE_SESSION} WHERE last<'{timestamp}'"
                await db.execute(sql)
                sql = f"DELETE FROM {TABLE_DEVICE_SETTINGS} WHERE last<'{timestamp}'"
                await db.execute(sql)
                await db.commit()  

##############################################

    @overload
    async def doStart(self):
        hs = self.hs
        self.http_port = hs.reserved_ports[0]
        await self.domain_register()
        try:
            self.server = http_simple(hs.log, self.http_port, self.handler)
            await self.server.setup()
        except Exception as e:
            hs.log.error(repr(e))
            hs.kill.set()
        hs.log.info(f"{hs.name} running http server on port: {self.http_port}")
        await hs.run_delay(3600, self.clean_db)

