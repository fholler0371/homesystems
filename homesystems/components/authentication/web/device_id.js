requirejs.config({
  "paths": {
    'jquery': '/js/jquery/jquery.min'
  }
})

function postData(url = '', data = {}) {
  return fetch(url, {
    method: 'POST',
    mode: 'cors',
    cache: 'no-cache',
    redirect: 'follow',
    referrerPolicy: 'no-referrer',
    body: JSON.stringify(data)
  }).then((response) => {
    if (response.status >= 200 && response.status <= 299) {
      return response.json();
    } else {
      return { 'error': response.status, 'text': response.text() }
    }
  }).catch(err => {
    return { 'error': 1 }
  })
}

define(["jquery"], function ($) {
  $(document).ready(function() {
    window.addEventListener("message", (event) => {
      var data = event.data,
          url = event.origin
      if (data.action == 'get_device_data') {
        data.device_id = localStorage.getItem('device_id')
        if (data.device_id == null) {
          data.device_id = ''
        }
        postData('/api', data).then(data => {
          if (data.error != null) {
            console.error(data)
          } else {
            if (!(data.new_device_id == undefined)) {
              localStorage.setItem('device_id', data.new_device_id)
            }
            data.action = 'settings_received'
            window.parent.postMessage(data, url)
          }
        })
      } else if (data.action == 'save_device_data') {
        data.device_id = localStorage.getItem('device_id')
        if (data.device_id == null) {
          data.device_id = ''
        }
        postData('/api', data).then(data => {
          if (data.error != null) {
            console.error(data)
          }
        })
      } else if (data.action == 'transfer_set') {
        localStorage.setItem('transfer_token', data.token)
      } else if (data.action == 'transfer_get') {
        var data = {'action': 'transfer_data', 'token': localStorage.getItem('transfer_token')}
        localStorage.removeItem('transfer_token')
        window.parent.postMessage(data, url)
      } else if (data.action == 'logout') {
        postData('/api', data).then(data => {
          if (data.error != null) {
            console.error(data)
          }
        })
      } else {
        console.log(data)
      }
    })
  })
})