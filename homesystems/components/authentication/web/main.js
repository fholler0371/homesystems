requirejs.config({
  paths: {
    "jquery": "/js/jquery/jquery.min",
    "inject" : "/js/svg-inject",
    "jqxcore": "/js/jqwidgets/jqxcore",
    "jqxinput": "/js/jqwidgets/jqxinput",
    "jqxbutton": "/js/jqwidgets/jqxbuttons",
    "jqxpassword": "/js/jqwidgets/jqxpasswordinput"
  },
  shim:{
    "jqxinput": {deps: ["jqxcore"]},
    "jqxpassword": {deps: ["jqxcore"]},
    "jqxbutton": {deps: ["jqxcore"]},
    "jqxcore": {deps: ["jquery"]}
  }
})

function postData(url = '', data = {}, token = false) {
  return fetch(url, {
    method: 'POST',
    mode: 'cors',
    cache: 'no-cache',
    redirect: 'follow',
    referrerPolicy: 'no-referrer',
    body: JSON.stringify(data)
  }).then((response) => {
    if (response.status >= 200 && response.status <= 299) {
        return response.json();
    } else {
      return { 'error': response.status, 'text': response.text() }
    }
  }).catch(err => {
    return { 'error': 1 }
  })
}

define(["jquery", "inject", "jqxinput", "jqxpassword", "jqxbutton"], function($) {
  $(function() {
    postData('/api', { action: 'get_app_data', client_id:  data_client_id}).then(data => {
      if (data.error != null) {
        $('.loading_inner > div').text('Sorry, no application found. ....')
        console.error(data)
      } else {
        $('.loading_inner').height(500).css('fill', 'var(--main-color_or)').css('text-align', 'center')
        $('.loading_inner').prepend('<img src="/js/img/mdi/'+data.icon+'" height="200px" width="200px" onload="SVGInject(this)">')
        $('.loading_inner > div').text(data.label)
        $('.loading_inner').append('<div style="height: 30px"></div>')
        $('.loading_inner').append('<input id="name" type="text" id="input"/>')
        $("#name").jqxInput({placeHolder: "Name", height: 30, width: 380, minLength: 1, theme: 'material' })
        $('.loading_inner').append('<div style="height: 25px"></div>')
        $('.loading_inner').append('<input id="password" type="password"/>')
        $("#password").jqxPasswordInput({placeHolder: "Passwort", height: 30, width: 380, minLength: 1, theme: 'material' })
        $('.loading_inner').append('<div style="height: 25px"></div>')
        $('.loading_inner').append('<input id="mfa" type="password"/>')
        $("#mfa").jqxPasswordInput({placeHolder: "MFA", height: 30, width: 380, minLength: 1, theme: 'material' })
        $('.loading_inner').append('<div style="height: 25px"></div>')
        $('.loading_inner').append('<input type="button" value="Anmelden" id="send" />')
        $("#send").jqxButton({ width: 120, height: 40, theme: 'material' })
        $("#send").on('click', function() {
          $("#send").jqxButton({disabled: true })
          var data = {client_id: data_client_id, scope: data_scope, redirect_uri: data_redirect_uri, state: data_state}
          data.name = $("#name").val()
          data.password = $("#password").val()
          data.mfa = $("#mfa").val()
          data.action = 'login'
          postData('/api', data).then(data => {
            if (data.error != null) {
              console.error(data)
              $("#send").jqxButton({disabled: false })
              $("#name").val('')
              $("#password").val('')
              $("#mfa").val('')
            } else {
              if (data.redirect_url != undefined) {
                window.location.replace(data.redirect_url)
              }
              $("#send").jqxButton({disabled: false })
              $("#name").val('')
              $("#password").val('')
              $("#mfa").val('')
              console.log(data)
            }
          })
        })
      }
    })
  })
})  