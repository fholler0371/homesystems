from .src.const import (
    EVENT_CAST_MASTER_REGISTER,
    EVENT_CAST_MASTER_REGISTER_DONE
)


labels = ['Modul']


def __dir__():
    return [*globals().keys(), *labels]


def __getattr__(name):
    if name == "Modul":
        from .src.modul import Modul
        return Modul
