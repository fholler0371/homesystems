import asyncio
import time
from typing import List
from dataclasses import dataclass

from aiohttp_xmlrpc.client import ServerProxy
from aiohttp import ClientConnectorError

from homesystems.lib.aio_udp import c_client_broadcast, c_client_receiver

from homesystems.process import c_process_base
from homesystems.lib.helper import overload

from homesystems.const import (
    EVENT_PROCESS_STOPPING,
    EVENT_CAST_REMOTE_MESSAGE
)

from .const import (
    DEFAULT_UDP_PORT,
    DEFAULT_DATA_PORT
)

FIRST_SENT = 2            # 2 nach Sekunden
BROADCAST_INTERVALL = 60  # 60 Sekunden


@dataclass
class HostInfo:
    time: str
    host: str
    ip: str
    port: str
    count: str

    def __post_init__(self):
        self.time = int(self.time)
        self.port = int(self.port)
        self.count = int(self.count)


class Modul(c_process_base):
    def __init__(self, *args, **kwargs):
        c_process_base.__init__(self, *args, **kwargs)
        self.modul_name = 'main'
        self.udp_broadcast = None
        self.rcp_port = None
        self.udp_port = None
        self.udp_sent_count = 0
        self.udp_receiver = None
        self._hosts = {}
        self._wild_apps = {}

##############################################

    @overload
    async def doInit(self):
        hs = self.hs
        await hs.bus.subscribe(EVENT_CAST_REMOTE_MESSAGE, self.remote_msg)

##############################################
##############################################

    async def udp_callback(self, msg, addr):
        hs = self.hs
        try:
            info = HostInfo(*msg.decode().split('|'))
        except Exception as e:
            return
        if info.time + 10 < time.time():  # ausblenden alter Nachrichten
            return
        if info.host == hs.hostname:      # ausblenden eigener Nachrichten
            return
        if info.count < 5:       # sofort senden nach den ersten Nachrichten
            await self.sent_udp(startTimer=False)
        if info.host not in self._hosts:
            hs.log.info(f"new host: {info.host} {info.ip}:{info.port}")
        self._hosts[info.host] = f"http://{info.ip}:{info.port}"

##############################################

    async def sent_udp(self, startTimer=True):
        hs = self.hs
        if self.running:
            self.udp_sent_count = self.udp_sent_count + 1
            await self.udp_broadcast.send(
                f"{int(time.time())}|{hs.hostname}|{hs.ip}|" +
                f"{self.rcp_port}|{self.udp_sent_count}")
            if startTimer:
                await hs.run_delay(BROADCAST_INTERVALL, self.sent_udp)

##############################################
##############################################

    async def remote_msg(self, msg, resp):
        hs = self.hs
        out_msg = msg.data['data']
        target_app = out_msg['target_app']
        if target_app.startswith('*.'):
            if not (target_app in self._wild_apps):
                app = await self.get_wild(target_app)
                if app.startswith('*.'):
                    hs.log.warn(f'can not find wild_app: {target_app}')
                    return
                self._wild_apps[target_app] = app
            target_app = self._wild_apps[target_app]
        target_host = target_app.split('.')[0]
        if target_host not in self._hosts:
            hs.log.warn(f'host not registered: {target_host}')
            return
        rpc = ServerProxy(self._hosts[target_host], loop=hs.loop)
        try:
            resp = await rpc.message_transfer(out_msg)
        except ClientConnectorError as e:
            hs.log.warn(f"can not connect to {target_host}")
        except Exception as e:
            hs.log.error(f"{repr(e)}: {target_host}")
        await rpc.close()

##############################################

    async def get_wild(self, app):
        hs = self.hs
        out_app = app
        try:
            for host in self._hosts:
                rpc = ServerProxy(self._hosts[host], loop=hs.loop)
                try:
                    resp = await rpc.get_target_host(app)
                    if resp['app'] != "":
                        out_app = resp['app']
                except TypeError:
                    pass
                except ClientConnectorError as e:
                    hs.log.warn(f"can not connect to {self._hosts[host]}")
                except Exception as e:
                    hs.log.error(f"{repr(e)}: {host}")
                await rpc.close()
        except Exception as e:
            hs.log.error(repr(e))
        return out_app

##############################################
##############################################

    @overload
    async def doStart(self):
        hs = self.hs
        await c_process_base.doStart(self)
        self.udp_port = DEFAULT_UDP_PORT
        self.udp_broadcast = c_client_broadcast(hs, self.udp_port)
        self.rcp_port = DEFAULT_DATA_PORT
        self.udp_receiver = c_client_receiver(hs, self.udp_port,
                                              self.udp_callback)
        await self.udp_receiver.start()
        await asyncio.sleep(FIRST_SENT)
        await self.sent_udp()
