from homesystems.process import c_process_base
from homesystems.lib.helper import overload

from homesystems.lib.scriptengine import c_scriptengine

from pprint import pprint

from homesystems.lib.aio_eventbus import (
    EVENT_RESPONSE
)

from homesystems.lib.modul_states import (
#    EVENT_STATES_DEVICE_GET,
    EVENT_STATES_CHANNEL_GET,
#    EVENT_STATES_CHANNEL_SUBSCRIBE,
#    EVENT_STATES_CHANNEL_DATA,
    EVENT_STATES_CHANNEL_SET_VALUE
)

class c_modul(c_process_base):
    def __init__(self, *args, **kwargs):
        c_process_base.__init__(self, *args, **kwargs)
        self.modul_name = 'main'
    
###############################################
###############################################

    async def doInit(self):
        hs = self.hs
        await hs.bus.add_listener(EVENT_STATES_CHANNEL_GET, self.dummy_get_channel)
        await hs.bus.add_listener(EVENT_STATES_CHANNEL_SET_VALUE, self.start_script)
        # await hs.bus.add_listener(EVENT_SYSTEM_PI_GIT_PULL, self.git_pull)
        # await hs.bus.add_listener(EVENT_SYSTEM_PI_RELOAD, self.cmd_reload)

##############################################
##############################################

    async def dummy_get_channel(self, msg):
        hs = self.hs
        data = msg.context
        if data is None:
            data = {}
        target_app = data.get('app', '')
        value = {'last': 0, 'lc': 9999999999, 'ls': 0, 'ts': 9999999999, 'type': 'int', 'value': 0}
        await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, target_app=target_app, \
                          context={'result': value})

##############################################

    async def start_script(self, msg):
        hs = self.hs
        if msg.target_app != '':
            return  # msg not for this process
        scene = hs.config.scenes[msg.sensor.split('.')[0]]
        if not isinstance(scene, list):
            hs.log.warn('not a valid scene {msg.sensor}')
            return
        engine = c_scriptengine(hs, scene)
        await engine.start()

##############################################
##############################################

    @overload
    async def doStart(self):
        hs = self.hs

