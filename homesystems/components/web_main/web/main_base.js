define(function () {
  var _modul = {
    icon: '/js/img/mdi/menu.svg',
    already_init : false,
    label: 'Menü',
    init: function() {
      if (!modul.main_base.already_init) {
        modul.main_base.already_init = true
      }
    },
    show: function() {
      console.log('show main_base')
      $('#header_appname').text(modul.main_base.label)
      require(['packery', 'inject', 'jqxinput', 'jqxbuttons', 'jqxpasswordinput'], function(Packery) {
        if (!($('#content_modul_main_base').length)) {
          var html = '<div id="content_modul_main_base" class="content_modul jqx-widget-content-material modul_main_base"></div>'
          $(".main_content").append(html)
        }
        $('#content_modul_main_base').show()
        postData('/api', { 'action': 'menu_get'}, token=true).then(data => {
          if (data.error != null) {
            console.error(data)
          } else {
            $('#content_modul_main_base').text("")
            for (var i=0; i<data.length; i++) {
              var html = '<div class="modul_main_base_item" style="width: 200px; height: 200px; border: 4px solid #36c; fill: #36c; text-align: center;'
              html += 'border-radius: 12px; color: #36c; cursor: pointer" data-domain="'+data[i].domain
              html += '"><img src="/js/img/mdi/'+data[i].icon+'" height="100px" width="100px"'
              html += ' onload="SVGInject(this)" style="position: relative; top: 20px"><div style="position: relative; top: 20px;">'+data[i].label
              html += '</div></div>'
              $('#content_modul_main_base').append(html)
            }
            new Packery( '.modul_main_base', {itemSelector: '.modul_main_base_item', gutter: 20})
            $('.modul_main_base_item').off('click')
            $('.modul_main_base_item').on('click', function(event) {
              var domain = $(event.currentTarget).data('domain')
              postData('/api', { 'action': 'transfer_token_get', 'domain': domain }, token=true).then(data => {
                if (data.error != null) {
                  console.error(data)
                } else {
                  console.log(data.token)
                  if (data.token == "no") {
                    modul.login.logout()
                  } else {
                    var postdata = {action: 'transfer_set'}
                    postdata.token = data.token
                    document.getElementById('frame_device_id').contentWindow.postMessage(postdata, "https://auth.lcars.holler.pro")
                    window.location.href = data.url
                  }
                }
              })
            })
          }
        })
      })
    }
  }
  window.modul.main_base = _modul
  console.log("main_base")
  return _modul
})
  