import os
import time
import aiohttp
import mimetypes
import aiofiles
import urllib.parse
import jwt

from homesystems.lib.aio_http import http_simple

from homesystems.process import c_process_base
from homesystems.lib.helper import overload

#import time

#from aio_process import c_modulcore
#from aio_server import http_simple, web

#from pprint import pprint

from homesystems.lib.aio_eventbus import (
    EVENT_RESPONSE
)

#from aio_process import (
#    EVENT_PROCESS_RUNNING,
#    EVENT_PROCESS_STOPPING,
#    EVENT_RCP_PORT_GET
#)

from homesystems.components.web_master import (
    EVENT_WEB_REGISTER_URL,
    EVENT_WEB_REGISTER_URL_DONE,

    EVENT_WEB_GET_ACTION
)

from homesystems.components.authentication import (
    EVENT_AUTH_APP_REGISTER,
    EVENT_AUTH_APP_SECRET_ID,
    EVENT_TRANSFERTOKEN_GET
)

from .const import (
    EVENT_WEB_MAIN_REGISTER,
    EVENT_WEB_MAIN_REGISTERED
)

class c_modul(c_process_base):
    def __init__(self, *args, **kwargs):
        c_process_base.__init__(self, *args, **kwargs)
        self.modul_name = 'main'
        self.domain_registered = False
        self.auth_registered = False
        self.secret_id = ''
        self.auth_domain = ''
        self.auth_path = ''
        self.public_key = None
        self._apps = []
    
##############################################

    @overload
    async def doInit(self):
        hs = self.hs
#        await core.bus.add_listener(EVENT_PROCESS_RUNNING, self.doStart)
#        await core.bus.add_listener(EVENT_PROCESS_STOPPING, self.doStop)
        await hs.bus.add_listener(EVENT_WEB_REGISTER_URL_DONE, self.domain_register_done)
        await hs.bus.add_listener(EVENT_WEB_GET_ACTION, self.handle_action)
        await hs.bus.add_listener(EVENT_AUTH_APP_SECRET_ID, self.set_secret_id)
        await hs.bus.add_listener(EVENT_WEB_MAIN_REGISTER, self.register_an_app)

##############################################
##############################################

    async def register_an_app(self, msg):
        hs = self.hs
        data = msg.context
        for entry in self._apps:
            if entry['domain'] == data['domain']:
                break
        else:
            self._apps.append(data)
            apps = self._apps
            self._apps = sorted(apps, key=lambda d: d['label'])
        await hs.bus.emit(EVENT_WEB_MAIN_REGISTERED, context={}, target_app=data['app'])

##############################################

    async def get_scopes(self):
        hs = self.hs
        out = []
        for scope in hs.config.data['scopes']:
            out.append(scope)
        for app in self._apps:
            for scope in app["scopes"]:
                out.append(scope)
        return out#

##############################################
##############################################

    async def get_app_list(self, data):
        hs = self.hs
        out = []
        for entry in data['scope'].split(' '):
            if entry in hs.config.data['scopes']:
                out.append({'url': f"/js/{entry}", 'modul': entry})
                break
        return {'user': data['firstname'], 'modul': out}

##############################################

    async def decode_token(self, token):
        hs = self.hs
        try: 
            key = "\n".join([l.lstrip() for l in self.public_key.split("\n")])
            data = jwt.decode(token, key, algorithms=['RS256'])
            if data['expire'] > time.time():
                return data
        except:
            pass
        return None

##############################################

    async def get_menu_list(self, scope):
        hs = self.hs
        out = []
        for app in self._apps:
            for app_scope in app['scopes']:
                if app_scope in scope.split(" "):
                    out.append({'icon': app['icon'], 'label': app['label'], 'domain': app['domain']})
                    break
        return out

##############################################

    async def get_transfer_token(self, token):
        hs = self.hs
        resp = await hs.bus.emit_wait(EVENT_TRANSFERTOKEN_GET, target_app="*.authentication", 
                                      context={'app': f"{hs.hostname}.{hs.manifest['name']}", 'token': token})
        return resp
 
##############################################

    async def handle_action(self, msg):
        hs = self.hs
        data = msg.context
        if data['action'] == 'get_login_url':
            url = f"https://{self.auth_domain}.{hs.config.base_domain}/{self.auth_path}?response_type=code"
            url += f"&client_id={hs.config.data['client_id']}"
            url += f"&redirect_uri="
            url += urllib.parse.quote_plus(f"https://{hs.config.data['http_domain']}.{hs.config.base_domain}/auth_callback")
            url += f"&scope={'+'.join(await self.get_scopes())}&state="
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                              context={'result': {'url': url}}, \
                              target_app= data['response_app'])
        elif data['action'] == 'check_code':
            url = f"https://{self.auth_domain}.{hs.config.base_domain}/oauth/token"
            post_data = f"grant_type=authorization_code&code={data['code']}"
            post_data += f"&client_id={hs.config.data['client_id']}&client_secret={self.secret_id}"
            async with aiohttp.ClientSession() as session:
                async with session.post(url, data=post_data) as resp:
                    await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                        context={'result': await resp.json()}, \
                        target_app= data['response_app'])
        elif data['action'] == 'refresh_token':
            url = f"https://{self.auth_domain}.{hs.config.base_domain}/oauth/token"
            post_data = f"grant_type=refresh_token&refresh_token={data['refresh_token']}"
            post_data += f"&client_id={hs.config.data['client_id']}&client_secret={self.secret_id}"
            async with aiohttp.ClientSession() as session:
                async with session.post(url, data=post_data) as resp:
                    await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                        context={'result': await resp.json()}, \
                        target_app= data['response_app'])
        elif data['action'] == 'get_module':
            token_data = await self.decode_token(data['token'])
            if token_data is None:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "token invalid"}}, \
                                    target_app= data['response_app'])
                return
            resp_data = await self.get_app_list(token_data)
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                context={'result': resp_data}, \
                                target_app= data['response_app'])
        elif data['action'] == 'menu_get':
            token_data = await self.decode_token(data['token'])
            if token_data is None:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "token invalid"}}, \
                                    target_app= data['response_app'])
                return
            if 'main_base' not in token_data['scope']:
                hs.log.warn(f"userget: web_main? {token_data['scope']}")
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "scope invalid"}}, \
                                    target_app= data['response_app'])
            resp_data = await self.get_menu_list(token_data['scope'])
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                context={'result': resp_data}, \
                                target_app= data['response_app'])
        elif data['action'] == 'transfer_token_get':
            token_data = await self.decode_token(data['token'])
            if token_data is None:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "token invalid"}}, \
                                    target_app= data['response_app'])
                return
            if 'main_base' not in token_data['scope']:
                await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                    context={'result': {'error': "scope invalid"}}, \
                                    target_app= data['response_app'])
            resp_data = await self.get_transfer_token(data['token'])
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                context={'result': {'token': resp_data, 'url': f"https://{data['domain']}.{hs.config.base_domain}"}}, \
                                target_app= data['response_app'])
        else:
            await hs.bus.emit(EVENT_RESPONSE, org_id=msg.id, \
                                context={'result': {'error': f"'{data['action']}' not handled"}}, \
                                target_app= data['response_app'])

##############################################

    async def handler(self, request):
        hs = self.hs
        headers = {}
        for key in request.headers:
            headers[key] = request.headers[key]
        if request.method == 'GET':
            url_path = request.path
            if url_path == '/':
                url_path = '/index.html'
            elif url_path == '/auth_callback':
                url_path = '/index.html'
            elif url_path == '/favicon.ico':
                url_path = '/menu.svg'
            elif url_path == '/js/main_base.js':
                url_path = '/main_base.js'
            path = f"{hs.path.comp}/{hs.manifest['name']}/web{url_path}"
            if os.path.isfile(path):
                r = aiohttp.web.StreamResponse(headers={'Content-Type': mimetypes.guess_type(path)[0]})
                await r.prepare(request)
                try:
                    async with aiofiles.open(path, mode='rb') as f:
                        if url_path == '/index.html':
                            content = (await f.read()).decode()
                            params = request.rel_url.query
                            do_in = '<script>\n'
                            for key in params:
                                do_in += f'      var data_{key} = "{params[key]}"\n'
                            do_in += '    </script>'
                            content.replace('<script></script>', do_in)
                            await r.write(content.replace('<script></script>', do_in).encode())
                        else:
                            await r.write(await f.read())
                    await r.write_eof()
                    return r
                except Exception as e:
                    print(repr(e))
            return aiohttp.web.Response(text="", status=999)
        else:
            print(request.method)
        return aiohttp.web.Response(text="OK", status=999)

##############################################
##############################################

    async def domain_register(self):
        hs = self.hs
        await hs.bus.emit(EVENT_WEB_REGISTER_URL, target_app="*.web_master", 
                          context={'app': f"{hs.hostname}.{hs.manifest['name']}", 
                                   'domain': f"{hs.config.data.get('http_domain', 'noname')}", 
                                   'url':f"http://{hs.ip}:{self.http_port}"})
        if self.domain_registered:
            await hs.run_delay(43200, self.domain_register)
        else:
            await hs.run_delay(15, self.domain_register)

##############################################

    async def domain_register_done(self, msg):
        self.domain_registered = True

##############################################
##############################################

    async def auth_register(self):
        hs = self.hs
        await hs.bus.emit(EVENT_AUTH_APP_REGISTER, target_app="*.authentication", 
                          context={'app': f"{hs.hostname}.{hs.manifest['name']}", 
                                   'client_id': hs.config.data['client_id'], 
                                   'scopes': hs.config.data['scopes'],
                                   'label': hs.config.data['label'],
                                   'icon': hs.config.data['icon'],
                                   'callback': f"https://{hs.config.data['http_domain']}.{hs.config.base_domain}/auth_callback"})
        if self.auth_registered:
            await hs.run_delay(900, self.auth_register)
        else:
            await hs.run_delay(15, self.auth_register)

##############################################

    async def set_secret_id(self, msg):
        hs = self.hs
        self.secret_id = msg.context['secret_id']
        self.auth_domain = msg.context['domain']
        self.auth_path = msg.context['path']
        self.public_key = msg.context['public_key']
        self.auth_registered = True

##############################################
##############################################

    @overload
    async def doStart(self):
        hs = self.hs
        self.http_port = hs.reserved_ports[0]
        await self.domain_register()
        await self.auth_register()
        #await self.main_menu_register()
        try:
            self.server = http_simple(hs.log, self.http_port, self.handler)
            await self.server.setup()
        except Exception as e:
            hs.log.error(repr(e))
            hs.kill.set()
        hs.log.info(f"{hs.name} running http server on port: {self.http_port}")
