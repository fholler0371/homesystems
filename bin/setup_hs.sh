#!/bin/bash

sudo useradd -rm hs -G sudo,dialout,gpio,i2c

sudo mkdir -p /srv/homesystems

sudo chown hs:hs /srv/homesystems

cd /srv/homesystems

if [ -x "$(command -v python3.11)" ]; then
  sudo su - hs -c "python3.11 -m venv /srv/homesystems/.venv"
else
  if [ -x "$(command -v python3.10)" ]; then
    sudo su - hs -c "python3.10 -m venv /srv/homesystems/.venv"
  else
    if [ -x "$(command -v python3.9)" ]; then
      sudo su - hs -c "python3.9 -m venv /srv/homesystems/.venv"
    else
      sudo su - hs -c "python3 -m venv /srv/homesystems/.venv"
    fi
  fi
fi

sudo mkdir -p /srv/homesystems/.git

sudo chown hs:hs /srv/homesystems/.git

if [ -d "/srv/homesystems/.git/homesystems" ]; then
  cd /srv/homesystems/.git/homesystems
  sudo su - hs -c "cd /srv/homesystems/.git/homesystems ; git pull --rebase"
else
  sudo su - hs -c "cd /srv/homesystems/.git ; git clone git@gitlab.com:fholler0371/homesystems.git"
fi

if [ ! -d "/srv/homesystems/bin" ]; then
  sudo su - hs -c "cd /srv/homesystems/ ; ln -s /srv/homesystems/.git/homesystems/bin bin"
fi

if [ ! -d "/srv/homesystems/homesystems" ]; then
  sudo su - hs -c "cd /srv/homesystems/ ; ln -s /srv/homesystems/.git/homesystems/homesystems homesystems"
fi

sudo su - hs -c "cd /srv/homesystems/ ; .venv/bin/python -m homesystems -s enable -v"
sudo su - hs -c "cd /srv/homesystems/ ; .venv/bin/python -m homesystems -s status"
